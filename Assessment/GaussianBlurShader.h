#ifndef _GAUSSIAN_BLUR_SHADER_H_
#define _GAUSSIAN_BLUR_SHADER_H_

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Gaussian Blur Shader.
/// Blurs using normailized gaussian values, in a horizontal, vertical 2 pass system.
/// Deviation gives a pseudo value for stronger gaussian values, produced artifacts at higher values.
/// </summary>
class GaussianBlurShader : public BaseShader {
private:
	struct GaussianBlurBufferType {
		int screen_width;
		int screen_height;

		/// <summary>
		/// 0 - Vertical
		/// 1 - Horiztontal
		/// </summary>
		int direction;
		float deviation;
	};

public:
	/// <summary>
	/// Initialises and loads the Vertex, and Pixel Shaders.
	/// </summary>	
	GaussianBlurShader(ID3D11Device* device, HWND hwnd);

	/// <summary>
	/// Clears pointers and releases all buffers.
	/// </summary>
	~GaussianBlurShader();
	
	/// <summary>
	/// Loads information into the Matrix and Gaussian Buffers.
	/// And connects the buffers to the correct shader pipeline.
	/// </summary>
	/// <param name="render_texture"> Render texture that will be blurred. </param>
	/// <param name="screen_width"> Width of the current screen. </param>
	/// <param name="screen_height"> Height of the Current screen</param>
	/// <param name="direction"> Which pass to do of the Gaussian Blur, 0 - Vertical 1- Horizontal. </param>
	/// <param name="deviation"> The amount of deviation from the normal Gaussian Blur sample points. Increases the distance between sampling points.</param>
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* render_texture, int screen_width, int screen_height, int direction, float deviation);

	/// <summary>
	/// Renders the previous sent Vertex buffer of Index Count.
	/// Renders using DrawIndexed, and sets sampler state information
	/// </summary>	
	/// <param name="index_count"> The number of indices in the vertex buffer. </param>
	void Render(ID3D11DeviceContext* device_context, int index_count);
private:
	/// <summary>
	/// Loads the shaders, and initialises the shader buffers descriptions.	
	/// Sets up matrix buffer, gaussian buffer, and texture sampler.
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader to load. </param>
	/// <param name="ps_filename"> Pixel Shader to load. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* ps_filename);

private:
	ID3D11Buffer* gaussian_blur_buffer_;

	ID3D11SamplerState* sample_state_;

};
#endif

