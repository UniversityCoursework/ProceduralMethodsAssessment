#include "PostProcessObject.h"

#include "../DXFramework/imgui.h"

PostProcessObject::PostProcessObject()
	: GameObject() {

	current_effect_ = kNothing;
	ResetChromaticAberation();
	ResetGaussianBlur();
}

PostProcessObject::PostProcessObject(std::string name)
	: PostProcessObject() {
	name_ = name;
}

PostProcessObject::~PostProcessObject() {
	GameObject::~GameObject();
}

PostProcessObject::PostProcessEffect PostProcessObject::GetPostProcessEffect() {
	return current_effect_;
}

XMFLOAT3 PostProcessObject::GetDisplacementScale() {
	return displacement_scale_;
}

float PostProcessObject::GetBaseRadius() {
	return base_radius_;
}

float PostProcessObject::GetFalloffRadius() {
	return falloff_radius_;
}

float PostProcessObject::GetChromaPower() {
	return chroma_power_;
}

float PostProcessObject::GetGaussianDeviation() {
	return gaussian_deviation_;
}

void PostProcessObject::RenderInspector() {
	GameObject::RenderInspector();

	// Change the post process that is used.
	int effect_enum = static_cast<int>(current_effect_);
	ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
	if (ImGui::Combo("##1", &effect_enum, "No Post Process\0GaussianBlur\0ChromaticAberation\0")) {
		current_effect_ = (PostProcessEffect)effect_enum;
	}
	// Reset Width
	ImGui::PushItemWidth(0);
	switch (current_effect_) {
		case PostProcessObject::kNothing:
		{
			ImGui::TextWrapped("Normal Rendering of the Scene with no effects.");
			break;
		}
		case PostProcessObject::kGaussianBlur:
		{
			if (ImGui::Button("Reset", ImVec2(ImGui::GetContentRegionAvailWidth(), 40))) {
				ResetGaussianBlur();
			}
			ImGui::TextWrapped("Uses Gaussian Blur on the render target, giving a blurred effect to the scene.");
			ImGui::DragFloat("Deviation", &gaussian_deviation_, 0.01f, 0.01f);
			if (ImGui::IsItemHovered()) {
				ImGui::SetTooltip("Amount of deviation from normal gaussian blur. 1 being normal.");
			}
			break;
		}
		case PostProcessObject::kChromaticAberation:
		{
			if (ImGui::Button("Reset", ImVec2(ImGui::GetContentRegionAvailWidth(), 40))) {
				ResetChromaticAberation();
			}
			ImGui::Text("Chroamtic Aberation Effect");
			ImGui::TextWrapped("Current values give a nice chromatic aberation effect. \n\nIncrease the power to make it more obvious, and mess around with the other settings to change the effect.");

			ImGui::SliderFloat3("Displacement", &displacement_scale_.x, 0.0f, 2.0f, "%.3f", 0.01f);
			if (ImGui::IsItemHovered()) {
				ImGui::SetTooltip("The amount of displacement of the RGB values.");
			}
			ImGui::DragFloatRange2("Radius", &base_radius_, &falloff_radius_, 0.01f, 0.0f, 0.0f, "Base: %.2f", "Falloff: %.2f");
			if (ImGui::IsItemHovered()) {
				ImGui::SetTooltip("Radii from centre screen which the aberation effects.");
			}

			ImGui::SliderFloat("Power", &chroma_power_, 0.0f, 100.0f, "%.3f");
			if (ImGui::IsItemHovered()) {
				ImGui::SetTooltip("Increases the overall displacement effect.");
			}

			break;
		}
		default:
			break;
	}

}

void PostProcessObject::ResetChromaticAberation() {
	displacement_scale_ = XMFLOAT3(0.995f, 1.0f, 1.005f);
	base_radius_ = 0.9f;
	falloff_radius_ = 1.8f;
	chroma_power_ = 5.0f;
}

void PostProcessObject::ResetGaussianBlur() {
	gaussian_deviation_ = 1.0f;
}
