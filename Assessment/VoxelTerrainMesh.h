#ifndef _VOXEL_TERRAIN_MESH_H_
#define _VOXEL_TERRAIN_MESH_H_

#include "../DXFramework/BaseMesh.h"

#include "Voxel.h"

/// <summary>
/// Abstract Base class used by Voxel terrains.
/// Stores common functionality and buffer intiailizition for easier maintenance.
/// </summary>
class VoxelTerrainMesh : public BaseMesh {
public:
	enum NeighbourhoodRule {
		kVonNeumann,
		kMoore,
		kNone
	};
private:
	/// <summary>
	/// Voxel Terrain Input buffer struct.
	/// </summary>
	struct TerrainVoxelType {
		XMFLOAT3 position;
		XMFLOAT3 colour;
		// Front,Back,Right,Left		
		XMFLOAT4 neighbour_heights = { -1,-1,-1,-1 };
	};
	/// <summary>
	/// Instance Input buffer struct.
	/// stores instance position offset, and current instance.
	/// </summary>
	struct InstanceType {
		XMFLOAT3 position;
		unsigned int instance_count;
	};

public:

	/// <summary>
	/// Voxel Terrain Mesh Only Sets grid size does not initialise terrain data itself.
	/// Functionality of this sort must be done by child classes.
	/// </summary>
	/// <param name="grid_size"> Grid size of Points used for terrains x/y. </param>
	VoxelTerrainMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, int grid_size = 128);

	/// <summary>
	/// Clears pointers and releases all buffers.
	/// </summary>
	~VoxelTerrainMesh();

	/// <summary>
	/// The number of points on the x and y axii, not the total size of the grid.
	/// i.e. 128x128 - returns 128.
	/// </summary>
	int GetGridSize();

	/// <summary>
	/// Overrides the current size of the grid, clamping it to a minimum size of 1.
	/// </summary>
	/// <param name="size"></param>
	void SetGridSize(int size);

	/// <summary>
	/// The currently used neighbourhoodrule.	
	/// <para> See <see cref="NeighbourhoodRule"/> for the different types of neighbourhood rules sets used.</para>
	/// </summary>	
	NeighbourhoodRule GetNeighbourhoodRule();
	
	/// <summary>
	/// Overrides the neighboorhood rule.
	/// Neighbours wont change until Terrain is Reinitialized
	/// </summary>
	/// <param name="rule"> New rule to be used when generating neighbours.</param>
	void SetNeighbourhoodRule(NeighbourhoodRule rule);
	
	/// <summary>
	/// The size the voxel cubes will be rendered at.
	/// </summary>
	float GetVoxelSize();
	
	/// <summary>
	/// Overrides the voxel cube size, clamped to a MinVoxelSize.
	/// This will update in real time, due to it being handled on the gpu.
	/// <para> See <see cref="kMinVoxelSize"/> for the minimum possible voxel size, note this is redicuosly small.</para>
	/// </summary>
	/// <param name="size"> New Size of the voxel. </param>
	void SetVoxelSize(float size);

	/// <summary>
	/// The max number of instances of the terrain to render, clamping to positive.
	/// This allows for depth in the voxel terrains, lower values will improve performance but if to low could cause gaps in the terrain.
	/// </summary>	
	void SetInstanceCount(int instance_count);

	/// <summary>
	/// Sends data to the vertex buffer and sets the primitive type to point for rendering the voxel terrain.
	/// </summary>
	void SendData(ID3D11DeviceContext* device_context) override;

	/// <summary>
	/// Generate the terrain heightmap data and initializes the buffers with the new data.
	/// Will also update the terrain size, position, and neighbours if they have changed.
	/// </summary>
	void GenerateTerrain(ID3D11Device* device);

protected:
	/// <summary>
	/// Initializes the terrain storing neighbours.
	/// Destroys the previous heightmap, and generates the connections for neighbours for the provided size of terrain and neighbour hood rule.
	/// </summary>
	void InitializeTerrain();

	/// <summary>
	/// Creates and sets all the required buffers for this mesh.
	/// Vertex,instance, class specific etc.
	/// </summary>
	void InitBuffers(ID3D11Device* device) override;

	/// <summary>
	/// Generates the Heightmap data of the voxels.	
	/// Pure Virtual Function does nothing.
	/// </summary>
	virtual void GenerateHeightMap() = 0;

	/// <summary>
	/// Create x/z offsets the Heightmap uses when calculating spacing.
	/// Based on width and voxel size, ensures the terrain is centred.
	/// </summary>
	void SetUpCentreOffsets();

	/// <summary>
	/// Checks if position is a valid index in the heightmap array for the current row.
	/// </summary>
	bool IsNeighbourOnRow(int position, int j);

	/// <summary>
	/// Checks if the position is a valid index in the Heightmap.
	/// </summary>
	bool IsNeighbour(int position);

protected:
	NeighbourhoodRule neighbourhood_rule_;
	NeighbourhoodRule previous_neighbourhood_rule_;

	Voxel* height_map_;

	int size_;
	int terrain_width_;
	int terrain_height_;
	float voxel_size_;

	float x_offset_;
	float z_offset_;
};
#endif // !_VOXEL_TERRAIN_MESH_H_