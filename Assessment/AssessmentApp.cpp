#include "AssessmentApp.h"

#include "../DXFramework/imgui.h"
#include "../DXFramework/imgui_impl_dx11.h"
#include "../DXFramework/imgui_style.h"
#include "../DXFramework/imgui_d3d.h"

#include "../DXFramework/imgui_gameobject_editor.h"
#include "../DXFramework/imgui_utils.h"

#include "../DXFramework/ConfigIni.h"

AssessmentApp::AssessmentApp() {
}

void AssessmentApp::Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input* input) {
	// Call super Init function (required!)
	BaseApplication::Init(h_instance, hwnd, screen_width, screen_height, input);

	ImGui::SetupImGuiStyle(true, 0.8f);

	PerlinNoise::InitialisePermutations();

	// Light Grey Colour
	scene_background_colour_ = XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f);

	// default values for UI etc.
	show_editor_outliner_ = true;
	show_editor_gizmos_ = false;

	// Setup the meshes etc.
	sphere_mesh_ = new SphereMesh(direct_3d_->GetDevice(), direct_3d_->GetDeviceContext(), L"../res/DefaultDiffuse.png");
	orth_mesh_ = new OrthoMesh(direct_3d_->GetDevice(), direct_3d_->GetDeviceContext(), screen_width, screen_height);

	cellular_automata_mesh_ = new CellularAutomataTerrain(direct_3d_->GetDevice(), direct_3d_->GetDeviceContext(), 32);
	fractional_brownian_motion_mesh_ = new FractionalBrownianMotionTerrain(direct_3d_->GetDevice(), direct_3d_->GetDeviceContext(), 32);

	// Setup the shaders
	light_gizmo_shader_ = new LightGizmoShader(direct_3d_->GetDevice(), hwnd);
	light_shader_ = new MultiLightShader(direct_3d_->GetDevice(), hwnd);
	voxel_terrain_shader_ = new VoxelTerrainShader(direct_3d_->GetDevice(), hwnd);
	chromatic_shader_ = new ChromaticShader(direct_3d_->GetDevice(), hwnd);
	gaussian_blur_shader_ = new GaussianBlurShader(direct_3d_->GetDevice(), hwnd);

	// Setup Render target for post processing.
	scene_render_texture_ = new RenderTexture(direct_3d_->GetDevice(), screen_width, screen_height, kScreenNear, kScreenDepth);
	gaussian_first_pass_texture_ = new RenderTexture(direct_3d_->GetDevice(), screen_width, screen_height, kScreenNear, kScreenDepth);

	// Generate Gameobjects for all objects in the scene.
	// This allows them to be selected and manipulated in the Outliner, and Inspector in ImGui.

	terrain_ = new VoxelTerrainObject("Voxel Terrain");
	terrain_->SetCellularAutomataMesh(cellular_automata_mesh_);
	terrain_->SetFbmMesh(fractional_brownian_motion_mesh_);
	terrain_->SetDevice(direct_3d_->GetDevice());
	terrain_->SetPosition(0, 0, 0);
	terrain_->SetUniformScale(1.0f);

	floating_orb_ = new GameObject("Floating Orb");
	floating_orb_->SetMesh(sphere_mesh_);
	floating_orb_->SetPosition(0, 6.0f, 0);
	floating_orb_->SetEulerRotation(45, 0, 0);
	floating_orb_->SetUniformScale(0.5f);

	post_process_ = new PostProcessObject("Post Processing");
	post_process_->SetMesh(orth_mesh_);

	// Add all the game objects to the vector for easy passing to the in-game editor.
	gameobjects_.push_back(terrain_);
	gameobjects_.push_back(post_process_);
	gameobjects_.push_back(floating_orb_);

	// Set up the 3 lights used in the scene, with some intersting values.
	lights_[0] = new Light;
	lights_[0]->SetType(LightType::kAmbient);
	lights_[0]->SetAmbientColour(0.2f, 0.2f, 0.2f, 1.0f);
	lights_[0]->SetSpecularColour(1.0f, 0.0f, 0.0f, 1.0f);
	lights_[0]->SetPosition(4.f, 2.f, 0.0f);


	lights_[1] = new Light;
	lights_[1]->SetType(LightType::kDirectional);
	lights_[1]->SetDiffuseColour(0.2f, 0.2f, 0.2f, 1.0f);
	lights_[1]->SetSpecularColour(0.2f, 0.2f, 0.2f, 1.0f);
	lights_[1]->SetPosition(-4.0f, 10.0f, 0.0f);
	lights_[1]->SetDirection(0.0f, -1.0f, -1.0f);
	lights_[1]->SetSpecularPower(0.0f);

	lights_[2] = new Light;
	lights_[2]->SetType(LightType::kPoint);
	lights_[2]->SetDiffuseColour(1.0f, 1.0f, 0.5f, 1.0f);
	lights_[2]->SetSpecularColour(1.0f, 1.0f, 0.5f, 1.0f);
	lights_[2]->SetPosition(0.f, 5.f, 4.0f);
	lights_[2]->SetSpecularPower(20.f);

	// Create game objects for manipulating the lights in the in-game editor.
	for (int i = 0; i < MAXLIGHTS; i++) {
		// Generate name for lights in outliner.
		std::string name = "Light: " + std::to_string(i);
		// Add all the lights as a gizmo as want them to be rendered a certain way.
		editor_lights_.push_back(new LightObject(name));
		// Give it a mesh aswell.
		editor_lights_.back()->SetMesh(sphere_mesh_);
		editor_lights_.back()->SetLight(lights_[i]);
		// Set the mesh's scale.
		editor_lights_.back()->SetUniformScale(0.1f);
		// add them to scene gameobjects as well ( allows them to show up easily in the outliner).
		gameobjects_.push_back(editor_lights_.back());
	}
	// Give the camera an nice position and rotation.
	camera_->SetPosition(0.0f, 8.0f, -10.0f);
	camera_->SetRotation(30, 0, 0);
	camera_->Update();
}

AssessmentApp::~AssessmentApp() {
	// Run base Application deconstructor
	BaseApplication::~BaseApplication();

	if (sphere_mesh_) {
		delete sphere_mesh_;
		sphere_mesh_ = nullptr;
	}

	if (orth_mesh_) {
		delete orth_mesh_;
		orth_mesh_ = nullptr;
	}

	if (cellular_automata_mesh_) {
		delete cellular_automata_mesh_;
		cellular_automata_mesh_ = nullptr;
	}

	if (fractional_brownian_motion_mesh_) {
		delete fractional_brownian_motion_mesh_;
		fractional_brownian_motion_mesh_ = nullptr;
	}

	if (light_gizmo_shader_) {
		delete light_gizmo_shader_;
		light_gizmo_shader_ = nullptr;
	}

	if (light_shader_) {
		delete light_shader_;
		light_shader_ = nullptr;
	}

	if (voxel_terrain_shader_) {
		delete voxel_terrain_shader_;
		voxel_terrain_shader_ = nullptr;
	}

	if (chromatic_shader_) {
		delete chromatic_shader_;
		chromatic_shader_ = nullptr;
	}

	if (gaussian_blur_shader_) {
		delete gaussian_blur_shader_;
		gaussian_blur_shader_ = nullptr;
	}

	if (scene_render_texture_) {
		delete scene_render_texture_;
		scene_render_texture_ = nullptr;
	}

	if (gaussian_first_pass_texture_) {
		delete gaussian_first_pass_texture_;
		gaussian_first_pass_texture_ = nullptr;
	}

	for (int i = 0; i < MAXLIGHTS; i++) {
		if (lights_[i]) {
			delete lights_[i];
			lights_[i] = nullptr;
		}
	}

	if (floating_orb_) {
		delete floating_orb_;
		floating_orb_ = nullptr;
	}

	if (terrain_) {
		delete terrain_;
		terrain_ = nullptr;
	}

	if (post_process_) {
		delete post_process_;
		post_process_ = nullptr;
	}
}

bool AssessmentApp::Frame() {
	bool result;

	result = BaseApplication::Frame();
	if (!result) {
		return false;
	}

	// Render the graphics.
	result = Render();
	if (!result) {
		return false;
	}

	return true;
}

bool AssessmentApp::Render() {

	// Clear the scene.
	direct_3d_->BeginScene(scene_background_colour_.x, scene_background_colour_.y, scene_background_colour_.z, scene_background_colour_.w);

	// Generate the view matrix based on the camera's position.
	camera_->Update();

	UpdateGizmos();

	// Renders selected effect.
	switch (post_process_->GetPostProcessEffect()) {
		case PostProcessObject::kNothing:
		{
			RenderScene();
			break;
		}
		case PostProcessObject::kGaussianBlur:
		{
			RenderSceneToTexture();
			RenderGaussianBlur();
			break;
		}
		case PostProcessObject::kChromaticAberation:
		{
			RenderSceneToTexture();
			RenderChromaticAberation();
			break;
		}
		default:
			break;
	}

	RenderImGui();

	// Present the rendered scene to the screen.
	direct_3d_->EndScene();
	return true;
}

void AssessmentApp::RenderSceneToTexture() {

	// Set Render target to the scene texture.
	scene_render_texture_->SetRenderTarget(direct_3d_->GetDeviceContext());

	// Clear the Render texture.
	scene_render_texture_->ClearRenderTarget(direct_3d_->GetDeviceContext(), scene_background_colour_.x, scene_background_colour_.y, scene_background_colour_.z, scene_background_colour_.w);

	RenderScene();

	// Reset Render target to the output.
	direct_3d_->SetBackBufferRenderTarget();
}

void AssessmentApp::RenderScene() {
	// Generate the view and projection Matrices, world matrix is taken from the Game object.
	XMMATRIX view_matrix, projection_matrix;
	camera_->GetViewMatrix(view_matrix);
	direct_3d_->GetProjectionMatrix(projection_matrix);

	// Send Floating Orb geometry data.
	floating_orb_->Mesh()->SendData(direct_3d_->GetDeviceContext());
	// Pass in the floating orb; world matrix (pos,rotation,scale), and texture. As well as all the lights and the camera position.
	light_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), floating_orb_->WorldMatrix(), view_matrix, projection_matrix, floating_orb_->Mesh()->GetTexture(),
		lights_, camera_->GetPosition());
	light_shader_->Render(direct_3d_->GetDeviceContext(), floating_orb_->Mesh()->GetIndexCount());

	// Send Terrain geometry data.
	terrain_->Mesh()->SendData(direct_3d_->GetDeviceContext());
	// Pass in the Terrain; world matrix (pos,rotation,scale), and voxel size. As well as all the lights and the camera position.	
	voxel_terrain_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), terrain_->WorldMatrix(), view_matrix, projection_matrix, terrain_->GetVoxelSize(), lights_, camera_->GetPosition());
	voxel_terrain_shader_->RenderInstance(direct_3d_->GetDeviceContext(), terrain_->Mesh()->GetVertexCount(), terrain_->Mesh()->GetInstanceCount());

	if (show_editor_gizmos_) {
		RenderGizmos();
	}
}

void AssessmentApp::RenderChromaticAberation() {
	// Generate Matrices for Ortho Projection.
	XMMATRIX baseViewMatrix, orthoMatrix;
	direct_3d_->GetOrthoMatrix(orthoMatrix);
	camera_->GetBaseViewMatrix(baseViewMatrix);

	// Turn depth off as ortho.
	direct_3d_->TurnZBufferOff();

	// Send geometry data.
	post_process_->Mesh()->SendData(direct_3d_->GetDeviceContext());

	// Pass the scene, the scene texture, and the chromatic aberation settings from the editor.
	chromatic_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), post_process_->WorldMatrix(), baseViewMatrix, orthoMatrix,
		scene_render_texture_->GetShaderResourceView(), post_process_->GetDisplacementScale(), post_process_->GetBaseRadius(), post_process_->GetFalloffRadius(), post_process_->GetChromaPower());

	// Render it onto the Ortho mesh.
	chromatic_shader_->Render(direct_3d_->GetDeviceContext(), post_process_->Mesh()->GetIndexCount());

	// Turn depth back on.
	direct_3d_->TurnZBufferOn();
}

void AssessmentApp::RenderGaussianBlur() {
	// Generate Matrices for Ortho Projection.
	XMMATRIX worldMatrix, baseViewMatrix, orthoMatrix;
	direct_3d_->GetWorldMatrix(worldMatrix);
	direct_3d_->GetOrthoMatrix(orthoMatrix);
	camera_->GetBaseViewMatrix(baseViewMatrix);

	// Render Vertical
	// Set Render target to the gaussian texture
	gaussian_first_pass_texture_->SetRenderTarget(direct_3d_->GetDeviceContext());

	// Clear the Render texture. (Light Grey Colour)
	gaussian_first_pass_texture_->ClearRenderTarget(direct_3d_->GetDeviceContext(), scene_background_colour_.x, scene_background_colour_.y, scene_background_colour_.z, scene_background_colour_.w);

	// Turn depth off as ortho.
	direct_3d_->TurnZBufferOff();

	// Send geometry data.
	post_process_->Mesh()->SendData(direct_3d_->GetDeviceContext());

	gaussian_blur_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), post_process_->WorldMatrix(), baseViewMatrix, orthoMatrix,
		scene_render_texture_->GetShaderResourceView(), direct_3d_->GetScreenWidth(), direct_3d_->GetScreenHeight(), 0, post_process_->GetGaussianDeviation());

	// Render it onto the Ortho mesh.
	gaussian_blur_shader_->Render(direct_3d_->GetDeviceContext(), post_process_->Mesh()->GetIndexCount());

	// Reset Render target to the output.
	direct_3d_->SetBackBufferRenderTarget();

	// Render Horizontal

	// Send geometry data.
	post_process_->Mesh()->SendData(direct_3d_->GetDeviceContext());

	gaussian_blur_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), post_process_->WorldMatrix(), baseViewMatrix, orthoMatrix,
		gaussian_first_pass_texture_->GetShaderResourceView(), direct_3d_->GetScreenWidth(), direct_3d_->GetScreenHeight(), 1, post_process_->GetGaussianDeviation());

	// Render it onto the Ortho mesh.
	gaussian_blur_shader_->Render(direct_3d_->GetDeviceContext(), post_process_->Mesh()->GetIndexCount());


	// Turn depth back on.
	direct_3d_->TurnZBufferOn();

}

void AssessmentApp::RenderImGui() {
	// Ensure all shaders are reset to null before rendering ImGui.
	direct_3d_->ResetShadersNull();

	// Pass in screen resolution so mouse input can be scaled to resolution if in fullscreen at low resolution.
	ImGui_ImplDX11_NewFrameDPI(direct_3d_->GetScreenWidth(), direct_3d_->GetScreenHeight());
	{
		// Menu bar along the top of the screen.
		if (ImGui::BeginMainMenuBar()) {
			// Drop down menu for exposing editor settings.
			if (ImGui::BeginMenu("Menu")) {
				ImGui::ExposeWireFrameMenuItem(direct_3d_);
				ImGui::MenuItem("Editor Gizmos", NULL, &show_editor_gizmos_);
				ImGui::MenuItem("Editor", NULL, &show_editor_outliner_);
				if (ImGui::MenuItem("Reset Camera", NULL)) {
					camera_->SetPosition(0, 0, -10.0f);
					camera_->SetRotation(0, 0, 0);
				}
				ImGui::EndMenu();
			}

			// Shows the Resolution changer options, and windowed fullscreen options.
			// If it returns true it means the screen has changed size.
			if (ImGui::ExposeResolutionOptions(direct_3d_, wnd_)) {
				// The window has been resized, therefore we Resize any Render Targets and ortho meshes.
				// ImGui handles its own resizing.
				int new_width = direct_3d_->GetScreenWidth();
				int new_height = direct_3d_->GetScreenHeight();

				orth_mesh_->ResizeMesh(direct_3d_->GetDevice(), new_width, new_height);
				scene_render_texture_->ResizeTexture(direct_3d_->GetDevice(), new_width, new_height);
				gaussian_first_pass_texture_->ResizeTexture(direct_3d_->GetDevice(), new_width, new_height);
			}
			// Displays the current fps at the end of the taskbar.
			ImGui::DisplayFPS();
			ImGui::EndMainMenuBar();
		}
		if (show_editor_outliner_) {
			ImGui::Editor(&show_editor_outliner_, gameobjects_);
		}
	}
	ImGui::Render();
}

void AssessmentApp::UpdateGizmos() {
	for (int i = 0; i < MAXLIGHTS; i++) {
		editor_lights_[i]->SetPosition(lights_[i]->GetPosition().x, lights_[i]->GetPosition().y, lights_[i]->GetPosition().z);
	}
}

void AssessmentApp::RenderGizmos() {
	XMMATRIX view_matrix, projection_matrix;
	// Get the view, projection, and ortho matrices from the camera and Direct3D objects.	
	camera_->GetViewMatrix(view_matrix);
	direct_3d_->GetProjectionMatrix(projection_matrix);

	for (int i = 0; i < MAXLIGHTS; i++) {
		// Send mesh data.
		sphere_mesh_->SendData(direct_3d_->GetDeviceContext());
		// Set shader parameters; matrices, texture, and Lights diffuse colour.
		light_gizmo_shader_->SetShaderParameters(direct_3d_->GetDeviceContext(), editor_lights_[i]->WorldMatrix(), view_matrix, projection_matrix, lights_[i]->GetDiffuseColour());
		light_gizmo_shader_->Render(direct_3d_->GetDeviceContext(), sphere_mesh_->GetIndexCount());
	}
}
