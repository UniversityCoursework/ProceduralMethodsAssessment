#ifndef _VOXEL_TERRAIN_OBJECT_H
#define _VOXEL_TERRAIN_OBJECT_H

#include "../DXFramework/GameObject.h"
#include "CellularAutomataTerrain.h"
#include "FractionalBrownianMotionTerrain.h"

/// <summary>
/// Exposes the fBM and Celullar Automata properties in the ImGui Editor.
/// Also Decides the mesh to render and where.
/// </summary>
class VoxelTerrainObject : public GameObject {
private:
	enum TerrainTechnique {
		kFractionalBrownianMotion,
		kCelluarAutomata
	};
	enum TerrainPresets {
		kSmall,
		kMedium,
		kLarge
	};
public:

	/// <summary>
	/// Sets up the Object with default parameters and clears the pointers.
	/// </summary>
	VoxelTerrainObject();

	/// <summary>
	/// Sets the name of the object, this name will be displayed in the outliner.
	/// </summary>
	/// <param name="name"> Name of GameObject displayed in ImGui Outliner. </param>	
	VoxelTerrainObject(std::string name);

	/// <summary>
	/// Clears the pointers.
	/// </summary>	
	~VoxelTerrainObject();

	/// <summary>
	/// The render size of the voxels used by the VoxelTerrainShader.
	/// </summary>
	/// <returns> The voxel size for the terrains. </returns>
	float GetVoxelSize();

	/// <summary>
	/// The currently active terrain mesh.
	/// </summary>
	/// <returns></returns>
	BaseMesh* Mesh() override;

	/// <summary>
	/// Stores the fBM mesh so it can be selectivly exposed via Mesh().
	/// </summary>
	/// <param name="fractional_brownian_motion_mesh"> Mesh pointer to fbm mesh that will be stored in game object. </param>
	void SetFbmMesh(FractionalBrownianMotionTerrain* fractional_brownian_motion_mesh);

	/// <summary>
	/// Stores the cellular automata mesh so it can be selectivly exposed via Mesh().	
	/// <param name="cellular_autoamta_mesh"> Mesh pointer to ca mesh that will be stored in gameobject. </param>
	void SetCellularAutomataMesh(CellularAutomataTerrain* cellular_autoamta_mesh);

	/// <summary>
	/// D3D device used to regenerate the Buffers for the meshes, when terrain size changed.
	/// </summary>
	/// <param name="device"></param>
	void SetDevice(ID3D11Device* device);

	/// <summary>
	/// Exposes the various settings for this object in ImGui.
	/// Ensure this is used within ImGui system, and best used as part of the Outliner/Editor only.
	/// </summary>
	void RenderInspector() override;

private:
	/// <summary>
	/// Function made private so SetFbmMesh and SetCellularAutomataMesh are used instead.
	/// </summary>
	void SetMesh(BaseMesh* mesh) override {}

	/// <summary>
	/// Generates the terrain for the currently active technique.
	/// </summary>
	void GenerateActiveTechnique();

	/// <summary>
	/// Renders all the ImGui Settings for the fractional Brownian Motion terrain.
	/// </summary>
	void RenderFbmInspector();
	/// <summary>
	/// Renders all the ImGui Settings for the Cellular Automata terrain.
	/// </summary>
	void RenderCaInspector();

	/// <summary>
	/// Resets the terrains settings to the currently selected preset.
	/// </summary>
	void ResetToPreset();

	TerrainTechnique terrain_technique_;
	TerrainPresets	terrain_preset_;

	FractionalBrownianMotionTerrain* fractional_brownian_motion_mesh_;

	CellularAutomataTerrain* cellular_automata_mesh_;

	ID3D11Device* device_;

	// random number engine used to generate accurate random number generation.
	std::mt19937 num_engine_;


};
#endif // !_VOXEL_TERRAIN_OBJECT_H
