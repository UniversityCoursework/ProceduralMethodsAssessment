#ifndef _FRACTIONAL_BROWNIAN_MOTION_TERRAIN_MESH_H_
#define _FRACTIONAL_BROWNIAN_MOTION_TERRAIN_MESH_H_

#include "../DXFramework/PerlinNoise.h"

#include "VoxelTerrainMesh.h"

using namespace DirectX;
using namespace LittleLot;


/// <summary>
/// Terrain Generation Using Fractional Brownian Motion, with a neumian neighbourhood.
/// The terrain is generated using a turbulence function of perlin noise.
/// <para> See <see cref="VoxelTerranObject"/> for how settings are exposed to ImGui Editor.</para>
/// </summary>
class FractionalBrownianMotionTerrain : public VoxelTerrainMesh {
public:

	/// <summary>
	/// Generates the terrain and sets up the default parameters for fBM.
	/// Sets the neighbourhood to neumian as gives best results for colouring filling.
	/// </summary>
	/// <param name="grid_size"> Grid size of Points used for terrains x/y. </param>
	FractionalBrownianMotionTerrain(ID3D11Device* device, ID3D11DeviceContext* device_context, int grid_size = 128);

	/// <summary>
	/// Clears pointers and releases all buffers.
	/// </summary>
	~FractionalBrownianMotionTerrain();

	/// <summary>
	/// The height at which the water is.
	/// This is rounded to voxel height when sent to the buffers.
	/// </summary>
	float GetWaterHeight();

	/// <summary>
	/// Overrides the current water height clamping to a positive number.
	/// </summary>
	/// <param name="water_height"> The height at which the water should be, will be rounded to voxel heights when used in buffers. </param>
	void SetWaterHeight(float water_height);

	/// <summary>
	/// The height at which the snow should start.
	/// This is rounded to voxel height when sent to the buffers.
	/// </summary>
	float GetSnowHeight();

	/// <summary>
	/// Overrides the current snow height clamping to a positive number.
	/// </summary>
	/// <param name="snow_height"> The height at which the snow should start, will be rounded to voxel heights when used in buffers. </param>	
	void SetSnowHeight(float snow_height);

	/// <summary>
	/// The PerlinNoise Fbm settings this mesh is using for perlin noise.
	/// </summary>
	PerlinNoise::FbmSettings GetFbmSettings();

	/// <summary>
	/// Overrides the current Fbm settings with the new struct.
	/// </summary>
	/// <param name="turbulence_settings"> New Turbulence Settings to use. </param>
	void SetFbmSettings(PerlinNoise::FbmSettings turbulence_settings);

private:
	/// <summary>
	/// Generates the heightmap data points and positions based on fractional brownian motion.
	/// <para> See <see cref="LittleLot::PerlinNoise"/> For Description of fBM implementation.</para>
	/// </summary>
	void GenerateHeightMap() override;

	/// <summary>
	/// Calculates colour of voxels based off neighbours and voxel states.
	/// </summary>
	void ColourInMap();

	/// <summary>
	/// Generates a Perlin Based Fractional Brownian Motion floating point at x,y using FbmSettings.
	/// <para> See <see cref="LittleLot::PerlinNoise"/> For Description of fBM implementation.</para>
	/// <para> See <see cref="LittleLot::PerlinNoise::FbmSettings"/> For Description of settings used.</para>
	/// </summary>
	float FbmRandomHeight(int x, int y);

private:
	PerlinNoise::FbmSettings fbm_settings_;
	float water_height_;
	float snow_height_;
};

#endif // !_FRACTIONAL_BROWNIAN_MOTION_TERRAIN_MESH_H_

