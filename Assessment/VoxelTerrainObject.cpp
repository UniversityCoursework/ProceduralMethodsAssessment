#include "VoxelTerrainObject.h"

#include "../DXFramework/imgui.h"

VoxelTerrainObject::VoxelTerrainObject()
	: GameObject() {
	fractional_brownian_motion_mesh_ = nullptr;
	cellular_automata_mesh_ = nullptr;
	device_ = nullptr;
	terrain_technique_ = kFractionalBrownianMotion;
}

VoxelTerrainObject::VoxelTerrainObject(std::string name)
	: VoxelTerrainObject() {
	name_ = name;
}

VoxelTerrainObject::~VoxelTerrainObject() {
	fractional_brownian_motion_mesh_ = nullptr;
	cellular_automata_mesh_ = nullptr;
	device_ = nullptr;
}

float VoxelTerrainObject::GetVoxelSize() {
	switch (terrain_technique_) {
		case VoxelTerrainObject::kFractionalBrownianMotion:
			return fractional_brownian_motion_mesh_->GetVoxelSize();
			break;
		case VoxelTerrainObject::kCelluarAutomata:
			return cellular_automata_mesh_->GetVoxelSize();
			break;
	}
	return -1.0f;
}

BaseMesh * VoxelTerrainObject::Mesh() {
	switch (terrain_technique_) {
		case VoxelTerrainObject::kFractionalBrownianMotion:
			return fractional_brownian_motion_mesh_;
			break;
		case VoxelTerrainObject::kCelluarAutomata:
			return cellular_automata_mesh_;
			break;
	}
	return nullptr;
}

void VoxelTerrainObject::SetFbmMesh(FractionalBrownianMotionTerrain * fractional_brownian_motion_mesh) {
	fractional_brownian_motion_mesh_ = fractional_brownian_motion_mesh;

}

void VoxelTerrainObject::SetCellularAutomataMesh(CellularAutomataTerrain * cellular_autoamta_mesh) {
	cellular_automata_mesh_ = cellular_autoamta_mesh;
}

void VoxelTerrainObject::SetDevice(ID3D11Device * device) {
	device_ = device;
}

void VoxelTerrainObject::RenderInspector() {
	ExposePosition();

	int temp_input_int;
	float temp_input_float;

	if (ImGui::Button("Generate Terrain", ImVec2(ImGui::GetContentRegionAvailWidth(), 40))) {
		GenerateActiveTechnique();
	}

	// Change the terrain that is used.
	temp_input_int = (int)terrain_technique_;
	ImGui::PushItemWidth(ImGui::GetContentRegionAvailWidth());
	if (ImGui::Combo("##1", &temp_input_int, "FractionalBrownianMotion\0CelluarAutomata\0")) {
		terrain_technique_ = (TerrainTechnique)temp_input_int;
		GenerateActiveTechnique();
	}

	// Change all settings to a preset.
	ImGui::PushItemWidth(100);
	temp_input_int = (int)terrain_preset_;
	if (ImGui::Combo("Preset", &temp_input_int, "Small\0Medium\0Large\0")) {
		terrain_preset_ = (TerrainPresets)temp_input_int;
		ResetToPreset();
		GenerateActiveTechnique();
	}
	if (ImGui::IsItemHovered()) {
		ImGui::SetTooltip("Preset Settings, will regenerate the terrain to the specific settings when changed.");
	}
	// Reset to the preset.
	ImGui::SameLine(0);
	if (ImGui::Button("Reset", ImVec2(ImGui::GetContentRegionAvailWidth(), 20))) {
		ResetToPreset();
		GenerateActiveTechnique();
	}

	ImGui::Text("Settings");

	// Expose the grid size of the terrain.
	temp_input_int = fractional_brownian_motion_mesh_->GetGridSize();
	ImGui::InputInt("Grid Size", &temp_input_int);
	fractional_brownian_motion_mesh_->SetGridSize(temp_input_int);
	cellular_automata_mesh_->SetGridSize(temp_input_int);
	if (ImGui::IsItemHovered()) {
		ImGui::SetTooltip("Size of the Voxel Grid used, number of voxels on x/z.");
	}

	// Expose the voxel size of the terrains voxels.
	temp_input_float = cellular_automata_mesh_->GetVoxelSize();
	ImGui::DragFloat("Voxel Size", &temp_input_float, 0.001f);
	fractional_brownian_motion_mesh_->SetVoxelSize(temp_input_float);
	cellular_automata_mesh_->SetVoxelSize(temp_input_float);
	if (ImGui::IsItemHovered()) {
		ImGui::SetTooltip("The size of each voxel cube.");
	}

	// Expose the Max Instances of the terrains.
	temp_input_int = fractional_brownian_motion_mesh_->GetInstanceCount();
	ImGui::InputInt("Max Instances", &temp_input_int);
	fractional_brownian_motion_mesh_->SetInstanceCount(temp_input_int);
	cellular_automata_mesh_->SetInstanceCount(temp_input_int);
	if (ImGui::IsItemHovered()) {
		ImGui::SetTooltip("The max number of voxel layers of terrain.");
	}

	// Render the specific settings for each technique.
	switch (terrain_technique_) {
		case VoxelTerrainObject::kFractionalBrownianMotion:
			RenderFbmInspector();
			break;
		case VoxelTerrainObject::kCelluarAutomata:
			RenderCaInspector();
			break;
	}

}

void VoxelTerrainObject::GenerateActiveTechnique() {
	switch (terrain_technique_) {
		case VoxelTerrainObject::kFractionalBrownianMotion:
			fractional_brownian_motion_mesh_->GenerateTerrain(device_);
			break;
		case VoxelTerrainObject::kCelluarAutomata:
			cellular_automata_mesh_->GenerateTerrain(device_);
			break;
	}
}

void VoxelTerrainObject::RenderFbmInspector() {
	float temp_input_float;

	if (ImGui::CollapsingHeader("fbM Settings")) {
		
		temp_input_float = fractional_brownian_motion_mesh_->GetWaterHeight();
		ImGui::InputFloat("Water Height", &temp_input_float, fractional_brownian_motion_mesh_->GetVoxelSize(), 0.0f, 3);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Used to decide what height the water starts at.");
		}
		fractional_brownian_motion_mesh_->SetWaterHeight(temp_input_float);

		temp_input_float = fractional_brownian_motion_mesh_->GetSnowHeight();
		ImGui::InputFloat("Snow Height", &temp_input_float, fractional_brownian_motion_mesh_->GetVoxelSize(), 0.0f, 3);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Used to decide what height the snow starts at.");
		}
		fractional_brownian_motion_mesh_->SetSnowHeight(temp_input_float);

		ImGui::Text("fbm Turbulence Settings");


		PerlinNoise::FbmSettings fbm_settings = fractional_brownian_motion_mesh_->GetFbmSettings();

		ImGui::DragFloat("Scaling Factor", &fbm_settings.scaling_factor, 0.01f);

		ImGui::DragFloat("Cross Section", &fbm_settings.cross_section, 0.01f);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Used to decide the Horizontal slice to take from 3D perlin Noise.");
		}
		ImGui::InputInt("Octaves", &fbm_settings.octaves);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Typically a bit less than log(terrain_width) / log(lacunarity). So, for a 1024 x 1024 heightfield, about 10 octaves are needed.");
		}
		ImGui::DragFloat("Lacunarity", &fbm_settings.lacunarity, 0.01f);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("The frequency multiplier between octaves, is Typically set to a number just under 2.0 (e.g. 1.92).");
		}
		ImGui::DragFloat("Gain", &fbm_settings.gain, 0.01f);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Directly influences the terrain roughness.");
		}
		fractional_brownian_motion_mesh_->SetFbmSettings(fbm_settings);

		if (ImGui::Button("Reset Turbulence Settings", ImVec2(ImGui::GetContentRegionAvailWidth(), 30))) {
			PerlinNoise::FbmSettings reset_settings;
			fractional_brownian_motion_mesh_->SetFbmSettings(reset_settings);
		}
		
	}
}

void VoxelTerrainObject::RenderCaInspector() {
	int temp_input_int;
	float temp_input_float;
	if (ImGui::CollapsingHeader("Cellular Automata Settings")) {
		
		temp_input_int = cellular_automata_mesh_->GetBirthThreshold();
		ImGui::InputInt("Birth Threshold", &temp_input_int);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Threshold number of neighbours for a dead voxel to be born.");
		}
		cellular_automata_mesh_->SetBirthThreshold(temp_input_int);

		temp_input_int = cellular_automata_mesh_->GetDeathThreshold();
		ImGui::InputInt("Death Threshold", &temp_input_int);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Threshold number of neighbours for an Alive voxel to die.");
		}
		cellular_automata_mesh_->SetDeathThreshold(temp_input_int);

		temp_input_float = cellular_automata_mesh_->GetInitialChanceOfBirth();
		ImGui::InputFloat("Chance of Birth", &temp_input_float, 0.01f, 0.0f, 2);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("The initial chance of a voxel being alive at the start.");
		}
		cellular_automata_mesh_->SetInitialChanceOfBirth(temp_input_float);

		temp_input_int = cellular_automata_mesh_->GetNumberOfIterations();
		ImGui::InputInt("Number of Iterations", &temp_input_int);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("How many times to run the cellular automata.");
		}
		cellular_automata_mesh_->SetNumberOfIterations(temp_input_int);

		temp_input_int = cellular_automata_mesh_->GetRandomSeed();
		ImGui::DragInt("", &temp_input_int);
		if (ImGui::IsItemHovered()) {
			ImGui::SetTooltip("Random Seed used for intial birth chances.");
		}
		cellular_automata_mesh_->SetRandomSeed(temp_input_int);

		ImGui::SameLine(0);
		if (ImGui::Button("Random Seed", ImVec2(ImGui::GetContentRegionAvailWidth(), 20))) {
			std::uniform_int_distribution<int> random_seed(0, INT_MAX);
			cellular_automata_mesh_->SetRandomSeed(random_seed(num_engine_));
		}

		if (ImGui::Button("Do another iteration", ImVec2(ImGui::GetContentRegionAvailWidth(), 30))) {
			cellular_automata_mesh_->DoIterationStep(device_);
		}
		
	}
}

void VoxelTerrainObject::ResetToPreset() {
	switch (terrain_preset_) {
		case VoxelTerrainObject::kSmall:
			// Set grid size.
			fractional_brownian_motion_mesh_->SetGridSize(32);
			cellular_automata_mesh_->SetGridSize(32);
			// set voxel size.
			fractional_brownian_motion_mesh_->SetVoxelSize(0.25f);
			cellular_automata_mesh_->SetVoxelSize(0.25f);
			break;
		case VoxelTerrainObject::kMedium:
			// Set grid size.
			fractional_brownian_motion_mesh_->SetGridSize(128);
			cellular_automata_mesh_->SetGridSize(128);
			// set voxel size.
			fractional_brownian_motion_mesh_->SetVoxelSize(0.25f);
			cellular_automata_mesh_->SetVoxelSize(0.25f);
			break;
		case VoxelTerrainObject::kLarge:
			// Set grid size.
			fractional_brownian_motion_mesh_->SetGridSize(256);
			cellular_automata_mesh_->SetGridSize(256);
			// set voxel size.
			fractional_brownian_motion_mesh_->SetVoxelSize(0.25f);
			cellular_automata_mesh_->SetVoxelSize(0.25f);
			break;
	}
}

