#include "FractionalBrownianMotionTerrain.h"

FractionalBrownianMotionTerrain::FractionalBrownianMotionTerrain(ID3D11Device * device, ID3D11DeviceContext * device_context, int grid_size)
	: VoxelTerrainMesh(device, device_context, grid_size) {
	neighbourhood_rule_ = kVonNeumann;

	water_height_ = 0.25f;
	snow_height_ = 1.5f;

	GenerateTerrain(device);
}

FractionalBrownianMotionTerrain::~FractionalBrownianMotionTerrain() {
	VoxelTerrainMesh::~VoxelTerrainMesh();
}

float FractionalBrownianMotionTerrain::GetWaterHeight() {
	return water_height_;
}

void FractionalBrownianMotionTerrain::SetWaterHeight(float water_height) {
	if (water_height < 0) {
		water_height = 0;
	}
	water_height_ = water_height;
}

float FractionalBrownianMotionTerrain::GetSnowHeight() {
	return snow_height_;
}

void FractionalBrownianMotionTerrain::SetSnowHeight(float snow_height) {
	if (snow_height < 0) {
		snow_height = 0;
	}
	snow_height_ = snow_height;
}

PerlinNoise::FbmSettings FractionalBrownianMotionTerrain::GetFbmSettings() {
	return fbm_settings_;
}

void FractionalBrownianMotionTerrain::SetFbmSettings(PerlinNoise::FbmSettings turbulence_settings) {
	if (fbm_settings_.octaves < 1) {
		fbm_settings_.octaves = 1;
	}

	fbm_settings_ = turbulence_settings;
}

void FractionalBrownianMotionTerrain::GenerateHeightMap() {
	int index;

	float height = 0.0f;
	float voxel_height = 0.0f;
	float x_pos = x_offset_;
	float z_pos = z_offset_;
	// Loop through the terrain and set the heights.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;

			// Generate random height.
			voxel_height = 0.0f;
			height = FbmRandomHeight(i, j);
			// find the closest voxel height to it.
			while (voxel_height < height) {
				voxel_height += voxel_size_;
			}
			height_map_[index].SetPosition(x_pos, voxel_height, z_pos);

			x_pos += voxel_size_;
		}
		x_pos = x_offset_;
		z_pos += voxel_size_;
	}
	ColourInMap();
}

void FractionalBrownianMotionTerrain::ColourInMap() {
	// Calculate water height in voxels. offset by one voxel.
	float water_height = 0.0f;
	while (water_height < water_height_ - voxel_size_) {
		water_height += voxel_size_;
	}

	int index;
	// Add water and set voxels that are at water to water height.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;

			if (height_map_[index].Position().y < water_height_) {
				height_map_[index].SetHeight(water_height);
				height_map_[index].SetState(Voxel::kWater);
			} else {
				// Reset to default.
				height_map_[index].SetState(Voxel::kNothing);
			}
		}
	}

	Voxel* centre;
	// Check neighbours, and decide voxel colours.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;
			centre = &height_map_[index];

			// if it isn't water we need to check its neigbours to decide its colour.
			if (centre->State() != Voxel::kWater) {

				// neighbours used for determining voxel colour.
				if (height_map_[index].IsBesideWater()) {
					centre->SetState(Voxel::kBeach);
				} else if (centre->IsLevelArea()) {
					if (centre->IsHigherThan(snow_height_)) {
						centre->SetState(Voxel::kSnow);
					} else {
						centre->SetState(Voxel::kGrass);
					}
				} else if (centre->IsLoneVoxel(voxel_size_)) {
					centre->SetState(Voxel::kShrub);
				} else {
					centre->SetState(Voxel::kRock);
				}
			}
		}
	}
}

float FractionalBrownianMotionTerrain::FbmRandomHeight(int x, int y) {
	return (float)PerlinNoise::FractionalBrownianMotion((float)x / (float)terrain_width_, (float)y / (float)terrain_height_, fbm_settings_);
}
