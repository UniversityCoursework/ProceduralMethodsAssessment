#ifndef _VOXEL_H_
#define _VOXEL_H_

#include <d3d11.h>
#include <directxmath.h>

#include <vector>

using namespace DirectX;

/// <summary>
/// Voxel Representation, this class allows easy checking of its neighbours.
/// Most voxel systems should be neighbour independent, however these systems currently use a very naive approach.
/// </summary>
class Voxel {
public:
	/// <summary>
	/// Voxel state of the voxel, used to determine colours and used in neighbour calculations.
	/// </summary>
	enum VoxelState {
		kNothing = 0,
		kWater,
		kBeach,
		kRock,
		kGrass,
		kShrub,
		kDirt,
		kSnow
	};

	Voxel();
	~Voxel();

	/// <summary>
	/// Current Position of the Voxel.
	/// </summary>
	XMFLOAT3 Position();

	/// <summary>
	/// Changes the Position of the voxel.
	/// </summary>	
	void SetPosition(float x, float y, float z);

	/// <summary>
	/// Changes the height of the voxel in the y.
	/// </summary>
	/// <param name="y"></param>
	void SetHeight(float y);

	/// <summary>
	/// The current State of the Voxel.
	/// <para> See <see cref="VoxelState"/>. </para>
	/// </summary>
	VoxelState State();

	/// <summary>
	/// Changes the VoxelState.
	/// </summary>
	/// <param name="state"> New State to change to.</param>
	void SetState(VoxelState state);

	/// <summary>
	/// The colour of the voxel based on its current state.
	/// </summary>
	/// <returns> RGB colour representation. </returns>
	XMFLOAT3 Colour();

	/// <summary>
	/// Stores a pointer to another voxel, to be used as its neighbour.
	/// This neighbour will be used in neighbour checkign calculations.
	/// </summary>
	/// <param name="voxel"> Pointer to the voxels new neighbour. </param>
	void AddNeighbour(Voxel* voxel);

	/// <summary>
	/// Checks if this voxel has any neighbours that are water.
	/// </summary>
	/// <returns> True if one or more of its neighbours are water. </returns>
	bool IsBesideWater();

	/// <summary>
	/// Checks if this voxel is in a level area.
	/// Level area is any area that doesn't not have neighbours lower than itself.
	/// </summary>
	/// <returns> True if no neighbours are lower than itself. </returns>
	bool IsLevelArea();

	/// <summary>
	/// Checks if this voxel is heigher than the provided height.
	/// </summary>
	/// <param name="height"> The height to compare against. </param>
	/// <returns> True if its y position is higher than provided height. </returns>
	bool IsHigherThan(float height);

	/// <summary>
	/// Checks if all of neighbour voxels are lower than itself.
	/// </summary>
	/// <param name="voxel_size"> Current global size of voxels.</param>
	/// <returns> True if this voxel is by itself. </returns>
	bool IsLoneVoxel(float voxel_size);

	/// <summary>
	/// Calcualtes the number of neighbour voxels only one below this voxel.
	/// </summary>
	/// <param name="voxel_size"> Current global size of voxels. </param>
	/// <returns> The number of voxels one below this voxel. </returns>
	int NumberOfNeighboursOneBelow(float voxel_size);

	/// <summary>
	/// Calcualtes the number of neighbour voxels of the provided state.
	/// </summary>
	/// <param name="state"> The state to check for. </param>
	/// <returns> The number of voxels of the provided state. </returns>
	int NumberOfNeighboursOfState(VoxelState state);

private:
	XMFLOAT3 position_;
	VoxelState state_;

	std::vector<Voxel*> neighbours_;
};

#endif // !_VOXEL_H_
