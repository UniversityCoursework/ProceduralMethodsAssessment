#include "GaussianBlurShader.h"

GaussianBlurShader::GaussianBlurShader(ID3D11Device * device, HWND hwnd)
	: BaseShader(device, hwnd) {
	InitShader(L"shaders/post_process_vs.hlsl", L"shaders/gaussian_blur_ps.hlsl");
}

GaussianBlurShader::~GaussianBlurShader() {
	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the box blur constant buffer.
	if (gaussian_blur_buffer_) {
		gaussian_blur_buffer_->Release();
		gaussian_blur_buffer_ = 0;
	}
	//Release base shader components
	BaseShader::~BaseShader();
}

void GaussianBlurShader::SetShaderParameters(ID3D11DeviceContext * device_context, const XMMATRIX & world_matrix, const XMMATRIX & view_matrix, const XMMATRIX & projection_matrix, ID3D11ShaderResourceView * render_texture, int screen_width, int screen_height, int direction, float deviation) {
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mapped_resource;

	MatrixBufferType* data_ptr;
	GaussianBlurBufferType* gaussian_ptr;

	unsigned int buffer_number;
	XMMATRIX transposed_world_matrix, transposed_view_matrix, transposed_projection_matrix;

	// Setup the Matrix Buffer.	
	// Transpose the matrices to prepare them for the shader.
	transposed_world_matrix = XMMatrixTranspose(world_matrix);
	transposed_view_matrix = XMMatrixTranspose(view_matrix);
	transposed_projection_matrix = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	data_ptr = (MatrixBufferType*)mapped_resource.pData;

	data_ptr->world = transposed_world_matrix;
	data_ptr->view = transposed_view_matrix;
	data_ptr->projection = transposed_projection_matrix;

	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	buffer_number = 0;

	// Set the constant buffer in the Vertex Shader.
	device_context->VSSetConstantBuffers(buffer_number, 1, &matrix_buffer_);

	/** Additional **/


	// Send Gaussian blur buffer to the pixel shader.

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(gaussian_blur_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	gaussian_ptr = (GaussianBlurBufferType*)mapped_resource.pData;

	gaussian_ptr->screen_width = screen_width;
	gaussian_ptr->screen_height = screen_height;
	gaussian_ptr->direction = direction;
	gaussian_ptr->deviation = deviation;

	// Unlock the constant buffer.
	device_context->Unmap(gaussian_blur_buffer_, 0);
	buffer_number = 0;
	// Set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(buffer_number, 1, &gaussian_blur_buffer_);

	// Set shader texture resource in the pixel shader.
	device_context->PSSetShaderResources(0, 1, &render_texture);
}

void GaussianBlurShader::Render(ID3D11DeviceContext * device_context, int index_count) {
	// Set the sampler state in the pixel shader.
	device_context->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(device_context, index_count);
}

void GaussianBlurShader::InitShader(WCHAR * vs_filename, WCHAR * ps_filename) {

	D3D11_BUFFER_DESC matrix_buffer_description;
	D3D11_SAMPLER_DESC texture_sampler_description;
	D3D11_BUFFER_DESC gaussian_blur_buffer_description;

	// Load (+ compile) shader files
	LoadVertexShader(vs_filename);
	LoadPixelShader(ps_filename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrix_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	matrix_buffer_description.ByteWidth = sizeof(MatrixBufferType);
	matrix_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrix_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrix_buffer_description.MiscFlags = 0;
	matrix_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&matrix_buffer_description, NULL, &matrix_buffer_);


	// Setup Gaussian Blur buffer.
	gaussian_blur_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	gaussian_blur_buffer_description.ByteWidth = sizeof(GaussianBlurBufferType);
	gaussian_blur_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	gaussian_blur_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	gaussian_blur_buffer_description.MiscFlags = 0;
	gaussian_blur_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer.
	device_->CreateBuffer(&gaussian_blur_buffer_description, NULL, &gaussian_blur_buffer_);


	// Setup Sampler State.
	texture_sampler_description.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	texture_sampler_description.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	texture_sampler_description.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	texture_sampler_description.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	texture_sampler_description.MipLODBias = 0.0f;
	texture_sampler_description.MaxAnisotropy = 1;
	texture_sampler_description.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	texture_sampler_description.BorderColor[0] = 0;
	texture_sampler_description.BorderColor[1] = 0;
	texture_sampler_description.BorderColor[2] = 0;
	texture_sampler_description.BorderColor[3] = 0;
	texture_sampler_description.MinLOD = 0;
	texture_sampler_description.MaxLOD = D3D11_FLOAT32_MAX;
	// Create the texture sampler state.
	device_->CreateSamplerState(&texture_sampler_description, &sample_state_);
}
