#include "CellularAutomataTerrain.h"

CellularAutomataTerrain::CellularAutomataTerrain(ID3D11Device * device, ID3D11DeviceContext * device_context, int grid_size)
	: VoxelTerrainMesh(device, device_context, grid_size) {
	neighbourhood_rule_ = kMoore;

	death_threshold_ = 3;
	birth_threshold_ = 4;
	number_of_iterations_ = 4;
	initial_chance_of_birth_ = 0.4f;
	random_seed_ = 1729;

	GenerateTerrain(device);
}

CellularAutomataTerrain::~CellularAutomataTerrain() {
	VoxelTerrainMesh::~VoxelTerrainMesh();
}

int CellularAutomataTerrain::GetDeathThreshold() {
	return death_threshold_;
}

void CellularAutomataTerrain::SetDeathThreshold(int death_threshold) {
	if (death_threshold < 0) {
		death_threshold = 0;
	}
	death_threshold_ = death_threshold;
}

int CellularAutomataTerrain::GetBirthThreshold() {
	return birth_threshold_;
}

void CellularAutomataTerrain::SetBirthThreshold(int birth_threshold) {
	if (birth_threshold < 0) {
		birth_threshold = 0;
	}
	birth_threshold_ = birth_threshold;
}

int CellularAutomataTerrain::GetNumberOfIterations() {
	return number_of_iterations_;
}

void CellularAutomataTerrain::SetNumberOfIterations(int number_of_iterations) {
	if (number_of_iterations < 1) {
		number_of_iterations = 1;
	}
	number_of_iterations_ = number_of_iterations;
}

float CellularAutomataTerrain::GetInitialChanceOfBirth() {
	return initial_chance_of_birth_;
}

void CellularAutomataTerrain::SetInitialChanceOfBirth(float chance_of_birth) {
	if (chance_of_birth < 0) {
		chance_of_birth = 0;
	}
	initial_chance_of_birth_ = chance_of_birth;
}

int CellularAutomataTerrain::GetRandomSeed() {
	return random_seed_;
}

void CellularAutomataTerrain::SetRandomSeed(int seed) {
	if (seed < 0) {
		seed = 0;
	}
	random_seed_ = seed;
}

void CellularAutomataTerrain::DoIterationStep(ID3D11Device * device) {
	// New Temporary height map to save steps too.
	// Ensures the current heighmap doesnt get edited during a step.
	Voxel* temp_height_map_ = new Voxel[terrain_width_ * terrain_height_];
	IterationStep(temp_height_map_);
	delete[] temp_height_map_;

	InitBuffers(device);
}

void CellularAutomataTerrain::GenerateHeightMap() {
	// rock = alive
	// dirt = dead
	int index;
	float x_pos = x_offset_;
	float z_pos = z_offset_;

	std::uniform_real_distribution<float> random_chance_of_birth(0, 1.0f);

	num_engine_.seed(random_seed_);

	// Loop through the terrain and reset to dead, with initial births.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;

			// Ensure edge pieces are alive.
			if (i == 0 || j == 0 || i == terrain_width_ - 1 || j == terrain_height_ - 1) {
				height_map_[index].SetState(Voxel::kRock);
				height_map_[index].SetPosition(x_pos, voxel_size_ * 2, z_pos);
			} else {
				if (random_chance_of_birth(num_engine_) < initial_chance_of_birth_) {
					height_map_[index].SetState(Voxel::kRock);
					height_map_[index].SetPosition(x_pos, voxel_size_ * 2, z_pos);
				} else {
					height_map_[index].SetState(Voxel::kDirt);
					height_map_[index].SetPosition(x_pos, 0, z_pos);
				}
			}
			x_pos += voxel_size_;
		}
		x_pos = x_offset_;
		z_pos += voxel_size_;
	}

	// New Temporary height map to save steps too.
	// Ensures the current heighmap doesnt get edited during a step.
	Voxel* temp_height_map_ = new Voxel[terrain_width_ * terrain_height_];


	for (int i = 0; i < number_of_iterations_; i++) {
		IterationStep(temp_height_map_);
	}

	delete[] temp_height_map_;

}

void CellularAutomataTerrain::IterationStep(Voxel * temp_voxel_map) {
	// rock = alive
	// dirt = dead
	int index;
	// Loop through the terrain and step forward one iteration.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;
			// keep edge pieces alive.
			if (i == 0 || j == 0 || i == terrain_width_ - 1 || j == terrain_height_ - 1) {
				temp_voxel_map[index].SetState(Voxel::kRock);
			} else {
				int number_alive = height_map_[index].NumberOfNeighboursOfState(Voxel::kRock);

				if (height_map_[index].State() == Voxel::kRock) {
					// If a cell is alive but has too few neighbours, kill it.
					if (number_alive < death_threshold_) {
						temp_voxel_map[index].SetState(Voxel::kDirt);
					} else {
						temp_voxel_map[index].SetState(Voxel::kRock);
					}
				} else {
					// If a cell is dead and it has the right number of neighbours to be 'born'
					if (number_alive > birth_threshold_) {
						temp_voxel_map[index].SetState(Voxel::kRock);
					} else {
						temp_voxel_map[index].SetState(Voxel::kDirt);
					}
				}
			}
		}
	}

	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;
			// copy over state from the temporary array.
			height_map_[index].SetState(temp_voxel_map[index].State());
			if (height_map_[index].State() == Voxel::kRock) {
				height_map_[index].SetHeight(voxel_size_ * 2);
			} else {
				height_map_[index].SetHeight(0);
			}
		}
	}
}
