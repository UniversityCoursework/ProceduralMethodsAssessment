#ifndef _POST_PROCCESS_OBJECT_H_
#define _POST_PROCCESS_OBJECT_H_

#include "../DXFramework/GameObject.h"

/// <summary>
/// Extension of the GameObject class to expose the Post Process effects easily in the editor.
/// Exposes the Settings for the Post Process effects available in the editor.
/// Allows easy modifiying and storing of the effects settings.
/// </summary>
class PostProcessObject : public GameObject {
public:
	enum PostProcessEffect {
		kNothing,
		kGaussianBlur,
		kChromaticAberation
	};
public:
	/// <summary>
	/// Sets up the Object with default parameters and clears the pointers.
	/// </summary>
	PostProcessObject();
	/// <summary>
	/// Sets the name of the object, this name will be displayed in the outliner.
	/// </summary>
	/// <param name="name"> Name of GameObject displayed in ImGui Outliner. </param>	
	PostProcessObject(std::string name);

	/// <summary>
	/// Clears the pointers.
	/// </summary>	
	~PostProcessObject();

	/// <summary>
	/// The currently active post process effect in the Editor.
	/// Used to determine render path to follow.
	/// </summary>
	/// <returns> The post process enum of the current effect. </returns>
	PostProcessEffect GetPostProcessEffect();

	/// <summary>
	/// The amount of Displacement in RGB Values for the Chromatic Aberation Effect.
	/// </summary>
	/// <returns> Displacement value used by the chromatic shader. </returns>
	XMFLOAT3 GetDisplacementScale();

	/// <summary>
	/// The Base radius for the Chromatic Aberation Effect.
	/// The lower limit of the effect.
	/// </summary>
	/// <returns> Base Radius used by the chromatic shader. </returns>
	float GetBaseRadius();

	/// <summary>
	/// The Falloff radius for the Chromatic Aberation Effect.
	/// The upper limit of the effects.
	/// </summary>
	/// <returns> Fallof Radius used by the chromatic shader. </returns>
	float GetFalloffRadius();

	/// <summary>
	/// The Chromatic power of the Chromatic Aberation Effect.
	/// Power that amplifies the displacement amounts.
	/// </summary>
	/// <returns> Chromatic Power used by the chromatic shader. </returns>
	float GetChromaPower();

	/// <summary>
	/// The amount of deviation from the normal Gaussian Blur sample points.
	/// Increases the distance between sampling points.
	/// </summary>
	/// <returns> The Deviation Used by the Gaussian Blur Shader. </returns>
	float GetGaussianDeviation();

	/// <summary>
	/// Exposes the various settings for this object in ImGui.
	/// Ensure this is used within ImGui system, and best used as part of the Outliner/Editor only.
	/// </summary>
	void RenderInspector() override;
private:
	/// <summary>
	/// Resets the Chromatic Aberation effect to some default settings.
	/// These settings will give the best expected results.
	/// </summary>
	void ResetChromaticAberation();

	/// <summary>
	/// Resets the Gaussian Blur effect to some default settings.
	/// These settings give the best expected results.
	/// </summary>
	void ResetGaussianBlur();

	XMFLOAT3 displacement_scale_;
	float base_radius_;
	float falloff_radius_;
	float chroma_power_;

	float gaussian_deviation_;
	PostProcessEffect current_effect_;

};

#endif // !_POST_PROCCESS_OBJECT_H_

