#include "VoxelTerrainMesh.h"

static const float kOutOfBoundsNeighbourHeight = -1.0f;
static const float kMinVoxelSize = 0.01f;

VoxelTerrainMesh::VoxelTerrainMesh(ID3D11Device * device, ID3D11DeviceContext * device_context, int grid_size) {
	instance_count_ = 10;
	voxel_size_ = 0.25f;
	neighbourhood_rule_ = kNone;

	SetGridSize(grid_size);
}

VoxelTerrainMesh::~VoxelTerrainMesh() {
	BaseMesh::~BaseMesh();
	if (height_map_) {
		delete[] height_map_;
	}
}

int VoxelTerrainMesh::GetGridSize() {
	return size_;
}

void VoxelTerrainMesh::SetGridSize(int size) {
	if (size < 1) {
		size = 1;
	}
	size_ = size;
}

VoxelTerrainMesh::NeighbourhoodRule VoxelTerrainMesh::GetNeighbourhoodRule() {
	return neighbourhood_rule_;
}

void VoxelTerrainMesh::SetNeighbourhoodRule(NeighbourhoodRule rule) {
	neighbourhood_rule_ = rule;
}

float VoxelTerrainMesh::GetVoxelSize() {
	return voxel_size_;
}

void VoxelTerrainMesh::SetVoxelSize(float size) {
	if (size < kMinVoxelSize) {
		size = kMinVoxelSize;
	}
	voxel_size_ = size;
}

void VoxelTerrainMesh::SetInstanceCount(int instance_count) {
	if (instance_count < 0) {
		instance_count = 0;
	}
	instance_count_ = instance_count;
}

void VoxelTerrainMesh::SendData(ID3D11DeviceContext * device_context) {
	unsigned int strides[2];
	unsigned int offsets[2];
	ID3D11Buffer* buffer_pointers[2];

	// Set the buffer strides.
	strides[0] = sizeof(TerrainVoxelType);
	strides[1] = sizeof(InstanceType);

	// Set the buffer offsets.
	offsets[0] = 0;
	offsets[1] = 0;

	// Set the array of pointers to the vertex and instance buffers.
	buffer_pointers[0] = vertex_buffer_;
	buffer_pointers[1] = instance_buffer_;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device_context->IASetVertexBuffers(0, 2, buffer_pointers, strides, offsets);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
}

void VoxelTerrainMesh::GenerateTerrain(ID3D11Device * device) {
	// The terrain size has changed.
	if ((terrain_width_ != size_ && terrain_height_ != size_) || (previous_neighbourhood_rule_ != neighbourhood_rule_)) {
		terrain_width_ = size_;
		terrain_height_ = size_;
		previous_neighbourhood_rule_ = neighbourhood_rule_;

		InitializeTerrain();
	}
	// Create x/z offsets the mesh based on width and voxel size.
	SetUpCentreOffsets();

	GenerateHeightMap();

	InitBuffers(device);
}

void VoxelTerrainMesh::InitializeTerrain() {

	// Delete any old terrain data.
	if (height_map_) {
		delete[] height_map_;
	}
	// Create the structure to hold the terrain data.
	height_map_ = new Voxel[terrain_width_ * terrain_height_];

	int index;
	int neighbour;
	// Loop through the terrain and set the neighbours we want.
	for (int j = 0; j < terrain_height_; j++) {
		for (int i = 0; i < terrain_width_; i++) {
			index = (terrain_height_ * j) + i;
			// Add desired Neighbour refrences.
			switch (neighbourhood_rule_) {
				case kNone:
					break;
				case kVonNeumann:
				{
					// north 
					neighbour = (terrain_height_ * (j - 1)) + i;
					if (IsNeighbour(neighbour)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					// south
					neighbour = (terrain_height_ * (j + 1)) + i;
					if (IsNeighbour(neighbour)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					// east
					neighbour = (terrain_height_ * j) + (i + 1);
					if (IsNeighbourOnRow(neighbour, j)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					// west
					neighbour = (terrain_height_ * j) + (i - 1);
					if (IsNeighbourOnRow(neighbour, j)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					break;
				}
				case kMoore:
				{
					// north 
					neighbour = (terrain_height_ * (j - 1)) + i;
					if (IsNeighbour(neighbour)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					// south
					neighbour = (terrain_height_ * (j + 1)) + i;
					if (IsNeighbour(neighbour)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
					}
					// east
					neighbour = (terrain_height_ * j) + (i + 1);
					if (IsNeighbourOnRow(neighbour, j)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);

						// north east
						neighbour = (terrain_height_ * (j - 1)) + (i + 1);
						if (IsNeighbour(neighbour)) {
							height_map_[index].AddNeighbour(&height_map_[neighbour]);
						}
						// south east
						neighbour = (terrain_height_ * (j + 1)) + (i + 1);
						if (IsNeighbour(neighbour)) {
							height_map_[index].AddNeighbour(&height_map_[neighbour]);
						}

					}
					// west
					neighbour = (terrain_height_ * j) + (i - 1);
					if (IsNeighbourOnRow(neighbour, j)) {
						height_map_[index].AddNeighbour(&height_map_[neighbour]);
						// north east
						neighbour = (terrain_height_ * (j - 1)) + (i - 1);
						if (IsNeighbour(neighbour)) {
							height_map_[index].AddNeighbour(&height_map_[neighbour]);
						}
						// south east
						neighbour = (terrain_height_ * (j + 1)) + (i - 1);
						if (IsNeighbour(neighbour)) {
							height_map_[index].AddNeighbour(&height_map_[neighbour]);
						}
					}
					break;
				}
				break;
				default:
					break;
			}
		}
	}
}

void VoxelTerrainMesh::InitBuffers(ID3D11Device * device) {
	TerrainVoxelType* voxels;
	InstanceType* instances;

	D3D11_BUFFER_DESC vertex_buffer_desc, instance_buffer_desc;
	D3D11_SUBRESOURCE_DATA vertex_data, instance_data;

	// Calculate the number of voxels in the terrain mesh.
	vertex_count_ = (terrain_width_) * (terrain_height_) * 4;

	// Create the vertex array.
	voxels = new TerrainVoxelType[vertex_count_];

	// Create all the voxels from the voxel data.
	int index, centre, neighbour;
	index = 0;
	for (int j = 0; j < (terrain_height_); j++) {
		for (int i = 0; i < (terrain_width_); i++) {
			centre = (terrain_height_ * j) + i;

			voxels[index].position = height_map_[centre].Position();
			voxels[index].colour = height_map_[centre].Colour();
			// Neighbour heights used for culling.
			// north 
			neighbour = (terrain_height_ * (j - 1)) + i;
			if (IsNeighbour(neighbour)) {
				voxels[index].neighbour_heights.x = height_map_[neighbour].Position().y;
			} else {
				voxels[index].neighbour_heights.x = kOutOfBoundsNeighbourHeight;
			}
			// south
			neighbour = (terrain_height_ * (j + 1)) + i;
			if (IsNeighbour(neighbour)) {
				voxels[index].neighbour_heights.y = height_map_[neighbour].Position().y;
			} else {
				voxels[index].neighbour_heights.y = kOutOfBoundsNeighbourHeight;
			}
			// east
			neighbour = (terrain_height_ * j) + (i + 1);
			if (IsNeighbourOnRow(neighbour, j)) {
				voxels[index].neighbour_heights.z = height_map_[neighbour].Position().y;
			} else {
				voxels[index].neighbour_heights.z = kOutOfBoundsNeighbourHeight;
			}
			// west
			neighbour = (terrain_height_ * j) + (i - 1);
			if (IsNeighbourOnRow(neighbour, j)) {
				voxels[index].neighbour_heights.w = height_map_[neighbour].Position().y;
			} else {
				voxels[index].neighbour_heights.w = kOutOfBoundsNeighbourHeight;
			}
			index++;
		}
	}

	// Set up the description of the static vertex buffer.
	vertex_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	vertex_buffer_desc.ByteWidth = sizeof(TerrainVoxelType)* vertex_count_;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.CPUAccessFlags = 0;
	vertex_buffer_desc.MiscFlags = 0;
	vertex_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertex_data.pSysMem = voxels;
	vertex_data.SysMemPitch = 0;
	vertex_data.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertex_buffer_desc, &vertex_data, &vertex_buffer_);

	// Release the arrays now that the buffers have been created and loaded.
	delete[] voxels;
	voxels = nullptr;

	// Create the instance buffer
	float height = 0.0f;
	instances = new InstanceType[instance_count_];
	for (int i = 0; i < instance_count_; i++) {
		instances[i].position = XMFLOAT3(0, height, 0);
		instances[i].instance_count = i;
		height -= voxel_size_;
	}

	// Set up the description of the instance buffer.
	instance_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	instance_buffer_desc.ByteWidth = sizeof(InstanceType) * instance_count_;
	instance_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	instance_buffer_desc.CPUAccessFlags = 0;
	instance_buffer_desc.MiscFlags = 0;
	instance_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the instance data.
	instance_data.pSysMem = instances;
	instance_data.SysMemPitch = 0;
	instance_data.SysMemSlicePitch = 0;

	// Now create the instance buffer.
	device->CreateBuffer(&instance_buffer_desc, &instance_data, &instance_buffer_);

	delete[] instances;
	instances = nullptr;
}

void VoxelTerrainMesh::SetUpCentreOffsets() {
	x_offset_ = -0.5f * (terrain_width_ * voxel_size_);
	z_offset_ = -0.5f * (terrain_height_ * voxel_size_);
}

bool VoxelTerrainMesh::IsNeighbourOnRow(int position, int j) {
	if (position < 0 || position >(terrain_width_*terrain_height_) ||// out of array bounds.
		(position > ((terrain_height_ * j) + terrain_width_ - 1)) || // on the next row.
		(position < (terrain_height_ * j)) // on the previous row.
		) {
		return false;
	}
	return true;
}

bool VoxelTerrainMesh::IsNeighbour(int position) {
	if (position < 0 || position >= (terrain_width_*terrain_height_)) { // out of array bounds.
		return false;
	}
	return true;
}
