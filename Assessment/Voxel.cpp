#include "Voxel.h"

#include <complex> 

/// <summary>
/// Allowed error in difference of height.
/// </summary>
const float kEpsilon = 0.00001f;

Voxel::Voxel() {
	state_ = kNothing;
}

Voxel::~Voxel() {
	neighbours_.clear();
}

XMFLOAT3 Voxel::Position() {
	return position_;
}

void Voxel::SetPosition(float x, float y, float z) {
	position_ = XMFLOAT3(x, y, z);
}

void Voxel::SetHeight(float y) {
	position_.y = y;
}

Voxel::VoxelState Voxel::State() {
	return state_;
}

void Voxel::SetState(VoxelState state) {
	state_ = state;
}

XMFLOAT3 Voxel::Colour() {
	XMFLOAT3 voxel_colour = XMFLOAT3(0, 0, 0);
	switch (state_) {
		case kNothing:
			voxel_colour = XMFLOAT3(0.957f, 0.216f, 1.0f);
			break;
		case kWater:
			voxel_colour = XMFLOAT3(0, 0, 1);
			break;
		case kBeach:
			voxel_colour = XMFLOAT3(0.98f, 0.78f, 0.369f);
			break;
		case kRock:
			voxel_colour = XMFLOAT3(0.3f, 0.3f, 0.3f);
			break;
		case kGrass:
			voxel_colour = XMFLOAT3(0, 1, 0);
			break;
		case kShrub:
			voxel_colour = XMFLOAT3(1, 0, 0);
			break;
		case kSnow:
			voxel_colour = XMFLOAT3(1, 1, 1);
			break;
		case kDirt:
			voxel_colour = XMFLOAT3(0.412f, 0.251f, 0.137f);
			break;

	}
	return voxel_colour;
}

void Voxel::AddNeighbour(Voxel * voxel) {
	neighbours_.push_back(voxel);
}

bool Voxel::IsBesideWater() {
	bool isBesideWater = false;
	for each (Voxel* neighbour in neighbours_) {
		if (neighbour->State() == kWater) {
			isBesideWater = true;
		}
	}
	return isBesideWater;
}

bool Voxel::IsLevelArea() {
	bool isLevelArea = true;
	for each (Voxel* neighbour in neighbours_) {
		if (neighbour->Position().y < position_.y) {
			isLevelArea = false;;
		}
	}
	return isLevelArea;
}

bool Voxel::IsHigherThan(float height) {
	if (position_.y > height) {
		return true;
	}
	return false;
}

bool Voxel::IsLoneVoxel(float voxel_size) {
	bool isLoneVoxel = false;
	if (NumberOfNeighboursOneBelow(voxel_size) == neighbours_.size()) {
		isLoneVoxel = true;
	}
	return isLoneVoxel;
}

int Voxel::NumberOfNeighboursOneBelow(float voxel_size) {
	int numberOfNeighboursBelow = 0;
	for each (Voxel* neighbour in neighbours_) {
		if (neighbour->Position().y < position_.y &&
			neighbour->Position().y >(position_.y - voxel_size - kEpsilon)) {
			numberOfNeighboursBelow++;
		}
	}
	return numberOfNeighboursBelow;
}

int Voxel::NumberOfNeighboursOfState(VoxelState state) {
	int numberOfAliveNeighbours = 0;
	for each (Voxel* neighbour in neighbours_) {
		if (neighbour->State() == state) {
			numberOfAliveNeighbours++;
		}
	}
	return numberOfAliveNeighbours;
}
