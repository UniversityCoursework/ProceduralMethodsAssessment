#ifndef _CHROMATIC_SHADER_H
#define _CHROMATIC_SHADER_H

#include "../DXFramework/BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Post Proccessing effect of offseting RGB values for Chromatic Aberations.
/// Varying based on distance from centre of screen.
/// </summary>
class ChromaticShader : public BaseShader {
private:
	struct ChromaticAberationBufferType {
		XMFLOAT3 displacement_scale;
		float base_radius;
		float falloff_radius;
		float chroma_power;
		XMFLOAT2 padding;
	};

public:
	/// <summary>
	/// Initialises and loads the Vertex, and Pixel Shaders.
	/// </summary>	
	ChromaticShader(ID3D11Device* device, HWND hwnd);

	/// <summary>
	/// Clears pointers and releases all buffers.
	/// </summary>
	~ChromaticShader();
	
	/// <summary>
	/// Loads information into the Matrix and Chromatic Buffers.
	/// And connects the buffers to the correct shader pipeline.
	/// </summary>
	/// <param name="render_texture"> Render texture that will be blurred. </param>	
	/// <param name="displacement_scale"> The amount of Displacement in RGB Values for the Chromatic Aberation Effect.</param>
	/// <param name="base_radius"> The lower limit of the effect. </param>
	/// <param name="falloff_radius"> The upper limit of the effects. </param>
	/// <param name="chroma_power"> Power that amplifies the displacement amounts. </param>
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* render_texture, XMFLOAT3 displacement_scale, float base_radius, float falloff_radius, float chroma_power);

	/// <summary>
	/// Renders the previous sent Vertex buffer of Index Count.
	/// Renders using DrawIndexed, and sets sampler state information
	/// </summary>	
	/// <param name="index_count"> The number of indices in the vertex buffer. </param>
	void Render(ID3D11DeviceContext* device_context, int index_count);
private:

	/// <summary>
	/// Loads the shaders, and initialises the shader buffers descriptions.	
	/// Sets up matrix buffer, chromatic buffer, and texture sampler.
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader to load. </param>
	/// <param name="ps_filename"> Pixel Shader to load. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* ps_filename);

private:
	ID3D11Buffer* chromatic_buffer_;

	ID3D11SamplerState* sample_state_;
};

#endif
