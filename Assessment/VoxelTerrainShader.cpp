#include "VoxelTerrainShader.h"

VoxelTerrainShader::VoxelTerrainShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"shaders/voxel_terrain_vs.hlsl", L"shaders/voxel_terrain_gs.hlsl", L"shaders/voxel_terrain_ps.hlsl");
}

VoxelTerrainShader::~VoxelTerrainShader() {
	//Release base shader components
	BaseShader::~BaseShader();

	// Release the light constant buffer.
	if (light_buffer_) {
		light_buffer_->Release();
		light_buffer_ = 0;
	}

	// Release the camera constant buffer.
	if (camera_buffer_) {
		camera_buffer_->Release();
		camera_buffer_ = 0;
	}

	// Release the voxel settings buffer.
	if (voxel_settings_buffer_) {
		voxel_settings_buffer_->Release();
		voxel_settings_buffer_ = 0;
	}
}

void VoxelTerrainShader::InitShader(WCHAR* vs_filename, WCHAR* ps_filename) {
	D3D11_BUFFER_DESC matrix_buffer_description;
	D3D11_BUFFER_DESC voxel_settings_buffer_description;
	D3D11_BUFFER_DESC light_buffer_description;
	D3D11_BUFFER_DESC camera_buffer_description;

	// Load (+ compile) shader files
	LoadVertexShader(vs_filename);
	LoadPixelShader(ps_filename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrix_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	matrix_buffer_description.ByteWidth = sizeof(MatrixBufferType);
	matrix_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrix_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrix_buffer_description.MiscFlags = 0;
	matrix_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&matrix_buffer_description, NULL, &matrix_buffer_);


	// Setup Terrain Settings description.
	voxel_settings_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	voxel_settings_buffer_description.ByteWidth = sizeof(VoxelSettingsType);
	voxel_settings_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	voxel_settings_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	voxel_settings_buffer_description.MiscFlags = 0;
	voxel_settings_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the constant buffer from within this class.
	device_->CreateBuffer(&voxel_settings_buffer_description, NULL, &voxel_settings_buffer_);


	// Setup Light Buffer Description
	light_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	light_buffer_description.ByteWidth = sizeof(LightBufferType);
	light_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	light_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	light_buffer_description.MiscFlags = 0;
	light_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&light_buffer_description, NULL, &light_buffer_);


	// Setup the Camera Buffer Description
	camera_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	camera_buffer_description.ByteWidth = sizeof(CameraBufferType);
	camera_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	camera_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	camera_buffer_description.MiscFlags = 0;
	camera_buffer_description.StructureByteStride = 0;
	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&camera_buffer_description, NULL, &camera_buffer_);

}

void VoxelTerrainShader::CreateInputBufferLayout(ID3DBlob * vertex_shader_buffer) {
	HRESULT result;
	D3D11_INPUT_ELEMENT_DESC polygon_layout[5];
	unsigned int number_of_elements;
	// Create the vertex input layout description.
	// This Matches the TerrainVoxelType stucture in the OldVoxelTerrainMesh.
	polygon_layout[0].SemanticName = "POSITION";
	polygon_layout[0].SemanticIndex = 0;
	polygon_layout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[0].InputSlot = 0;
	polygon_layout[0].AlignedByteOffset = 0;
	polygon_layout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[0].InstanceDataStepRate = 0;

	// For Storing the voxels colour.
	polygon_layout[1].SemanticName = "COLOR";
	polygon_layout[1].SemanticIndex = 0;
	polygon_layout[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[1].InputSlot = 0;
	polygon_layout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygon_layout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[1].InstanceDataStepRate = 0;

	// Extra Layout for storing 4 neighbour heights.
	polygon_layout[2].SemanticName = "HEIGHTS";
	polygon_layout[2].SemanticIndex = 0;
	polygon_layout[2].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygon_layout[2].InputSlot = 0;
	polygon_layout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygon_layout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[2].InstanceDataStepRate = 0;

	// For instance data for instanced rendering.
	polygon_layout[3].SemanticName = "INSTANCE";
	polygon_layout[3].SemanticIndex = 0;
	polygon_layout[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[3].InputSlot = 1;
	polygon_layout[3].AlignedByteOffset = 0;
	polygon_layout[3].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	polygon_layout[3].InstanceDataStepRate = 1;

	// For instance count for instanced rendering.
	polygon_layout[4].SemanticName = "INSTANCECOUNT";
	polygon_layout[4].SemanticIndex = 0;
	polygon_layout[4].Format = DXGI_FORMAT_R32_UINT;
	polygon_layout[4].InputSlot = 1;
	polygon_layout[4].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygon_layout[4].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	polygon_layout[4].InstanceDataStepRate = 1;

	// Get a count of the elements in the layout.
	number_of_elements = sizeof(polygon_layout) / sizeof(polygon_layout[0]);

	// Create the vertex input layout.
	result = device_->CreateInputLayout(polygon_layout, number_of_elements, vertex_shader_buffer->GetBufferPointer(), vertex_shader_buffer->GetBufferSize(),
		&layout_);
	if (FAILED(result)) {
		//return false;
	}
}

void VoxelTerrainShader::InitShader(WCHAR* vs_filename, WCHAR* gs_filename, WCHAR* ps_filename) {
	// Load vertex and pixel shaders + setup buffers
	InitShader(vs_filename, ps_filename);

	// Load Geometry Shader as required for this shader.
	LoadGeometryShader(gs_filename);
}

void VoxelTerrainShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix,
	const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
	float voxel_size,
	Light* light[MAXLIGHTS], XMFLOAT3 camera_position) {

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mapped_resource;
	MatrixBufferType* data_ptr;
	LightBufferType* light_ptr;
	CameraBufferType* camera_ptr;
	VoxelSettingsType* voxel_settings_ptr;
	unsigned int buffer_number;
	XMMATRIX transposed_world_matrix, transposed_view_matrix, transposed_projection_matrix;

	// Send Matrix Data to the Geometry Shader.
	// Transpose the matrices to prepare them for the shader.
	transposed_world_matrix = XMMatrixTranspose(world_matrix);
	transposed_view_matrix = XMMatrixTranspose(view_matrix);
	transposed_projection_matrix = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	data_ptr = (MatrixBufferType*)mapped_resource.pData;

	// Copy the matrices into the constant buffer.
	data_ptr->world = transposed_world_matrix;
	data_ptr->view = transposed_view_matrix;
	data_ptr->projection = transposed_projection_matrix;

	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	buffer_number = 0;

	// Now set the constant buffer in the geometry shader with the updated values.
	device_context->GSSetConstantBuffers(buffer_number, 1, &matrix_buffer_);

	/** Additional **/


	// Send Voxel Terrain Settings to Geometry Shader.

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(voxel_settings_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	voxel_settings_ptr = (VoxelSettingsType*)mapped_resource.pData;
	voxel_settings_ptr->size = voxel_size;
	voxel_settings_ptr->padding = { 0, 0, 0 };

	// Unlock the constant buffer.
	device_context->Unmap(voxel_settings_buffer_, 0);
	buffer_number = 1;

	// Now set the constant buffer in the Geometry shader.
	device_context->GSSetConstantBuffers(buffer_number, 1, &voxel_settings_buffer_);


	// Send camera data to vertex shader

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(camera_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	camera_ptr = (CameraBufferType*)mapped_resource.pData;
	camera_ptr->camera_position = camera_position;
	camera_ptr->padding = 0.0f;

	// Unlock the constant buffer.
	device_context->Unmap(camera_buffer_, 0);

	buffer_number = 2;
	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->GSSetConstantBuffers(buffer_number, 1, &camera_buffer_);


	// Send light data to pixel shader

	// Lock the light to the constant buffer so it can be written to.
	device_context->Map(light_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// get a pointer to the data in the constant buffer
	light_ptr = (LightBufferType*)mapped_resource.pData;

	for (int i = 0; i < MAXLIGHTS; i++) {
		// Copy light data into the buffer		
		light_ptr->ambient[i] = light[i]->GetAmbientColour();
		light_ptr->diffuse[i] = light[i]->GetDiffuseColour();
		light_ptr->specular[i] = light[i]->GetSpecularColour();
		light_ptr->position[i] = XMFLOAT4(light[i]->GetPosition().x, light[i]->GetPosition().y, light[i]->GetPosition().z, light[i]->GetSpecularPower());
		light_ptr->attenuation[i] = XMFLOAT4(light[i]->GetConstantFactor(), light[i]->GetLinearFactor(), light[i]->GetQuadraticFactor(), light[i]->GetRange());
		light_ptr->direction[i] = XMFLOAT4(light[i]->GetDirection().x, light[i]->GetDirection().y, light[i]->GetDirection().z, 0);
		light_ptr->type[i] = XMINT4((int)light[i]->GetType(), 0, 0, 0);
	}

	// unlock the buffer
	device_context->Unmap(light_buffer_, 0);
	buffer_number = 0;

	// set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(buffer_number, 1, &light_buffer_);

}
