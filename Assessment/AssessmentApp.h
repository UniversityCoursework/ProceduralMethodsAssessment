#ifndef _ASSESSMENT_APP_H_
#define _ASSESSMENT_APP_H_

#include "../DXFramework/BaseApplication.h"
#include "../DXFramework/RenderTexture.h"
#include "../DXFramework/SphereMesh.h"
#include "../DXFramework/OrthoMesh.h"
#include "D3D.h"

#include "../DXFramework/LightGizmoShader.h"
#include "../DXFramework/MultiLightShader.h"

#include "../DXFramework/GameObject.h"
#include "../DXFramework/LightObject.h"

#include <vector>

#include "VoxelTerrainObject.h"
#include "PostProcessObject.h"
#include "CellularAutomataTerrain.h"
#include "FractionalBrownianMotionTerrain.h"

#include "VoxelTerrainShader.h"
#include "ChromaticShader.h"
#include "GaussianBlurShader.h"

/// <summary>
/// Procedural Methods Assessment App
/// <para> - Based On Graphics Programming Framework.</para>
/// <para> - Expanded to showcase procedural methods.</para>
/// <para> - fBM Noise Terrain, using perlin noise. </para>
/// <para> - Terrain Color based on height and slope, and surronding voxels data ( multiple parameters).</para>
/// <para> - Cellular Automata cave generation. </para>
/// <para> - Post Proccesses; Gaussian Blur, chromatic aberation.</para>
/// </summary>
class AssessmentApp : public BaseApplication {
public:
	AssessmentApp();
	~AssessmentApp();
	void Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input* input);

	bool Frame();
protected:
	bool Render();
private:
	/// <summary>
	/// Renders the default scene to the scene render texture.
	/// </summary>
	void RenderSceneToTexture();

	/// <summary>
	/// The default scene rendered normally.
	/// </summary>
	void RenderScene();

	/// <summary>
	/// Modifies the scene render target using the chromatic aberation shader.
	/// Then renders to a full screen ortho mesh.
	/// </summary>
	void RenderChromaticAberation();

	/// <summary>
	/// Modifies the scene render target using the gaussian blur shader with a horizontal and vertical blur.
	/// Then renders to a full screen ortho mesh.
	/// </summary>
	void RenderGaussianBlur();

	/// <summary>
	/// Handles all of the ImGui Ui rendering.
	/// </summary>
	void RenderImGui();

	/// <summary>
	/// Updates the editor Gizmos, used for showing position of something that doesnt have a mesh.
	/// Currently just updates the light gizmo's to match their lights.
	/// </summary>
	void UpdateGizmos();

	/// <summary>
	/// Renders the Editor Gizmos.
	/// Currently just renders the light gizmos, as a sphere with colour matched to the diffuse colour
	///  of the light.
	/// </summary>
	void RenderGizmos();

	bool show_editor_outliner_;
	bool show_editor_gizmos_;

	// Meshes
	SphereMesh* sphere_mesh_;
	OrthoMesh* orth_mesh_;

	CellularAutomataTerrain* cellular_automata_mesh_;
	FractionalBrownianMotionTerrain* fractional_brownian_motion_mesh_;

	// Shaders
	LightGizmoShader* light_gizmo_shader_;
	MultiLightShader* light_shader_;
	VoxelTerrainShader* voxel_terrain_shader_;
	ChromaticShader* chromatic_shader_;
	GaussianBlurShader* gaussian_blur_shader_;

	// Render targets for post processing.
	RenderTexture* scene_render_texture_;

	RenderTexture* gaussian_first_pass_texture_;

	// array of lights for the light shaders.
	Light* lights_[MAXLIGHTS];

	// Game objects.
	GameObject* floating_orb_;
	VoxelTerrainObject* terrain_;
	PostProcessObject* post_process_;

	// Simple Wrapper for drawing light Widgets
	std::vector<LightObject*> editor_lights_;

	// Used for passing all modifiable objects to editor.
	std::vector<GameObject*> gameobjects_;

	XMFLOAT4 scene_background_colour_;
};

#endif // !_ASSESSMENT_APP_H_