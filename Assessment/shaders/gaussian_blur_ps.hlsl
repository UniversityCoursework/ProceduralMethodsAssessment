/// <summary>
/// Gaussian Blur Pixel Shader.
/// Blurs using normailized gaussian values, in a horizontal, vertical 2 pass system.
/// Deviation gives a pseudo value for stronger gaussian values, produced artifacts at higher values.
/// </summary>

Texture2D scene_texture : register(t0);

SamplerState texture_sampler : register(s0);

cbuffer GaussianBlurBuffer : register(cb0) {
	int screen_width;
	int screen_height;

	/// <summary>
	/// 0 - Vertical
	/// 1 - Horiztontal
	/// </summary>
	int direction;
	float deviation;
}

struct InputType {
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
};


float4 main(InputType input) : SV_TARGET {
	float weight[] = {
		0.2270270270f, 0.1945945946f, 0.1216216216f,
	0.0540540541f, 0.0162162162f
	};
	
	float4 colour = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float texel_width = 0;
	float texel_height = 0;
	float weight_normalisation = 0;
	float total_weights = 0;
	int i = 0;

	// calculate weight normalizer
	for (i = 1; i < 5; i++) {
		total_weights += weight[i];
	}
	weight_normalisation = weight[0] + 2.0f * total_weights;

	// normalize all weightings
	for (i = 0; i < 5; i++) {
		weight[i] = weight[i] / weight_normalisation;
	}

	// Add Centre pixel
	colour += scene_texture.Sample(texture_sampler, input.tex) * weight[0];

	// vertical
	if (direction == 0) {
		texel_width = deviation * (1.0f / screen_width);
	}
	// horizontal 
	if (direction == 1) {
		texel_height = deviation * (1.0f / screen_height);
	}
	
	// Add left and right pixels
	for (i = 1; i < 5; i++) {
		colour +=
		scene_texture.Sample(texture_sampler, input.tex + float2(texel_width * (float) i, texel_height * (float) i)) * weight[i] +
		scene_texture.Sample(texture_sampler, input.tex - float2(texel_width * (float) i, texel_height * (float) i)) * weight[i];
	}
	colour.a = 1.0f;
	return colour;
}