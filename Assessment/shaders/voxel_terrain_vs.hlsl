/// <summary>
/// Voxel Terrain Vertex Shader.
/// Passes information to other stages offset by instance positions.
/// </summary>

struct InputType {
	float3 position : POSITION;
	float3 color : COLOR0;
	float4 neighbour_heights : HEIGHTS0;
	float3 instance_position : INSTANCE0;
	int instance_count : INSTANCECOUNT0;
};

struct OutputType {
	float4 position : POSITION;
	float3 color : COLOR0;
	float4 neighbour_heights : HEIGHTS0;
	int instance_count : INSTANCECOUNT0;
};

OutputType main(InputType input) {
	OutputType output;
	 // Update the position of the vertices based on the data for this particular instance.
	output.position.x = input.position.x + input.instance_position.x;
	output.position.y = input.position.y + input.instance_position.y;
	output.position.z = input.position.z + input.instance_position.z;
	output.position.w = 1.0f;
	output.neighbour_heights = input.neighbour_heights;	
	output.color = input.color;
	output.instance_count = input.instance_count;
	// Pass info to the geometry shader.
	return output;
}