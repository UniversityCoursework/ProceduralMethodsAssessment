/// <summary>
/// Voxel Terrain Pixel Shader.
/// Renders voxels with bling-phong shading.
/// Uses voxel colour as basis instead of textures.
/// </summary>

// If changed make sure is also changed in Light shader, and enough lights are set up. 
// (Even if just turned off.)
#define MAXLIGHTS 3

cbuffer LightBuffer : register(cb0) {
	float4 ambient_color[MAXLIGHTS];
	float4 diffuse_color[MAXLIGHTS];
	float4 specular_color[MAXLIGHTS];
	// position.xyz, specularpower.w
	float4 light_position[MAXLIGHTS];
	// attenuation
	// constant,linear,quad,range
	float4 attenuation[MAXLIGHTS];
	float4 direction[MAXLIGHTS];
	int4 type[MAXLIGHTS];
};

struct InputType {
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float3 color : COLOR0;
	float3 view_direction : COLOR1;
	float3 world_space_position : COLOR2;
};

float4 main(InputType input) : SV_TARGET {
		
	float4 voxel_colour = float4(input.color, 1.f);
	float light_intensity;
	float3 light_direction;
	float4 light_color = float4(0.f, 0.f, 0.f, 1.f);
	float3 reflection;
	float3 halfway_direction;
	float4 specular;
	float4 final_specular = float4(0.f, 0.f, 0.f, 1.f);
	float4 diffuse_contribution = float4(0.f, 0.f, 0.f, 1.f);

	float distance;
	float final_attenuation;
		
	for (int i = 0; i < MAXLIGHTS; i++) {

		switch (type[i].x) {
			case 0: // kAmbient
				{
					light_color += ambient_color[i];
					break;
				}
			case 1: //kPoint
				{
					light_direction = input.world_space_position - light_position[i].xyz;
					distance = length(light_direction);
					if (distance < attenuation[i].w) {
						light_direction = normalize(light_direction);
						// Calculate the amount of light on this pixel.
						light_intensity = dot(input.normal, -light_direction);
						if (light_intensity > 0.0f) {
							
							light_intensity = saturate(light_intensity);
							// Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
							diffuse_contribution = (diffuse_color[i] * light_intensity);
							// calculate attenuation
							final_attenuation = 1.0f / (attenuation[i].x + attenuation[i].y * distance + attenuation[i].z * pow(distance, 2));
							// Reduce the diffuse contribution by the attenuation value.
							light_color += (diffuse_contribution * final_attenuation);
							if (light_position[i].w > 0) {
								// blinn-phong			
								halfway_direction = normalize(-light_direction + input.view_direction);
								specular = pow(saturate(dot(halfway_direction, input.normal)), light_position[i].w);

								specular *= light_intensity;
								// sum up specular light
								final_specular += ((specular_color[i] * specular) * final_attenuation);
							}
						}
					}
					break;
				}
			case 2: //kDirectional
				{
					light_direction = normalize(direction[i].xyz);
					// Calculate the amount of light on this pixel.
					light_intensity = dot(input.normal, -light_direction);
					if (light_intensity > 0.0f) {
						
						light_intensity = saturate(light_intensity);
						// Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
						light_color += (diffuse_color[i] * light_intensity);
						if (light_position[i].w > 0) {
							// blinn-phong			
							halfway_direction = normalize(-light_direction + input.view_direction);
							specular = pow(saturate(dot(halfway_direction, input.normal)), light_position[i].w);

							specular *= light_intensity;
							// sum up specular light
							final_specular += ((specular_color[i] * specular));
						}
					}
					break;
				}
			case 3: //kOff
				break;
		}
	}
	// add the Voxel colour
	light_color = light_color * voxel_colour;
	// add specular
	light_color += final_specular;
	return light_color;
}