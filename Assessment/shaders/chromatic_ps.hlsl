/// <summary>
/// Chromatic Aberation Pixel Shader.
/// Offsets RGB channels based on distance from centre of screen.
/// With extreme values can give interesting results.
/// </summary>

Texture2D scene_texture : register(t0);

SamplerState texture_sampler : register(s0);

cbuffer ChromaticBuffer : register(cb0) {
    float3 displacement_scale;
    float base_radius;
    float falloff_radius;
    float chromatic_power;
    float2 padding;
};


struct InputType {
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

float4 main(InputType input) : SV_TARGET {
    float4 textureColor;

    // Calculate distance from centre.
    float dist = distance(input.tex, float2(0.5, 0.5));
    // Lerp between base radius and falloff, clamping to these values.
    float f = smoothstep(base_radius, falloff_radius, dist);
    // Calculate Chroma rgb, .
    float3 chroma = pow(abs(f + displacement_scale), abs(chromatic_power));
       
    // Offset texture coords by chroma, and ensure still valid UV coords. 
    // i.e. centred on the centre of the screen, but each  rgb is offset dependent on screen position.
    float2 tr = ((2.0 * input.tex - 1.0) * chroma.r) * 0.5 + 0.5;
    float2 tg = ((2.0 * input.tex - 1.0) * chroma.g) * 0.5 + 0.5;
    float2 tb = ((2.0 * input.tex - 1.0) * chroma.b) * 0.5 + 0.5;

    // Sample texture and new UV coords, for rgb.
    textureColor.r = scene_texture.Sample(texture_sampler, tr).r;
    textureColor.g = scene_texture.Sample(texture_sampler, tg).g;
    textureColor.b = scene_texture.Sample(texture_sampler, tb).b;

    // Makes sure alpha is full.
    textureColor.a = 1.0f;
    return textureColor;
}