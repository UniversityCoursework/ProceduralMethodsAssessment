/// <summary>
/// Voxel Terrain Geometry Shader.
/// Generates voxel cubes with normals and culled faces based on surronding neighbour heights.
/// </summary>

cbuffer MatrixBuffer : register(cb0) {
	matrix world_matrix;
	matrix view_matrix;
	matrix projection_matrix;
};

cbuffer VoxelSettingsType : register(cb1) {	
	float size;
	float3 padding;
};

cbuffer CameraBuffer : register(cb2) {
	float3 camera_position;
	float camera_padding;
};

struct InputType {
	float4 position : POSITION;
	float3 color : COLOR0;
	float4 neighbour_heights : HEIGHTS0;
	int instance_count : INSTANCECOUNT0;
};

struct OutputType {
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float3 color : COLOR0;
	float3 view_direction : COLOR1;
	float3 world_space_position : COLOR2;
};

[maxvertexcount(36)]
void main(point InputType input[1], inout TriangleStream<OutputType> triStream) {	
	// if lower than mesh 0 (Done before world matrix etc calculations.)
	if (input[0].position.y < 0) {
		return;
	}
	// voxel layout of visible faces.
	const float3 cube_layout[] = {
		// top face
		{ 0.5f, 0.5f, -0.5f },
		{ -0.5f, 0.5f, 0.5f },
		{ -0.5f, 0.5f, -0.5f },
		{ 0.5f, 0.5f, -0.5f },
		{ 0.5f, 0.5f, 0.5f },
		{ -0.5f, 0.5f, 0.5f },
		// north face
		{ -0.5f, 0.5f, -0.5f },
		{ -0.5f, -0.5f, -0.5f },
		{ 0.5f, -0.5f, -0.5f },
		{ -0.5f, 0.5f, -0.5f },
		{ 0.5f, -0.5f, -0.5f },
		{ 0.5f, 0.5f, -0.5f },
		// south face
		{ 0.5f, 0.5f, 0.5f },
		{ -0.5f, -0.5f, 0.5f },
		{ -0.5f, 0.5f, 0.5f },
		{ 0.5f, 0.5f, 0.5f },
		{ 0.5f, -0.5f, 0.5f },
		{ -0.5f, -0.5f, 0.5f },
		// east face
		{ 0.5f, 0.5f, -0.5f },
		{ 0.5f, -0.5f, -0.5f },
		{ 0.5f, -0.5f, 0.5f },
		{ 0.5f, 0.5f, -0.5f },
		{ 0.5f, -0.5f, 0.5f },
		{ 0.5f, 0.5f, 0.5f },
		// west face
		{ -0.5f, 0.5f, 0.5f },
		{ -0.5f, -0.5f, -0.5f },
		{ -0.5f, 0.5f, -0.5f },
		{ -0.5f, 0.5f, 0.5f },
		{ -0.5f, -0.5f, 0.5f },
		{ -0.5f, -0.5f, -0.5f },
		
	};

	const float3 normal_layout[] = {
		// top face
		{ 0, 1, 0 },
		// north face
		{ 0, 0, 1 },
		// south face
		{ 0, 0, -1 },
		// east face
		{ 1, 0, 0 },
		// west face
		{ -1, 0, 0 },
		
	};
		
	OutputType output;

	int pos_i = 0;

	float4 world_position;

	// Add top face to first instance.
	if (input[0].instance_count == 0) {
		// Add the triangles of the face.
		for (int triangles = 0; triangles < 2; triangles++) {
			// Add the vertices of the face.
			for (int verts = 0; verts < 3; verts++) {

				// Calculate the position of the vertex against the world, view, and projection matrices.
				// Offset by cube_layout faces and voxel size.
				world_position = mul(input[0].position + float4(cube_layout[pos_i] * size, 0.0f), world_matrix);

				output.position = mul(world_position, view_matrix);
				output.position = mul(output.position, projection_matrix);
				output.normal = normal_layout[0];
				output.color = input[0].color;
				output.view_direction = normalize(camera_position.xyz - world_position.xyz);
				output.world_space_position = world_position.xyz;
				triStream.Append(output);
				
				pos_i++;
			}
			triStream.RestartStrip();
		}
	}
	// Add other faces that are visible.
	for (int faces = 1; faces < 5; faces++) {
		// Ensures on correct pos_i for cube_layout, even when previous face not rendered.
		pos_i = faces * 6;
		// Determine if the face will be visible.
		if (input[0].position.y > input[0].neighbour_heights[faces - 1]) {
			// Add the triangles of the face.
			for (int triangles = 0; triangles < 2; triangles++) {
				// Add the vertices of the face.
				for (int verts = 0; verts < 3; verts++) {

					// Calculate the position of the vertex against the world, view, and projection matrices.
					world_position = mul(input[0].position + float4(cube_layout[pos_i] * size, 0.0f), world_matrix);

					output.position = mul(world_position, view_matrix);
					output.position = mul(output.position, projection_matrix);
					// Add normals from layout, uses face value, as normals shared for face.
					output.normal = normal_layout[faces];
					output.color = input[0].color;
					output.view_direction = normalize(camera_position.xyz - world_position.xyz);					
					output.world_space_position = world_position.xyz;
					triStream.Append(output);
				
					pos_i++;
				}
				triStream.RestartStrip();
			}
		}
	}
}