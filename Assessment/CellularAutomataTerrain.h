#ifndef _CELLULAR_AUTOMATA_TERRAIN_H_
#define _CELLULAR_AUTOMATA_TERRAIN_H_

#include "VoxelTerrainMesh.h"

#include <random>

/// <summary>
/// Terrain Generation Using Cellular Automata with a Moore Neighbourhood.
/// The terrain is generated based on death and birth thresholds with an initial chance of birth
/// used from a mt19937 random number engine with a specific seed.
/// <para> See <see cref="VoxelTerranObject"/> for how settings are exposed to ImGui Editor.</para>
/// </summary>
class CellularAutomataTerrain : public VoxelTerrainMesh {
public:

	/// <summary>
	/// Generate the terrain and sets up the default parameters.
	/// Sets the neighbourhood to Moore as required for CA.
	/// Gives it an initial seed that produces decent results.
	/// </summary>
	/// <param name="grid_size"> Grid size of Points used for terrains x/y. </param>
	CellularAutomataTerrain(ID3D11Device* device, ID3D11DeviceContext* device_context, int grid_size = 128);

	/// <summary>
	/// Clears pointers and releases all buffers.
	/// </summary>
	~CellularAutomataTerrain();

	/// <summary>
	/// Threshold number of neighbours for an Alive voxel to die.
	/// </summary>
	int GetDeathThreshold();

	/// <summary>
	/// Overrides current death threshold clamping to a positive number.
	/// </summary>
	/// <param name="death_threshold"> Number of neighbours for an Alive voxel to die</param>
	void SetDeathThreshold(int death_threshold);

	/// <summary>
	/// Threshold number of neighbours for a dead voxel to be born.
	/// </summary>
	int GetBirthThreshold();

	/// <summary>
	/// Overrides current birth threshold clamping to a positive number.
	/// </summary>
	/// <param name="birth_threshold"> Number of neighbours for a dead voxel to be born. </param>
	void SetBirthThreshold(int birth_threshold);

	/// <summary>
	/// How many times to run the cellular automata over the terrain data.
	/// </summary>
	int GetNumberOfIterations();
		
	/// <summary>
	/// Overrides the current number of iterations clamping to a number greater than 1.
	/// </summary>
	/// <param name="number_of_iterations"> The number of iterations to run. </param>
	void SetNumberOfIterations(int number_of_iterations);
	
	/// <summary>
	/// The initial chance of a voxel being alive at the start.
	/// </summary>
	float GetInitialChanceOfBirth();
	
	/// <summary>
	/// Overrides the chance of birth clamping to a positive number.
	/// </summary>
	/// <param name="chance_of_birth"> The percentage chance of birth, (1.0f = 100%)</param>
	void SetInitialChanceOfBirth(float chance_of_birth);

	/// <summary>
	/// Random Seed used in mt1997 for intial birth chances.
	/// </summary>
	int GetRandomSeed();
	
	/// <summary>
	/// Overrides the current seed clamping to a positive number.
	/// </summary>
	/// <param name="seed"> Random Seed for intial birth chances.</param>
	void SetRandomSeed(int seed);

	/// <summary>
	/// Moves the terrain map data forward one iteration step in the Cellular Automata.
	/// This will reinitialize the buffers to update the mesh data.
	/// </summary>
	void DoIterationStep(ID3D11Device * device);

private:

	/// <summary>
	/// Generates the heightmap data.
	/// Using intial birth chance to create intial births,
	/// then iterates over the data for number of iterations using  <see cref="IterationStep"/>.
	/// </summary>
	void GenerateHeightMap() override;

	/// <summary>
	/// Moves the terrain map data forward one iteration step in the Cellular Automata.
	/// </summary>
	/// <param name="temp_voxel_map"> 
	/// Voxel Array used to temporarily store the height map data.
	/// This should be the same size as the current height map data.
	/// Voxel* temp_height_map_ = new Voxel[terrain_width_ * terrain_height_];
	/// This is passed in to allow the declartion to be done once, if needing to do multiple steps.	
	/// The calling function should handle deleting the temporary array.
	///  </param>
	void IterationStep(Voxel* temp_voxel_map);

private:
	// random number engine used to generate accurate random number generation.
	std::mt19937 num_engine_;

	int death_threshold_;
	int birth_threshold_;
	int number_of_iterations_;
	float initial_chance_of_birth_;
	int random_seed_;
};

#endif // !_CELLULAR_AUTOMATA_TERRAIN_H_

