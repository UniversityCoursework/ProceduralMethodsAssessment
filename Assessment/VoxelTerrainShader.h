#ifndef _VOXEL_TERRAIN_SHADER_H_
#define _VOXEL_TERRAIN_SHADER_H_

#include "../DXFramework/BaseShader.h"
#include "../DXFramework/MultiLightBuffers.h"
#include "../DXFramework/Light.h"

#include "VoxelTerrainMesh.h"

using namespace DirectX;

/// <summary>
/// Voxel Terrain Shader, handles rendering the point information into instanced voxel cubes.
/// </summary>
class VoxelTerrainShader : public BaseShader {

private:
	/// <summary>
	/// Voxel Settings Buffer Struture.
	/// Used to send settings to the shaders.
	/// </summary>
	struct VoxelSettingsType {
		float size;
		XMFLOAT3 padding;
	};

public:

	/// <summary>
	/// Stores the device and windows handle, and initializes the shaders.
	/// </summary>
	VoxelTerrainShader(ID3D11Device* device, HWND hwnd);
	~VoxelTerrainShader();

	/// <summary>
	/// Loads all the shader information into the shaders buffers.
	/// </summary>
	/// <param name="terrain_settings"> Terraing Settings of the mesh to be rendered. </param>
	/// <param name="light"> Array of all the lights to be rendered. </param>
	/// <param name="camera_position"> Position of the camera in world space. </param>
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix,
		const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		float voxel_size,
		Light* light[MAXLIGHTS], XMFLOAT3 camera_position);

private:
	/// <summary>
	/// Ensures all the shaders are loaded.
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader. </param>
	/// <param name="gs_filename"> Geometry Shader. </param>
	/// <param name="ps_filename"> Pixel Shader. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* gs_filename, WCHAR* ps_filename);

	/// <summary>
	/// Creates all the Buffer descriptions, and loads the Vertex, and Pixel Shaders.
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader. </param>
	/// <param name="ps_filename"> Pixel Shader. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* ps_filename);

	/// <summary>
	/// Creates the custom Input buffer layout for this shader.
	/// This is due to the shader requiring instancing, and extra information per point.
	/// </summary>
	/// <param name="vertex_shader_buffer"></param>
	void CreateInputBufferLayout(ID3DBlob* vertex_shader_buffer) override;

private:
	ID3D11Buffer* voxel_settings_buffer_;
	ID3D11Buffer* light_buffer_;
	ID3D11Buffer* camera_buffer_;
};

#endif // !_VOXEL_TERRAIN_SHADER_H_
