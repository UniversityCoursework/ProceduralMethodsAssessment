#ifndef _BASEAPPLICATION_H_
#define _BASEAPPLICATION_H_

const bool kFullScreen = false;
const bool kWindowedFullScreen = false;
const bool kVSyncEnabled = true;
const float kScreenDepth = 200.0f;	// 1000.0f
const float kScreenNear = 0.1f;		//0.1f

#include "input.h"
#include "d3d.h"
#include "camera.h"
#include "timer.h"

/// <summary>
/// Contains base application functionality. For inheritance purposes.
/// </summary>
class BaseApplication {
public:

	BaseApplication();
	~BaseApplication();

	/// <summary>
	/// Initialises the Base Application Functionality.
	/// Sets up the direct_3d instance, initialise the camera, timer, and ImGui.
	/// </summary>
	virtual void Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input *input);

	/// <summary>
	/// Proccess the game frame, handling Input and updating the timer.
	/// </summary>
	virtual bool Frame();

protected:
	/// <summary>
	/// Handles the input for the default camera.
	/// </summary>
	virtual void HandleInput(float frame_time);

	/// <summary>
	/// Renders the 3d scene.
	/// </summary>
	virtual bool Render() = 0;

protected:
	HWND wnd_;							// Handle to the window.
	int screen_width_, screen_height_;	// Screen Width/Height
	int mouse_delta_x_, mouse_delta_y_;	// For mouse Movement.
	POINT cursor_;						// Used for converting mouse coordinates to client screen space
	Input* input_;						// input class for handling input.
	D3D* direct_3d_;
	Camera* camera_;
	Timer* timer_;
};

#endif