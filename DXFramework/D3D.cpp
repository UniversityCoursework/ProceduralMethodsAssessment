#include "d3d.h"

D3D::D3D(int screen_width, int screen_height, bool vsync, HWND hwnd, bool fullscreen, float screen_depth, float screen_near) {
	IDXGIFactory* factory;
	IDXGIAdapter* adapter;
	IDXGIOutput* adapter_output;
	unsigned int number_of_modes, i, numerator, denominator, string_length;
	DXGI_MODE_DESC* display_mode_list;
	DXGI_ADAPTER_DESC adapter_description;
	DXGI_SWAP_CHAIN_DESC swap_chain_description;
	D3D_FEATURE_LEVEL feature_level;
	ID3D11Texture2D* back_buffer_ptr;
	float field_of_view, screen_aspect;

	// store initial screen settings
	screen_width_ = screen_width;
	screen_height_ = screen_height;
	screen_depth_ = screen_depth;
	screen_near_ = screen_near;

	// Store the vsync setting.
	vsync_enabled_ = vsync;

	// Create a DirectX graphics interface factory.
	CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

	// Use the factory to create an adapter for the primary graphics interface (video card).
	factory->EnumAdapters(0, &adapter);

	// Enumerate the primary adapter output (monitor).
	adapter->EnumOutputs(0, &adapter_output);

	// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
	adapter_output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &number_of_modes, NULL);

	// Create a list to hold all the possible display modes for this monitor/video card combination.
	display_mode_list = new DXGI_MODE_DESC[number_of_modes];

	// Now fill the display mode list structures.
	adapter_output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &number_of_modes, display_mode_list);


	// Now go through all the display modes and find the one that matches the screen width and height.
	// When a match is found store the numerator and denominator of the refresh rate for that monitor.
	for (i = 0; i < number_of_modes; i++) {
		if (display_mode_list[i].Width == (unsigned int)screen_width) {
			if (display_mode_list[i].Height == (unsigned int)screen_height) {
				numerator = display_mode_list[i].RefreshRate.Numerator;
				denominator = display_mode_list[i].RefreshRate.Denominator;
			}
		}
	}
	// Get the adapter (video card) description.
	adapter->GetDesc(&adapter_description);

	// Store the dedicated video card memory in megabytes.
	video_card_memory_ = (int)(adapter_description.DedicatedVideoMemory / 1024 / 1024);

	// Convert the name of the video card to a character array and store it.
	wcstombs_s(&string_length, video_card_description_, 128, adapter_description.Description, 128);

	// Release the display mode list.
	delete[] display_mode_list;
	display_mode_list = 0;

	// Release the adapter output.
	adapter_output->Release();
	adapter_output = 0;

	// Release the adapter.
	adapter->Release();
	adapter = 0;

	// Release the factory.
	factory->Release();
	factory = 0;

	// Initialize the swap chain description.
	ZeroMemory(&swap_chain_description, sizeof(swap_chain_description));

	// Set to a single back buffer.
	swap_chain_description.BufferCount = 1;

	// Set the width and height of the back buffer.
	swap_chain_description.BufferDesc.Width = screen_width;
	swap_chain_description.BufferDesc.Height = screen_height;

	// Set regular 32-bit surface for the back buffer.
	swap_chain_description.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	// Set the refresh rate of the back buffer.
	if (vsync_enabled_) {
		swap_chain_description.BufferDesc.RefreshRate.Numerator = numerator;
		swap_chain_description.BufferDesc.RefreshRate.Denominator = denominator;
	} else {
		swap_chain_description.BufferDesc.RefreshRate.Numerator = 0;
		swap_chain_description.BufferDesc.RefreshRate.Denominator = 1;
	}

	// Set the usage of the back buffer.
	swap_chain_description.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	// Set the handle for the window to render to.
	swap_chain_description.OutputWindow = hwnd;

	// Turn multisampling off.
	swap_chain_description.SampleDesc.Count = 1;
	swap_chain_description.SampleDesc.Quality = 0;

	// Set to full screen or windowed mode.
	if (fullscreen) {
		swap_chain_description.Windowed = false;
	} else {
		swap_chain_description.Windowed = true;
	}

	// Set the scan line ordering and scaling to unspecified.
	swap_chain_description.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swap_chain_description.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// Discard the back buffer contents after presenting.
	swap_chain_description.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	// Don't set the advanced flags.
	swap_chain_description.Flags = 0;

	// Set the feature level to DirectX 11.
	feature_level = D3D_FEATURE_LEVEL_11_0;

	// Create the swap chain, Direct3D device, and Direct3D device context.
	D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, &feature_level, 1, D3D11_SDK_VERSION, &swap_chain_description, &swap_chain_, &device_, NULL, &device_context_);

	// Get the pointer to the back buffer.
	swap_chain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&back_buffer_ptr);

	// Create the render target view with the back buffer pointer.
	device_->CreateRenderTargetView(back_buffer_ptr, NULL, &render_target_view_);

	// Release pointer to the back buffer as we no longer need it.
	back_buffer_ptr->Release();
	back_buffer_ptr = 0;

	SetBuffers();

	// Setup the viewport for rendering.
	viewport_.Width = (float)screen_width;
	viewport_.Height = (float)screen_height;
	viewport_.MinDepth = 0.0f;
	viewport_.MaxDepth = 1.0f;
	viewport_.TopLeftX = 0.0f;
	viewport_.TopLeftY = 0.0f;

	// Create the viewport.
	device_context_->RSSetViewports(1, &viewport_);

	// Setup the projection matrix.
	field_of_view = (float)XM_PI / 4.0f;
	screen_aspect = (float)screen_width / (float)screen_height;

	// Create the projection matrix for 3D rendering.
	projection_matrix_ = XMMatrixPerspectiveFovLH(field_of_view, screen_aspect, screen_near, screen_depth);

	// Initialize the world matrix to the identity matrix.
	world_matrix_ = XMMatrixIdentity();

	// Create an orthographic projection matrix for 2D rendering.
	ortho_matrix_ = XMMatrixOrthographicLH((float)screen_width, (float)screen_height, screen_near, screen_depth);

	is_wireframe_on_ = false;
}

D3D::~D3D() {
	// Before shutting down set to windowed mode or when you release the swap chain it will throw an exception.
	if (swap_chain_) {
		swap_chain_->SetFullscreenState(false, NULL);
	}
	ReleaseBuffers();

	if (device_context_) {
		device_context_->Release();
		device_context_ = 0;
	}

	if (device_) {
		device_->Release();
		device_ = 0;
	}

	if (swap_chain_) {
		swap_chain_->Release();
		swap_chain_ = 0;
	}
}

void D3D::BeginScene(float red, float green, float blue, float alpha) {
	float color[4];

	// Setup the color to clear the buffer to.
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	// Clear the back buffer.
	device_context_->ClearRenderTargetView(render_target_view_, color);

	// Clear the depth buffer.
	device_context_->ClearDepthStencilView(depth_stencil_view_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void D3D::EndScene() {
	// Present the back buffer to the screen since rendering is complete.
	if (vsync_enabled_) {
		// Lock to screen refresh rate.
		swap_chain_->Present(1, 0);
	} else {
		// Present as fast as possible.
		swap_chain_->Present(0, 0);
	}
}

ID3D11Device* D3D::GetDevice() {
	return device_;
}

ID3D11DeviceContext* D3D::GetDeviceContext() {
	return device_context_;
}

void D3D::GetProjectionMatrix(XMMATRIX& projection_matrix) {
	projection_matrix = projection_matrix_;
}

void D3D::GetWorldMatrix(XMMATRIX& world_matrix) {
	world_matrix = world_matrix_;
}

void D3D::GetOrthoMatrix(XMMATRIX& ortho_matrix) {
	ortho_matrix = ortho_matrix_;
}

void D3D::GetVideoCardInfo(char* card_name, int& memory) {
	strcpy_s(card_name, 128, video_card_description_);
	memory = video_card_memory_;
}

void D3D::TurnZBufferOn() {
	device_context_->OMSetDepthStencilState(depth_stencil_state_, 1);
}

void D3D::TurnZBufferOff() {
	device_context_->OMSetDepthStencilState(depth_disabled_stencil_state_, 1);
}

void D3D::TurnZBufferWriteOn() {
	device_context_->OMSetDepthStencilState(depth_stencil_state_, 1);
}

void D3D::TurnZBufferWriteOff() {
	device_context_->OMSetDepthStencilState(depth_write_disabled_stencil_state_, 1);
}

void D3D::TurnOnAlphaBlending() {
	float blend_factor[4];


	// Setup the blend factor.
	blend_factor[0] = 0.0f;
	blend_factor[1] = 0.0f;
	blend_factor[2] = 0.0f;
	blend_factor[3] = 0.0f;

	// Turn on the alpha blending.
	device_context_->OMSetBlendState(alpha_enable_blending_state_, blend_factor, 0xffffffff);
}

void D3D::TurnOffBlending() {
	float blend_factor[4];


	// Setup the blend factor.
	blend_factor[0] = 0.0f;
	blend_factor[1] = 0.0f;
	blend_factor[2] = 0.0f;
	blend_factor[3] = 0.0f;

	// Turn off the alpha blending.
	device_context_->OMSetBlendState(disable_blending_state_, blend_factor, 0xffffffff);
}

void  D3D::TurnOnAdditiveBlending() {
	float blend_factor[4];


	// Setup the blend factor.
	blend_factor[0] = 0.0f;
	blend_factor[1] = 0.0f;
	blend_factor[2] = 0.0f;
	blend_factor[3] = 0.0f;

	// Turn on the alpha blending.
	device_context_->OMSetBlendState(additive_enable_blending_state_, blend_factor, 0xffffffff);

}

void D3D::SetBackBufferRenderTarget() {
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	device_context_->OMSetRenderTargets(1, &render_target_view_, depth_stencil_view_);
}

void D3D::ResetViewport() {
	// Set the viewport.
	device_context_->RSSetViewports(1, &viewport_);
}

void D3D::ResetShadersNull() {
	device_context_->HSSetShader(NULL, NULL, 0);
	device_context_->DSSetShader(NULL, NULL, 0);
	device_context_->GSSetShader(NULL, NULL, 0);

}

bool D3D::IsWireFrameModeOn() {
	return is_wireframe_on_;
}

void D3D::TurnOnWireframe() {
	is_wireframe_on_ = true;
	// Now set the rasterizer state.
	device_context_->RSSetState(raster_state_wireframe_);
}

void D3D::TurnOffWireframe() {
	is_wireframe_on_ = false;
	// Now set the rasterizer state.
	device_context_->RSSetState(raster_state_);
}

bool D3D::ChangeResolution(int screen_width, int screen_height, HWND hwnd) {
	if (screen_width_ == screen_width && screen_height_ == screen_height) {
		return false;
	}
	screen_width_ = screen_width;
	screen_height_ = screen_height;
	if (!IsFullscreen()) {

		// Place the window in the middle of the screen.
		int position_x = (GetSystemMetrics(SM_CXSCREEN) - screen_width_) / 2;
		int position_y = (GetSystemMetrics(SM_CYSCREEN) - screen_height_) / 2;
		// change size/position of current window
		SetWindowPos(hwnd, 0, position_x, position_y, screen_width_, screen_height_, SWP_NOZORDER | SWP_NOACTIVATE);

	}
	ResizeScreen();
	return true;
}

bool D3D::IsFullScreenWindowed() {
	return is_windowed_fullscreen_;
}

bool D3D::TurnFullScreenWindowedOn(HWND hwnd) {
	if (!is_windowed_fullscreen_) {
		// just incase it is on. turn it off anyway
		TurnFullscreenOff();
		// save full for turn off.
		saved_windowed_fullscreen_width_ = screen_width_;
		saved_windowed_fullscreen_height_ = screen_height_;
		// get the monitors width/height
		screen_width_ = GetSystemMetrics(SM_CXSCREEN);
		screen_height_ = GetSystemMetrics(SM_CYSCREEN);
		// Place the window in the middle of the screen.
		int position_x = (GetSystemMetrics(SM_CXSCREEN) - screen_width_) / 2;
		int position_y = (GetSystemMetrics(SM_CYSCREEN) - screen_height_) / 2;
		// change size/position of current window
		SetWindowPos(hwnd, 0, position_x, position_y, screen_width_, screen_height_, SWP_NOZORDER | SWP_NOACTIVATE);
		ResizeScreen();
		is_windowed_fullscreen_ = true;
		return true;
	}
	return false;
}

bool D3D::TurnFullScreenWindowedOff(HWND hwnd) {
	if (is_windowed_fullscreen_) {
		// get the monitors width/height
		screen_width_ = saved_windowed_fullscreen_width_;
		screen_height_ = saved_windowed_fullscreen_height_;
		// Place the window in the middle of the screen.
		int position_x = (GetSystemMetrics(SM_CXSCREEN) - screen_width_) / 2;
		int position_y = (GetSystemMetrics(SM_CYSCREEN) - screen_height_) / 2;
		// change size/position of current window
		SetWindowPos(hwnd, 0, position_x, position_y, screen_width_, screen_height_, SWP_NOZORDER | SWP_NOACTIVATE);
		ResizeScreen();
		is_windowed_fullscreen_ = false;
		return true;
	}
	return false;
}

bool D3D::IsFullscreen() {
	BOOL is_fullscreen = 0;
	swap_chain_->GetFullscreenState(&is_fullscreen, NULL);
	return is_fullscreen != 0;
}

bool D3D::TurnFullscreenOn() {
	if (!IsFullscreen()) {
		swap_chain_->SetFullscreenState(TRUE, NULL);
		ResizeScreen();
		return true;
	}
	return false;
}

bool D3D::TurnFullscreenOff() {
	if (IsFullscreen()) {
		swap_chain_->SetFullscreenState(FALSE, NULL);
		ResizeScreen();
		return true;
	}
	return false;
}

int D3D::GetScreenWidth() {
	return screen_width_;
}

int D3D::GetScreenHeight() {
	return screen_height_;
}

void D3D::SetBuffers() {
	D3D11_TEXTURE2D_DESC depth_buffer_description;
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depth_stencil_view_description;
	D3D11_RASTERIZER_DESC raster_desc;
	D3D11_DEPTH_STENCIL_DESC depth_disabled_stencil_desc;
	D3D11_BLEND_DESC blend_state_description;


	// Initialize the description of the depth buffer.
	ZeroMemory(&depth_buffer_description, sizeof(depth_buffer_description));

	// Set up the description of the depth buffer.
	depth_buffer_description.Width = screen_width_;
	depth_buffer_description.Height = screen_height_;
	depth_buffer_description.MipLevels = 1;
	depth_buffer_description.ArraySize = 1;
	depth_buffer_description.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_buffer_description.SampleDesc.Count = 1;
	depth_buffer_description.SampleDesc.Quality = 0;
	depth_buffer_description.Usage = D3D11_USAGE_DEFAULT;
	depth_buffer_description.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depth_buffer_description.CPUAccessFlags = 0;
	depth_buffer_description.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description.
	device_->CreateTexture2D(&depth_buffer_description, NULL, &depth_stencil_buffer_);

	// Initialize the description of the stencil state.
	ZeroMemory(&depth_stencil_desc, sizeof(depth_stencil_desc));

	// Set up the description of the stencil state.
	depth_stencil_desc.DepthEnable = true;
	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS;

	depth_stencil_desc.StencilEnable = true;
	depth_stencil_desc.StencilReadMask = 0xFF;
	depth_stencil_desc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing.
	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing.
	depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the depth stencil state.
	device_->CreateDepthStencilState(&depth_stencil_desc, &depth_stencil_state_);

	// Set the depth stencil state.
	device_context_->OMSetDepthStencilState(depth_stencil_state_, 1);

	// Initialize the depth stencil view.
	ZeroMemory(&depth_stencil_view_description, sizeof(depth_stencil_view_description));

	// Set up the depth stencil view description.
	depth_stencil_view_description.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_stencil_view_description.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depth_stencil_view_description.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	device_->CreateDepthStencilView(depth_stencil_buffer_, &depth_stencil_view_description, &depth_stencil_view_);

	// Bind the render target view and depth stencil buffer to the output render pipeline.
	device_context_->OMSetRenderTargets(1, &render_target_view_, depth_stencil_view_);

	// Setup the raster description which will determine how and what polygons will be drawn.
	raster_desc.AntialiasedLineEnable = false;
	raster_desc.CullMode = D3D11_CULL_BACK;
	raster_desc.DepthBias = 0;
	raster_desc.DepthBiasClamp = 0.0f;
	raster_desc.DepthClipEnable = true;
	raster_desc.FillMode = D3D11_FILL_SOLID;
	raster_desc.FrontCounterClockwise = true;
	raster_desc.MultisampleEnable = false;
	raster_desc.ScissorEnable = false;
	raster_desc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	device_->CreateRasterizerState(&raster_desc, &raster_state_);

	// Now set the rasterizer state.
	device_context_->RSSetState(raster_state_);

	//create raster state with wireframe enabled
	raster_desc.FillMode = D3D11_FILL_WIREFRAME;
	device_->CreateRasterizerState(&raster_desc, &raster_state_wireframe_);

	// Clear the second depth stencil state before setting the parameters.
	ZeroMemory(&depth_disabled_stencil_desc, sizeof(depth_disabled_stencil_desc));

	// Now create a second depth stencil state which turns off the Z buffer for 2D rendering.  The only difference is 
	// that DepthEnable is set to false, all other parameters are the same as the other depth stencil state.
	depth_disabled_stencil_desc.DepthEnable = false;
	depth_disabled_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depth_disabled_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS;
	depth_disabled_stencil_desc.StencilEnable = true;
	depth_disabled_stencil_desc.StencilReadMask = 0xFF;
	depth_disabled_stencil_desc.StencilWriteMask = 0xFF;
	depth_disabled_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_disabled_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depth_disabled_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_disabled_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_disabled_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_disabled_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depth_disabled_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_disabled_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create the state using the device.
	device_->CreateDepthStencilState(&depth_disabled_stencil_desc, &depth_disabled_stencil_state_);

	// Create a new state with the just writeing to the depth buffer disabled.
	depth_disabled_stencil_desc.DepthEnable = true;
	depth_disabled_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	// Create the state using the device.
	device_->CreateDepthStencilState(&depth_disabled_stencil_desc, &depth_write_disabled_stencil_state_);


	// Clear the blend state description.
	ZeroMemory(&blend_state_description, sizeof(D3D11_BLEND_DESC));

	// Create an alpha enabled blend state description.
	blend_state_description.RenderTarget[0].BlendEnable = TRUE;
	blend_state_description.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_state_description.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state_description.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state_description.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_INV_DEST_ALPHA;
	blend_state_description.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state_description.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blend_state_description.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	// Create the Alpha blend state using the description.
	device_->CreateBlendState(&blend_state_description, &alpha_enable_blending_state_);

	blend_state_description.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state_description.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state_description.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state_description.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state_description.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;

	// Create the additive blend state using the description.
	device_->CreateBlendState(&blend_state_description, &additive_enable_blending_state_);

	// Modify the description to create an disabled blend state description.
	blend_state_description.RenderTarget[0].BlendEnable = FALSE;

	// Create the second blend state using the description.
	device_->CreateBlendState(&blend_state_description, &disable_blending_state_);
}

void D3D::ReleaseBuffers() {

	if (alpha_enable_blending_state_) {
		alpha_enable_blending_state_->Release();
		alpha_enable_blending_state_ = 0;
	}

	if (depth_disabled_stencil_state_) {
		depth_disabled_stencil_state_->Release();
		depth_disabled_stencil_state_ = 0;
	}

	if (raster_state_) {
		raster_state_->Release();
		raster_state_ = 0;
	}

	if (depth_stencil_view_) {
		depth_stencil_view_->Release();
		depth_stencil_view_ = 0;
	}

	if (depth_stencil_state_) {
		depth_stencil_state_->Release();
		depth_stencil_state_ = 0;
	}

	if (depth_stencil_buffer_) {
		depth_stencil_buffer_->Release();
		depth_stencil_buffer_ = 0;
	}

	if (render_target_view_) {
		render_target_view_->Release();
		render_target_view_ = 0;
	}

	if (disable_blending_state_) {
		disable_blending_state_->Release();
		disable_blending_state_ = 0;
	}

	if (additive_enable_blending_state_) {
		additive_enable_blending_state_->Release();
		additive_enable_blending_state_ = 0;
	}

	if (depth_write_disabled_stencil_state_) {
		depth_write_disabled_stencil_state_->Release();
		depth_write_disabled_stencil_state_ = 0;
	}

}

void D3D::ResizeScreen() {

	float field_of_view, screen_aspect;
	ID3D11Texture2D* texture_buffer;
	DXGI_MODE_DESC  new_mode_description;

	// new Mode.
	new_mode_description.Width = screen_width_;
	new_mode_description.Height = screen_height_;

	new_mode_description.RefreshRate.Numerator = 0;		// let dxgi decide refresh rate.
	new_mode_description.RefreshRate.Denominator = 0;	// let dxgi decide refresh rate.
	new_mode_description.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	new_mode_description.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	new_mode_description.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	swap_chain_->ResizeTarget(&new_mode_description);

	device_context_->OMSetRenderTargets(0, NULL, NULL);

	// Release all outstanding references to the swap chain's buffers.
	ReleaseBuffers();

	swap_chain_->ResizeBuffers(0, screen_width_, screen_height_, DXGI_FORMAT_UNKNOWN, 0);

	// Get buffer and create a render-target-view.	
	swap_chain_->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&texture_buffer);
	device_->CreateRenderTargetView(texture_buffer, NULL, &render_target_view_);
	texture_buffer->Release();
	device_context_->OMSetRenderTargets(1, &render_target_view_, NULL);

	// recreate all the buffers.
	SetBuffers();

	// reset the viewport
	viewport_.Width = static_cast<float>(screen_width_);
	viewport_.Height = static_cast<float>(screen_height_);

	device_context_->RSSetViewports(1, &viewport_);
	// Setup the projection matrix.
	field_of_view = (float)XM_PI / 4.0f;
	screen_aspect = (float)screen_width_ / (float)screen_height_;

	// Create the projection matrix for 3D rendering.
	projection_matrix_ = XMMatrixPerspectiveFovLH(field_of_view, screen_aspect, screen_near_, screen_depth_);

	// Initialize the world matrix to the identity matrix.
	world_matrix_ = XMMatrixIdentity();

	// Create an orthographic projection matrix for 2D rendering.
	ortho_matrix_ = XMMatrixOrthographicLH((float)screen_width_, (float)screen_height_, screen_near_, screen_depth_);
}

