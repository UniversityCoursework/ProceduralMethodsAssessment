#ifndef _TESSELLATED_SPHEREMESH_H_
#define _TESSELLATED_SPHEREMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

/// <summary>
/// Generates Vertex/Quad information from CubeSphere with 4 control point patchlist for Tessellation.
/// </summary>
class TessellatedSphereMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the Sphere Mesh and generates the vertex data.
	/// Uses a CubeSphere approach, generates a cube subdivided by the resolutions then Normalises
	///  it into a sphere.
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	/// <param name="resolution">
	/// The amount of subdivision on the faces of the longitiude and latitude of the sphere.
	/// </param>
	TessellatedSphereMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution = 10);
	~TessellatedSphereMesh();

	/// <summary>
	/// Sends data to the vertex buffer with 4 control point patchlist primitive type.
	/// </summary>
	void SendData(ID3D11DeviceContext* device_context);

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device* device);

	int resolution_;
};

#endif