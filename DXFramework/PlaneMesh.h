#ifndef _PLANEMESH_H_
#define _PLANEMESH_H_

#include "BaseMesh.h"

/// <summary>
/// Quad mesh made of many quads. Default is 100x100
/// </summary>
class PlaneMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the Plane Mesh and generates the vertex data.
	/// The plane will be subdivided into sub quads based on the resolution.
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	/// <param name="resolution"> The amount of subdivision on the faces of the cube. </param>
	PlaneMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution = 100);
	~PlaneMesh();

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device* device);

	int resolution_;
};

#endif