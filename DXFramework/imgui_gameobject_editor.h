#ifndef _IMGUI_OUTLINER_H
#define _IMGUI_OUTLINER_H

#include "../DXFramework/imgui.h"
#include "../DXFramework/Light.h"

#include "GameObject.h"

#include <vector>

namespace ImGui {

	/// <summary>
	/// Shows the outliner window, this shows all the game objects that can be itneracted with and modified within the game.
	/// </summary>
	/// <param name="is_open"> Whether the window is open. </param>
	/// <param name="gameobjects"> The list of game objects that will appear in the outliner. </param>
	void Editor(bool* is_open, std::vector<GameObject*> gameobjects);
}

#endif
