#ifndef _MULTILIGHT_SHADER_H
#define _MULTILIGHT_SHADER_H

#include "BaseShader.h"
#include "Light.h"
#include "MultiLightBuffers.h"

using namespace DirectX;

/// <summary>
/// Multiple Light Shader.
/// Using light type, to calculate the correct type of lighting for each light.
/// </summary>
class MultiLightShader : public BaseShader {
public:
	MultiLightShader(ID3D11Device* device, HWND hwnd);
	~MultiLightShader();

	/// <summary>
	/// Loads all the lighting data and camera data into the correct buffers for the lighting shaders.
	/// </summary>
	/// <param name="texture"> Texture to be applied to the mesh. </param>
	/// <param name="light"> Array of lights to be used in lighting calculation. </param>
	/// <param name="camera_position">Current position of the camera. </param>
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
		ID3D11ShaderResourceView* texture, Light* light[MAXLIGHTS], XMFLOAT3 camera_position);

	/// <summary>
	/// Renders the previous sent Vertex buffer of Index Count, with Lighting.
	/// Renders using DrawIndexed.
	/// </summary>	
	/// <param name="index_count"> The number of indices in the vertex buffer. </param>
	void Render(ID3D11DeviceContext* device_context, int index_count);

private:
	/// <summary>
	/// Loads the shaders, and initialises the shader buffers descriptions.
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader to load. </param>
	/// <param name="ps_filename"> Pixel Shader to load. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* ps_filename);

private:
	ID3D11SamplerState* sample_state_;
	ID3D11Buffer* light_buffer_;
	ID3D11Buffer* camera_buffer_;
};

#endif