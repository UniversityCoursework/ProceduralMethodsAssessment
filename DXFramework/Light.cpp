#include "light.h"

#include <assert.h>

Light::Light() {
	SetAmbientColour(0, 0, 0, 1.0f);
	SetDiffuseColour(0, 0, 0, 1.0f);
	SetDirection(0, 0, 0);
	SetSpecularColour(0, 0, 0, 1.0f);
	SetSpecularPower(2.0f);
	SetPosition(0, 0, 0);
	SetLookAt(0, 0, 0);

	light_type_ = kOff;

	constant_factor_ = 1.0f;
	linear_factor_ = 0.125f;
	quadratic_factor_ = 0.0f;
	range_ = 50.0f;
}

void Light::GenerateViewMatrix() {

	XMVECTOR look_at_direction = look_at_ - position_;

	// Cant have a look and position the same...as you then have no direction -.-
	assert(!XMVector3Equal(look_at_direction, XMVectorZero()));

	XMVECTOR up = XMVectorSet(-XMVectorGetY(look_at_direction), XMVectorGetX(look_at_direction), 0.0f, 1.0f);

	view_matrix_ = XMMatrixLookAtLH(position_, look_at_, up);
}

void Light::GenerateProjectionMatrix(float screen_near, float screen_far) {
	float field_of_view, screen_aspect;

	// Setup field of view and screen aspect for a square light source.
	field_of_view = (float)XM_PI / 2.0f;
	screen_aspect = 1.0f;

	// Create the projection matrix for the light.
	projection_matrix_ = XMMatrixPerspectiveFovLH(field_of_view, screen_aspect, screen_near, screen_far);
}

void Light::GenerateOrthoMatrix(float screen_width, float screen_height, float near, float far) {
	ortho_matrix_ = XMMatrixOrthographicLH(screen_width, screen_height, near, far);
}

void Light::SetType(LightType type) {
	light_type_ = type;
}

void Light::SetAmbientColour(float red, float green, float blue, float alpha) {
	ambient_colour_ = XMFLOAT4(red, green, blue, alpha);
	diffuse_colour_ = XMFLOAT4(red, green, blue, alpha);
}

void Light::SetDiffuseColour(float red, float green, float blue, float alpha) {
	diffuse_colour_ = XMFLOAT4(red, green, blue, alpha);
	ambient_colour_ = XMFLOAT4(red, green, blue, alpha);
}

void Light::SetDirection(float x, float y, float z) {
	direction_ = XMFLOAT3(x, y, z);
}

void Light::SetSpecularColour(float red, float green, float blue, float alpha) {
	specular_colour_ = XMFLOAT4(red, green, blue, alpha);
}

void Light::SetSpecularPower(float specular_power) {
	specular_power_ = specular_power;
}

void Light::SetConstantFactor(float constant_factor) {
	constant_factor_ = constant_factor;
}

void Light::SetLinearFactor(float linear_factor) {
	linear_factor_ = linear_factor;
}

void Light::SetQuadraticFactor(float quadratic_factor) {
	quadratic_factor_ = quadratic_factor;
}

void Light::SetRange(float range) {
	range_ = range;
}

void Light::SetPosition(float x, float y, float z) {
	position_ = XMVectorSet(x, y, z, 1.0f);
}

void Light::SetLookAt(float x, float y, float z) {
	look_at_ = XMVectorSet(x, y, z, 1.0f);
}

LightType Light::GetType() {
	return light_type_;
}

XMFLOAT4 Light::GetAmbientColour() {
	return ambient_colour_;
}

XMFLOAT4 Light::GetDiffuseColour() {
	return diffuse_colour_;
}

XMFLOAT3 Light::GetDirection() {
	return direction_;
}

XMFLOAT4 Light::GetSpecularColour() {
	return specular_colour_;
}

float Light::GetSpecularPower() {
	return specular_power_;
}

XMFLOAT3 Light::GetPosition() {
	XMFLOAT3 temp(XMVectorGetX(position_), XMVectorGetY(position_), XMVectorGetZ(position_));
	return temp;
}

XMFLOAT3 Light::GetLookAt() {
	XMFLOAT3 temp(XMVectorGetX(look_at_), XMVectorGetY(look_at_), XMVectorGetZ(look_at_));
	return temp;
}

XMMATRIX Light::GetViewMatrix() {
	return view_matrix_;
}

XMMATRIX Light::GetProjectionMatrix() {
	return projection_matrix_;
}

XMMATRIX Light::GetOrthoMatrix() {
	return ortho_matrix_;
}

float Light::GetConstantFactor() {
	return constant_factor_;
}

float Light::GetLinearFactor() {
	return linear_factor_;
}

float Light::GetQuadraticFactor() {
	return quadratic_factor_;
}

float Light::GetRange() {
	return range_;
}
