#ifndef _SPHEREMESH_H_
#define _SPHEREMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

/// <summary>
/// Sphere Mesh, Generates a sphere.
/// </summary>
class SphereMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the Sphere Mesh and generates the vertex data.
	/// Uses a CubeSphere approach, generates a cube subdivided by the resolutions then Normalises
	///  it into a sphere.
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	/// <param name="resolution">
	/// The amount of subdivision on the faces of the longitiude and latitude of the sphere.
	/// </param>
	SphereMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution = 20);
	~SphereMesh();

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device* device);

	int resolution_;
};

#endif