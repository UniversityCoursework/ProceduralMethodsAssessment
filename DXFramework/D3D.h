#ifndef _D3D_H_
#define _D3D_H_

// Link libaries dxgi.lib, d3d11.lib, d3dx11.lib, d3dx10.lib

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>

using namespace DirectX;

/// <summary>
/// Handles DirectX 11 swap chain and Rendering.
/// </summary>
class D3D {
public:
	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}

	D3D(int screen_width, int screen_height, bool vsync, HWND hwnd, bool fullscreen, float screen_depth, float screen_near);
	~D3D();

	/// <summary>
	/// Clears the RenderTarget and Stencil.
	/// </summary>
	void BeginScene(float r, float g, float b, float a);

	/// <summary>
	/// Calls Present with depending on v-sync state.
	/// </summary>
	void EndScene();

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();

	void GetProjectionMatrix(XMMATRIX& projection_matrix);
	void GetWorldMatrix(XMMATRIX& world_matrix);
	void GetOrthoMatrix(XMMATRIX& ortho_matrix);

	void GetVideoCardInfo(char* card_name, int& memory);

	void TurnZBufferOn();
	void TurnZBufferOff();

	void TurnOnAlphaBlending();

	void TurnOffBlending();

	void SetBackBufferRenderTarget();
	void ResetViewport();

	void TurnOnWireframe();
	void TurnOffWireframe();
	bool IsWireFrameModeOn();

	void TurnZBufferWriteOn();
	/// <summary>
	/// Turns off the ability to write to the depth buffer, still runs the depth test. 
	/// </summary>
	void TurnZBufferWriteOff();

	void TurnOnAdditiveBlending();

	/// <summary>
	/// Reset HS, DS, GS shaders, useful for when you no longer wish to use thses shaders.
	/// i.e after finished with tesselation
	/// </summary>
	void ResetShadersNull();

	/// <summary>
	/// Change resolution, ensure valid resolution is passed.
	/// hwnd used to set to centre of screen. 
	/// </summary>
	/// <returns> True when succesfully changes resolution, will return false if already at that resolution. </returns>
	bool ChangeResolution(int screen_width, int screen_height, HWND hwnd);

	bool IsFullScreenWindowed();

	/// <summary>
	/// Extension of Change resolution, create the d3d at the max size available for the monitor
	/// without any borders etc. Saves the previous resolution to change back to when turned off.
	/// Will only work on Monitor max Resolution.
	/// hwnd used to set to centre of screen.
	/// </summary>
	/// <returns> True when succesfully set to FullScreenWindowed. </returns>
	bool TurnFullScreenWindowedOn(HWND hwnd);

	/// <summary>
	/// Reverts back to the old resolution setting saved when it went fullscreen.
	/// </summary>
	/// <param name="hwnd"></param>
	/// <returns> True if succesfully turns off, false if already off. </returns>
	bool TurnFullScreenWindowedOff(HWND hwnd);

	/// <summary>
	/// Whether the device is already in fullscreen Exclusive mode. 
	/// </summary>
	bool IsFullscreen();

	/// <summary>
	/// Turns on Fullscreen Exclusive mode.
	/// </summary>
	/// <returns> True when set to fullscreen exclusive mode</returns>
	bool TurnFullscreenOn();

	/// <summary>
	/// Turns off Fullscreen Exclusive mode.
	/// </summary>
	/// <returns>  True if succesfully turns off, false if already off. </returns>
	bool TurnFullscreenOff();


	/// <summary>
	/// The current directX width/height in pixels.
	/// Usefull for when in exclsive mode, as screen size will not match pixel resolution.
	/// </summary>
	XMFLOAT2 Resolution();

	int GetScreenWidth();
	int GetScreenHeight();
protected:

	/// <summary>
	/// Sets up all the Buffers, states and views required for DX11
	/// The depth,alpha,stencil and render target.
	/// </summary>
	void SetBuffers();

	/// <summary>
	/// Releases and nulls all the buffers used by DX11
	/// </summary>
	void ReleaseBuffers();

	/// <summary>
	/// Resizes the viewport and recalculates all the matrices required for projection.
	/// </summary>
	void ResizeScreen();

protected:
	bool is_wireframe_on_;
	bool vsync_enabled_;
	int video_card_memory_;
	char video_card_description_[128];
	IDXGISwapChain* swap_chain_;
	ID3D11Device* device_;
	ID3D11DeviceContext* device_context_;
	ID3D11RenderTargetView* render_target_view_;
	ID3D11Texture2D* depth_stencil_buffer_;
	ID3D11DepthStencilState* depth_stencil_state_;
	ID3D11DepthStencilView* depth_stencil_view_;
	ID3D11RasterizerState* raster_state_;
	ID3D11RasterizerState* raster_state_wireframe_;
	XMMATRIX projection_matrix_;
	XMMATRIX world_matrix_;
	XMMATRIX ortho_matrix_;
	ID3D11DepthStencilState* depth_disabled_stencil_state_;
	ID3D11BlendState* alpha_enable_blending_state_;

	D3D11_VIEWPORT viewport_;

	int screen_width_;
	int screen_height_;
	float screen_depth_;
	float screen_near_;
	bool is_windowed_fullscreen_;

	int saved_windowed_fullscreen_width_;
	int saved_windowed_fullscreen_height_;

	ID3D11BlendState* disable_blending_state_;
	ID3D11BlendState* additive_enable_blending_state_;
	ID3D11DepthStencilState* depth_write_disabled_stencil_state_;


};

#endif