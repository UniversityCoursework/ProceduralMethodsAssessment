#include "System.h"

#include "imgui.h"
#include "imgui_impl_dx11.h"

System::System(BaseApplication* application) {
	int screen_width, screen_height;

	// Initialize the width and height of the screen to zero before sending the variables into the function.
	screen_width = 0;
	screen_height = 0;

	// Initialize the windows api.
	InitializeWindows(screen_width, screen_height);

	// Create the application wrapper object.	
	application_ = application;
	application_->Init(h_instance_, hwnd_, screen_width, screen_height, &input_);
}

System::~System() {
	// Release the application wrapper object.
	if (application_) {
		//application_->Shutdown();
		delete application_;
		application_ = 0;
	}

	// Shutdown the window.
	ShutdownWindows();
}

void System::Run() {
	MSG msg;
	bool done, result;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user.
	done = false;
	while (!done) {
		// Handle the windows messages.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if (msg.message == WM_QUIT) {
			done = true;
		} else {
			// Otherwise do the frame processing.
			result = Frame();
			if (!result) {
				done = true;
			}
		}

	}
}

bool System::Frame() {
	bool result;


	// Do the frame processing for the application object.
	result = application_->Frame();
	if (!result) {
		return false;
	}

	return true;
}

LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam) {
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

void System::InitializeWindows(int& screen_width, int& screen_height) {
	WNDCLASSEX window_class;
	DEVMODE dev_mode_screen_settings;
	int position_x, position_y;

	// Get an external pointer to this object.	
	kApplicationHandle = this;

	// Get the instance of this application.
	h_instance_ = GetModuleHandle(NULL);

	// Give the application a name.
	application_name_ = L"Engine";

	// Setup the windows class with default settings.
	window_class.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	window_class.lpfnWndProc = WndProc;
	window_class.cbClsExtra = 0;
	window_class.cbWndExtra = 0;
	window_class.hInstance = h_instance_;
	window_class.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	window_class.hIconSm = window_class.hIcon;
	window_class.hCursor = LoadCursor(NULL, IDC_ARROW);
	window_class.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	window_class.lpszMenuName = NULL;
	window_class.lpszClassName = application_name_;
	window_class.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&window_class);

	// Determine the resolution of the clients desktop screen.
	screen_width = GetSystemMetrics(SM_CXSCREEN);
	screen_height = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (kFullScreen) {
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dev_mode_screen_settings, 0, sizeof(dev_mode_screen_settings));
		dev_mode_screen_settings.dmSize = sizeof(dev_mode_screen_settings);
		dev_mode_screen_settings.dmPelsWidth = (unsigned long)screen_width;
		dev_mode_screen_settings.dmPelsHeight = (unsigned long)screen_height;
		dev_mode_screen_settings.dmBitsPerPel = 32;
		dev_mode_screen_settings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dev_mode_screen_settings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		position_x = position_y = 0;
	} else {
		if (!kWindowedFullScreen) {
			// If windowed then set it to 800x600 resolution.
			screen_width = 800;
			screen_height = 600;
		}
		// Place the window in the middle of the screen.
		position_x = (GetSystemMetrics(SM_CXSCREEN) - screen_width) / 2;
		position_y = (GetSystemMetrics(SM_CYSCREEN) - screen_height) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	hwnd_ = CreateWindowEx(WS_EX_APPWINDOW, application_name_, application_name_,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		position_x, position_y, screen_width, screen_height, NULL, NULL, h_instance_, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(hwnd_, SW_SHOW);
	SetForegroundWindow(hwnd_);
	SetFocus(hwnd_);
}

void System::ShutdownWindows() {
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if (kFullScreen) {
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(hwnd_);
	hwnd_ = NULL;

	// Remove the application instance.
	UnregisterClass(application_name_, h_instance_);
	h_instance_ = NULL;

	// Release the pointer to this class.
	kApplicationHandle = NULL;

	ImGui_ImplDX11_Shutdown();
}

extern LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK System::WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam) {
	switch (umessage) {
		// Handle keyboard and mouse input.
		case WM_KEYDOWN:
		{
			kApplicationHandle->input_.SetKeyDown(wparam);
			break;
		}
		case WM_KEYUP:
		{
			kApplicationHandle->input_.SetKeyUp(wparam);
			break;
		}
		case WM_MOUSEMOVE:
		{
			kApplicationHandle->input_.SetMouseX(LOWORD(lparam));
			kApplicationHandle->input_.SetMouseY(HIWORD(lparam));
			break;
		}
		case WM_LBUTTONDOWN:
		{
			kApplicationHandle->input_.SetLeftMouse(true);
			break;
		}
		case WM_RBUTTONDOWN:
		{
			kApplicationHandle->input_.SetRightMouse(true);
			break;
		}
		case WM_LBUTTONUP:
		{
			kApplicationHandle->input_.SetLeftMouse(false);
			break;
		}
		case WM_RBUTTONUP:
		{
			kApplicationHandle->input_.SetRightMouse(false);
			break;
		}
		// Check if the window is being destroyed.
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}

	}

	// Update the ImGui Input.
	if (ImGui_ImplDX11_WndProcHandler(hwnd, umessage, wparam, lparam))
		return true;

	return kApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
}