/*
	Beginning DirectX 11 Game Programming
	By Allen Sherrod and Wendy Jones

	TokenStream - Used to return blocks of text in a file.
*/
#include<string>
#include"TokenStream.h"

bool IsValidIdentifier(char c) {
	// Ascii from ! to ~.
	if ((int)c > 32 && (int)c < 127)
		return true;

	return false;
}

bool IsValidIdentifier(char c, char* delimiters, int total_delimiters) {
	if (delimiters == 0 || total_delimiters == 0) {
		return IsValidIdentifier(c);
	}

	for (int i = 0; i < total_delimiters; i++) {
		if (c == delimiters[i])
			return false;
	}
	return true;
}

TokenStream::TokenStream() {
	ResetStream();
}

void TokenStream::ResetStream() {
	start_index_ = end_index_ = 0;
}

void TokenStream::SetTokenStream(char *data) {
	ResetStream();
	data_ = data;
}

bool TokenStream::GetNextToken(std::string* buffer, char* delimiters, int total_delimiters) {
	start_index_ = end_index_;

	bool in_string = false;
	int length = (int)data_.length();

	if (start_index_ >= length - 1)
		return false;

	while (start_index_ < length && IsValidIdentifier(data_[start_index_],
		delimiters, total_delimiters) == false) {
		start_index_++;
	}

	end_index_ = start_index_ + 1;

	if (data_[start_index_] == '"')
		in_string = !in_string;

	if (start_index_ < length) {
		while (end_index_ < length && (IsValidIdentifier(data_[end_index_], delimiters,
			total_delimiters) || in_string == true)) {
			if (data_[end_index_] == '"')
				in_string = !in_string;

			end_index_++;
		}

		if (buffer != NULL) {
			int size = (end_index_ - start_index_);
			int index = start_index_;

			buffer->reserve(size + 1);
			buffer->clear();

			for (int i = 0; i < size; i++) {
				buffer->push_back(data_[index++]);
			}
		}

		return true;
	}

	return false;
}

bool TokenStream::MoveToNextLine(std::string* buffer) {
	int length = (int)data_.length();

	if (start_index_ < length && end_index_ < length) {
		end_index_ = start_index_;

		while (end_index_ < length && (IsValidIdentifier(data_[end_index_]) ||
			data_[end_index_] == ' ')) {
			end_index_++;
		}

		if ((end_index_ - start_index_) == 0)
			return false;

		if (end_index_ - start_index_ >= length)
			return false;

		if (buffer != NULL) {
			int size = (end_index_ - start_index_);
			int index = start_index_;

			buffer->reserve(size + 1);
			buffer->clear();

			for (int i = 0; i < size; i++) {
				buffer->push_back(data_[index++]);
			}
		}
	} else {
		return false;
	}

	end_index_++;
	start_index_ = end_index_ + 1;

	return true;
}