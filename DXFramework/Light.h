#ifndef _LIGHT_H_
#define _LIGHT_H_

#include <directxmath.h>

using namespace DirectX;

/// <summary>
/// Different types of lights.
/// </summary>
enum LightType {
	kAmbient = 0,
	kPoint = 1,
	kDirectional = 2,
	kOff = 3
};

/// <summary>
/// Holds data that represents a light source; type, diffuse color, attenuation etc.
/// Default LightType kOff.
/// </summary>
class Light {
public:

	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}

	Light();
	
	/// <summary>
	/// Generate the ViewMatrix for the light based on light data.
	/// </summary>
	void GenerateViewMatrix();
	
	/// <summary>
	/// Generates the Projection Matrix for the light using the screen near and far.
	/// Note: Cant have a look and position the same...as you then have no direction.
	/// </summary>
	void GenerateProjectionMatrix(float screen_near, float screen_far);

	/// <summary>
	/// Generates an OrthoGraphic Matrix for the light using the screen width, height, near and far.
	/// </summary>
	void GenerateOrthoMatrix(float screen_width, float screen_height, float near, float far);

	void SetType(LightType type);
	void SetAmbientColour(float red, float green, float blue, float alpha);
	void SetDiffuseColour(float red, float green, float blue, float alpha);
	void SetDirection(float x, float y, float z);
	void SetSpecularColour(float red, float green, float blue, float alpha);
	void SetSpecularPower(float specular_power);
	void SetPosition(float x, float y, float z);
	void SetLookAt(float x, float y, float z);

	void SetConstantFactor(float constant_factor);
	void SetLinearFactor(float linear_factor);
	void SetQuadraticFactor(float quadratic_factor);
	void SetRange(float range);

	LightType GetType();
	XMFLOAT4 GetAmbientColour();
	XMFLOAT4 GetDiffuseColour();
	XMFLOAT3 GetDirection();
	XMFLOAT4 GetSpecularColour();
	float GetSpecularPower();

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetLookAt();

	XMMATRIX GetViewMatrix();
	XMMATRIX GetProjectionMatrix();
	XMMATRIX GetOrthoMatrix();

	float GetConstantFactor();
	float GetLinearFactor();
	float GetQuadraticFactor();
	float GetRange();

protected:
	XMFLOAT4 ambient_colour_;
	XMFLOAT4 diffuse_colour_;
	XMFLOAT3 direction_;
	XMFLOAT4 specular_colour_;
	float specular_power_;
	XMVECTOR position_;
	XMMATRIX view_matrix_;
	XMMATRIX projection_matrix_;
	XMMATRIX ortho_matrix_;
	XMVECTOR look_at_;

	LightType light_type_;

	float constant_factor_;
	float linear_factor_;
	float quadratic_factor_;
	float range_;
};

#endif