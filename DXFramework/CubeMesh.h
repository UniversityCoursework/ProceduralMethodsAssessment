#ifndef _CUBEMESH_H_
#define _CUBEMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

/// <summary>
/// Basic Cube Mesh, with subdivided faces.
/// </summary>
class CubeMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the CubeMesh and generates the vertex data.
	/// The cubes faces will be subdivided into sub quads based on the resolution.
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	/// <param name="resolution"> The amount of subdivision on the faces of the cube. </param>
	CubeMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution = 20);	
	~CubeMesh();

protected:	
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device* device);

	int resolution_;
};

#endif