#include "PerlinNoise.h"

#include "../DXFramework/MathsUtils.h"

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 02/02/17
/// </summary>
namespace LittleLot {

	static int grad3[][3] = { { 1,1,0 },{ -1,1,0 },{ 1,-1,0 },{ -1,-1,0 },
	{ 1,0,1 },{ -1,0,1 },{ 1,0,-1 },{ -1,0,-1 },
	{ 0,1,1 },{ 0,-1,1 },{ 0,1,-1 },{ 0,-1,-1 } };
	static int p[] = { 151,160,137,91,90,15,
		131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
		190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
		88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
		77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
		102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
		135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
		5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
		223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
		129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
		251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
		49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
		138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180 };

	int PerlinNoise::permutations_[512];

	PerlinNoise::FbmSettings::FbmSettings(float new_scaling, float new_cross_section, int new_ocatves, float new_lacunarity, float new_gain) {
		scaling_factor = new_scaling;
		
		cross_section = new_cross_section;
		
		octaves = new_ocatves;
		
		lacunarity = new_lacunarity;
		
		gain = new_gain;
	}

	void PerlinNoise::InitialisePermutations() {
		// To remove the need for index wrapping, double the permutation table length
		for (int i = 0; i < 512; i++) {
			permutations_[i] = p[i & 255];
		}
	}

	double PerlinNoise::Noise(double x, double y, double z, double frequency) {
		x *= frequency;
		y *= frequency;
		z *= frequency;
		// Find unit grid cell containing point
		int X = FastFloor(x);
		int Y = FastFloor(y);
		int Z = FastFloor(z);

		// Get relative xyz coordinates of point within that cell
		x = x - X;
		y = y - Y;
		z = z - Z;

		// Wrap the integer cells at 255 (smaller integer period can be introduced here)
		X = X & 255;
		Y = Y & 255;
		Z = Z & 255;

		// Calculate a set of eight hashed gradient indices
		int gi000 = permutations_[X + permutations_[Y + permutations_[Z]]] % 12;
		int gi001 = permutations_[X + permutations_[Y + permutations_[Z + 1]]] % 12;
		int gi010 = permutations_[X + permutations_[Y + 1 + permutations_[Z]]] % 12;
		int gi011 = permutations_[X + permutations_[Y + 1 + permutations_[Z + 1]]] % 12;
		int gi100 = permutations_[X + 1 + permutations_[Y + permutations_[Z]]] % 12;
		int gi101 = permutations_[X + 1 + permutations_[Y + permutations_[Z + 1]]] % 12;
		int gi110 = permutations_[X + 1 + permutations_[Y + 1 + permutations_[Z]]] % 12;
		int gi111 = permutations_[X + 1 + permutations_[Y + 1 + permutations_[Z + 1]]] % 12;

		// The gradients of each corner are now:
		// g000 = grad3[gi000];
		// g001 = grad3[gi001];
		// g010 = grad3[gi010];
		// g011 = grad3[gi011];
		// g100 = grad3[gi100];
		// g101 = grad3[gi101];
		// g110 = grad3[gi110];
		// g111 = grad3[gi111];

		// Calculate noise contributions from each of the eight corners
		double n000 = Dot(grad3[gi000], x, y, z);
		double n100 = Dot(grad3[gi100], x - 1, y, z);
		double n010 = Dot(grad3[gi010], x, y - 1, z);
		double n110 = Dot(grad3[gi110], x - 1, y - 1, z);
		double n001 = Dot(grad3[gi001], x, y, z - 1);
		double n101 = Dot(grad3[gi101], x - 1, y, z - 1);
		double n011 = Dot(grad3[gi011], x, y - 1, z - 1);
		double n111 = Dot(grad3[gi111], x - 1, y - 1, z - 1);

		// Compute the fade curve value for each of x, y, z
		double u = Fade(x);
		double v = Fade(y);
		double w = Fade(z);
		// Interpolate along x the contributions from each of the corners
		double nx00 = LittleLot::Lerp(n000, n100, u);
		double nx01 = LittleLot::Lerp(n001, n101, u);
		double nx10 = LittleLot::Lerp(n010, n110, u);
		double nx11 = LittleLot::Lerp(n011, n111, u);
		// Interpolate the four results along y
		double nxy0 = LittleLot::Lerp(nx00, nx10, v);
		double nxy1 = LittleLot::Lerp(nx01, nx11, v);
		// Interpolate the two last results along z
		double nxyz = LittleLot::Lerp(nxy0, nxy1, w);

		return nxyz;
	}

	double PerlinNoise::FractionalBrownianMotion(double x, double y, FbmSettings settings) {
		double z = settings.cross_section;
		double sum = 0;
		double freq = 1.0, amp = 1.0;
		for (int i = 0; i < settings.octaves; i++) {
			double n = Noise(x, y, z, freq);
			sum += n*amp;
			freq *= settings.lacunarity;
			amp *= settings.gain;
		}
		return settings.scaling_factor*sum;
	}

	int PerlinNoise::FastFloor(double x) {
		return x > 0 ? (int)x : (int)x - 1;
	}

	double PerlinNoise::Dot(int g[3], double x, double y, double z) {
		return g[0] * x + g[1] * y + g[2] * z;
	}

	double PerlinNoise::Fade(double t) {
		return t*t*t*(t*(t * 6 - 15) + 10);
	}
	
}
