#include "basemesh.h"

BaseMesh::BaseMesh() {
	instance_count_ = 0;
	vertex_count_ = 0;
	index_count_ = 0;
}

BaseMesh::~BaseMesh() {
	instance_count_ = 0;
	vertex_count_ = 0;
	index_count_ = 0;
	// Release the index buffer.
	if (index_buffer_) {
		index_buffer_->Release();
		index_buffer_ = nullptr;
	}

	// Release the vertex buffer.
	if (vertex_buffer_) {
		vertex_buffer_->Release();
		vertex_buffer_ = nullptr;
	}

	// Release the instance buffer.
	if (instance_buffer_) {
		instance_buffer_->Release();
		instance_buffer_ = nullptr;
	}

	// Release the texture object.
	if (texture_) {
		delete texture_;
		texture_ = 0;
	}
}

int BaseMesh::GetIndexCount() {
	return index_count_;
}

int BaseMesh::GetVertexCount() {
	return vertex_count_;
}

int BaseMesh::GetInstanceCount() {
	return instance_count_;
}

ID3D11ShaderResourceView* BaseMesh::GetTexture() {
	return texture_->GetTexture();
}

void BaseMesh::SendData(ID3D11DeviceContext* device_context) {
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device_context->IASetVertexBuffers(0, 1, &vertex_buffer_, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device_context->IASetIndexBuffer(index_buffer_, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void BaseMesh::LoadTexture(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* filename) {
	// Create the texture object.
	texture_ = new Texture(device, device_context, filename);
}



