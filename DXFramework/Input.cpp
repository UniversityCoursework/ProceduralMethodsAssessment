#include "Input.h"

void Input::SetKeyDown(WPARAM key) {
	keys[key] = true;
}

void Input::SetKeyUp(WPARAM key) {
	keys[key] = false;
}

bool Input::IsKeyDown(int key) {
	return keys[key];
}

void Input::SetMouseX(int x_position) {
	mouse.x = x_position;
}

void Input::SetMouseY(int y_position) {
	mouse.y = y_position;
}

int Input::GetMouseX() {
	return mouse.x;
}

int Input::GetMouseY() {
	return mouse.y;
}

void Input::SetLeftMouse(bool down) {
	mouse.left = down;
}

void Input::SetRightMouse(bool down) {
	mouse.right = down;
}

bool Input::IsLeftMouseDown() {
	return mouse.left;
}

bool Input::IsRightMouseDown() {
	return mouse.right;
}

void Input::SetMouseActive(bool active) {
	mouse.is_active = active;
}
bool Input::IsMouseActive() {
	return mouse.is_active;
}