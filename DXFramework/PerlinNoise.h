#ifndef _LITTLELOT_PERLIN_NOISE_H
#define _LITTLELOT_PERLIN_NOISE_H

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 02/02/17
/// </summary>
namespace LittleLot {

	/// <summary>
	/// Generates Perlin Noise based on Ken perlins Outline.
	/// Will generate various types of perlin noise for given parameters.
	/// </summary>
	class PerlinNoise {
	public:
		/// <summary>
		/// Simple struct for Fractional Brownian Motion function settings.
		/// </summary>
		struct FbmSettings {
			FbmSettings() {};
			FbmSettings(float new_scaling, float new_cross_section, int new_ocatves, float new_lacunarity, float new_gain);
			/// <summary>
			/// How much to amplify the output noise value by.
			/// </summary>
			float scaling_factor = 1.0f;
			/// <summary>
			/// Where on the 3D perlin noise function to horizontally slice.
			/// <para>Note: Only used if using 2D Turbulence.</para>
			/// </summary>
			float cross_section = 0.0f;
			/// <summary>
			/// Typically a bit less than log(terrain_width) / log(lacunarity).
			/// So, for a 1024 x 1024 heightfield, about 10 octaves are needed.
			/// </summary>
			int octaves = 8;
			/// <summary>
			/// The frequency multiplier between octaves.
			/// Is typically set to a number just under 2.0 (e.g. 1.92). 
			/// </summary>
			float lacunarity = 2.0f;
			/// <summary>
			/// Directly influences the terrain roughness. 
			/// </summary>
			float gain = 0.6f;
		};
				
		/// <summary>
		/// Initialises the Permutation array used in Perlin Noise.
		/// Only needs to be initialised once, but must be done to enable
		/// proper perlin noise.
		/// </summary>
		static void InitialisePermutations();

		/// <summary>
		/// Generates perlin noise value between -1 and 1. Using Ken Perlins Perlin Noise.
		/// https://blackboard.abertay.ac.uk/bbcswebdav/pid-399424-dt-content-rid-1071967_1/courses/CMP305.2016-7.S2/simplexnoisedemystified.pdf
		/// http://weber.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf	// public link.
		/// </summary>
		/// <param name="x"> Ensure value is between 0 to 1, i.e horz tile divide by terrain width. </param>
		/// <param name="y"> Ensure value is between 0 to 1, Any fixed value, decides vertical slice value. </param>
		/// <param name="z"> Ensure value is between 0 to 1, i.e vert tile divide by terrain width. </param>
		/// <param name="frequency"> How Zoomed out the perlin noise is generated from. (higher = more detail) </param>
		/// <returns> Noise value between -1 and 1. </returns>
		static double Noise(double x, double y, double z, double frequency);

		/// <summary>
		/// Calculates the weighted sum of a number of scaled Perlin noise 'octaves', adding detail to each octave.
		/// </summary>
		/// <param name="x"> Ensure value is between 0 to 1, i.e divide by terrain width. </param>
		/// <param name="y"> Ensure value is between 0 to 1, i.e divide by terrain height. </param>
		/// <param name="settings"></param>
		/// <returns> Noise Value. </returns>
		static double FractionalBrownianMotion(double x, double y, FbmSettings settings);

	private:
		static int FastFloor(double x);
		static double Dot(int g[3], double x, double y, double z);
		static double Fade(double t);

		static int permutations_[512];
	};
}
#endif // !_PERLIN_NOISE_H

