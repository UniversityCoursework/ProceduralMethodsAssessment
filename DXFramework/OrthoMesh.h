#ifndef _ORTHOMESH_H_
#define _ORTHOMESH_H_

#include "basemesh.h"

using namespace DirectX;

/// <summary>
/// For OrthoGraphic rendering will render a quad at position,
///  of widthXheight infront of the camera.
/// Generally Used for post proccessing. 
/// Requires the use of an orthographic matrix instead of projection matrix.
/// </summary>
class OrthoMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the OrthoMesh and generates the vertex data.
	/// The quad will be created to overlay the camera. Requires the use of an orthographic matrix.
	/// </summary>
	/// <param name="width"> Width to create quad at.</param>
	/// <param name="height"> Height to create quad at. </param>
	/// <param name="x_position"> X Position of the quad in screen space. </param>
	/// <param name="y_position"> Y Position of the quad in screen space.</param>
	OrthoMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, int width, int height, int x_position = 0, int y_position = 0);
	~OrthoMesh();

	/// <summary>
	/// Resizes the mesh to fit a new width/height.
	/// Generally used for when the screen resolution has changed
	/// and you wish the ortho mesh to cover the whole screen again.
	/// </summary>
	/// <param name="width"> New width of the mesh. </param>
	/// <param name="height"> New height of the mesh. </param>
	void ResizeMesh(ID3D11Device* device, int width, int height);

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device*);

	int width_, height_, x_position_, y_position_;
};

#endif