#include "model.h"

Model::Model(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, WCHAR* filename) {
	// Load model data
	LoadModel(filename);

	// Initialize the vertex and index buffer that hold the geometry for the model.
	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, device_context, texture_filename);
}

Model::~Model() {
	// Run parent deconstructor
	BaseMesh::~BaseMesh();

	if (model_) {
		delete[] model_;
		model_ = 0;
	}

}

void Model::InitBuffers(ID3D11Device* device) {
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertex_buffer_desc, index_buffer_desc;
	D3D11_SUBRESOURCE_DATA vertex_data, index_data;

	vertices = new VertexType[vertex_count_];

	indices = new unsigned long[index_count_];

	// Load the vertex array and index array with data.
	for (int i = 0; i < vertex_count_; i++) {
		vertices[i].position = XMFLOAT3(model_[i].x, model_[i].y, -model_[i].z);
		vertices[i].texture = XMFLOAT2(model_[i].tu, model_[i].tv);
		vertices[i].normal = XMFLOAT3(model_[i].nx, model_[i].ny, -model_[i].nz);

		indices[i] = i;
	}

	// Set up the description of the static vertex buffer.
	vertex_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	vertex_buffer_desc.ByteWidth = sizeof(VertexType)* vertex_count_;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.CPUAccessFlags = 0;
	vertex_buffer_desc.MiscFlags = 0;
	vertex_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertex_data.pSysMem = vertices;
	vertex_data.SysMemPitch = 0;
	vertex_data.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertex_buffer_desc, &vertex_data, &vertex_buffer_);

	// Set up the description of the static index buffer.
	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	index_buffer_desc.ByteWidth = sizeof(unsigned long)* index_count_;
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.CPUAccessFlags = 0;
	index_buffer_desc.MiscFlags = 0;
	index_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	index_data.pSysMem = indices;
	index_data.SysMemPitch = 0;
	index_data.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&index_buffer_desc, &index_data, &index_buffer_);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}

void Model::LoadModel(WCHAR* filename) {
	// Process model file
	std::ifstream file_stream;
	int file_size = 0;

	file_stream.open(filename, std::ifstream::in);

	if (file_stream.is_open() == false)
		MessageBox(NULL, filename, L"Missing Model File", MB_OK);

	file_stream.seekg(0, std::ios::end);
	file_size = (int)file_stream.tellg();
	file_stream.seekg(0, std::ios::beg);

	if (file_size <= 0)
		MessageBox(NULL, filename, L"Model file empty", MB_OK);

	char *buffer = new char[file_size];

	if (buffer == 0)
		MessageBox(NULL, filename, L"Model buffer is to small", MB_OK);

	memset(buffer, '\0', file_size);

	TokenStream token_stream, line_stream, face_stream;
	std::string temp_line, token;

	file_stream.read(buffer, file_size);
	token_stream.SetTokenStream(buffer);

	delete[] buffer;

	token_stream.ResetStream();

	std::vector<float> verts, norms, texC;
	std::vector<int> faces;

	char line_delimiters[2] = { '\n', ' ' };

	while (token_stream.MoveToNextLine(&temp_line)) {
		line_stream.SetTokenStream((char*)temp_line.c_str());
		token_stream.GetNextToken(0, 0, 0);

		if (!line_stream.GetNextToken(&token, line_delimiters, 2))
			continue;

		if (strcmp(token.c_str(), "v") == 0) {
			line_stream.GetNextToken(&token, line_delimiters, 2);
			verts.push_back((float)atof(token.c_str()));

			line_stream.GetNextToken(&token, line_delimiters, 2);
			verts.push_back((float)atof(token.c_str()));

			line_stream.GetNextToken(&token, line_delimiters, 2);
			verts.push_back((float)atof(token.c_str()));
		} else if (strcmp(token.c_str(), "vn") == 0) {
			line_stream.GetNextToken(&token, line_delimiters, 2);
			norms.push_back((float)atof(token.c_str()));

			line_stream.GetNextToken(&token, line_delimiters, 2);
			norms.push_back((float)atof(token.c_str()));

			line_stream.GetNextToken(&token, line_delimiters, 2);
			norms.push_back((float)atof(token.c_str()));
		} else if (strcmp(token.c_str(), "vt") == 0) {
			line_stream.GetNextToken(&token, line_delimiters, 2);
			texC.push_back((float)atof(token.c_str()));

			line_stream.GetNextToken(&token, line_delimiters, 2);
			texC.push_back((float)atof(token.c_str()));
		} else if (strcmp(token.c_str(), "f") == 0) {
			char faceTokens[3] = { '\n', ' ', '/' };
			std::string faceIndex;

			face_stream.SetTokenStream((char*)temp_line.c_str());
			face_stream.GetNextToken(0, 0, 0);

			for (int i = 0; i < 3; i++) {
				face_stream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));

				face_stream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));

				face_stream.GetNextToken(&faceIndex, faceTokens, 3);
				faces.push_back((int)atoi(faceIndex.c_str()));
			}
		} else if (strcmp(token.c_str(), "#") == 0) {
			int a = 0;
			int b = a;
		}

		token[0] = '\0';
	}

	// "Unroll" the loaded obj information into a list of triangles.

	int vertex_index = 0;
	int numFaces = (int)faces.size() / 9;

	// Create the model using the vertex count that was read in.
	vertex_count_ = numFaces * 3;

	// Delete everything if it already exists.
	if (model_) {
		delete[] model_;
		model_ = 0;
	}
	model_ = new ModelType[vertex_count_];

	for (int f = 0; f < (int)faces.size(); f += 3) {
		model_[vertex_index].x = verts[(faces[f + 0] - 1) * 3 + 0];
		model_[vertex_index].y = verts[(faces[f + 0] - 1) * 3 + 1];
		model_[vertex_index].z = verts[(faces[f + 0] - 1) * 3 + 2];
		model_[vertex_index].tu = texC[(faces[f + 1] - 1) * 2 + 0];
		model_[vertex_index].tv = texC[(faces[f + 1] - 1) * 2 + 1];
		model_[vertex_index].nx = norms[(faces[f + 2] - 1) * 3 + 0];
		model_[vertex_index].ny = norms[(faces[f + 2] - 1) * 3 + 1];
		model_[vertex_index].nz = norms[(faces[f + 2] - 1) * 3 + 2];

		//increase index count
		vertex_index++;

	}
	index_count_ = vertex_index;

	verts.clear();
	norms.clear();
	texC.clear();
	faces.clear();
}