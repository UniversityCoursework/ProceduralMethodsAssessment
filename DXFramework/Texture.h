#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <d3d11.h>
#include <string>
#include <fstream>

#include "../DirectXTK/Inc/DDSTextureLoader.h"
#include "../DirectXTK/Inc/WICTextureLoader.h"

using namespace DirectX;

/// <summary>
/// Texture Class wraps up loading and storing texture information.
/// Stores the texture in a ID3D11ShaderResourceView.
/// </summary>
class Texture {
public:

	/// <summary>
	/// Loads the texture into memory.
	/// </summary>	
	/// <param name="filename"> Filename of the texture. </param>
	Texture(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* filename);
	~Texture();

	ID3D11ShaderResourceView* GetTexture();

private:

	bool DoesFileExist(const WCHAR *fileName);

	ID3D11ShaderResourceView* texture_;
};

#endif