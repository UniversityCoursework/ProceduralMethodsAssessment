#ifndef _TIMER_H_
#define _TIMER_H_

#include <windows.h>

/// <summary>
/// Creates a high frequency timer.
/// </summary>
class Timer {
public:
	Timer();
	~Timer();

	/// <summary>
	/// Calculates the frame time and restarts the timer.
	/// </summary>
	void Frame();

	/// <summary>
	/// The last frame time.
	/// </summary>
	float GetTime();

private:
	INT64 frequency_;
	float ticks_per_second_;
	INT64 start_time_;
	float frame_time_;
};

#endif