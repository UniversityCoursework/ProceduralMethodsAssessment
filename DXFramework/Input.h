#ifndef INPUT_H
#define INPUT_H

#include <Windows.h>

/// <summary>
/// Simple class for handling Mouse and Keyboard Input.
/// </summary>
class Input {
	/// <summary>
	/// Mouse information struct.
	/// </summary>
	struct Mouse {
		int x, y;
		bool left, right, is_active;
	};

public:
	void SetKeyDown(WPARAM key);
	void SetKeyUp(WPARAM key);

	bool IsKeyDown(int key);
	void SetMouseX(int x_position);
	void SetMouseY(int y_position);
	int GetMouseX();
	int GetMouseY();
	void SetLeftMouse(bool down);
	void SetRightMouse(bool down);
	bool IsLeftMouseDown();
	bool IsRightMouseDown();
	void SetMouseActive(bool active);
	bool IsMouseActive();

private:
	bool keys[256];
	Mouse mouse;
};

#endif