#ifndef _LIGHT_GAMEOBJECT_H
#define _LIGHT_GAMEOBJECT_H

#include "Light.h"

#include "GameObject.h"

/// <summary>
/// Light object for controlling and modifing Lights in the editor.
/// </summary>
class LightObject : public GameObject {
public:
	LightObject();
	LightObject(std::string name);
	~LightObject();

	/// <summary>
	/// The light this object represents.
	/// Will be used to determine where to render a Light gizmo if enabled.
	/// Will show all the settings for the Light allowing the light to be modified.
	/// </summary>
	/// <param name="particle_emitter"> New Light. </param>
	void SetLight(Light* light);

	/// <summary>
	/// Sets the position of the light.
	/// Override to set the position of the light, and the light gizmo's mesh.
	/// </summary>
	void SetPosition(float x, float y, float z) override;

	Light* GetLight();

	/// <summary>
	/// Renders the Inspector for the light.
	/// Exposes the lighting information for the light.
	/// </summary>
	void RenderInspector() override;

private:
	/// <summary>
	/// Shows a drop down list for changing the Light Type in ImGui.
	/// </summary>
	void ExposeChangeType();

	/// <summary>
	/// Shows the Light Position in ImGui.
	/// </summary>
	void ExposePosition();

	/// <summary>
	/// Shows the Light Direction in ImGui.
	/// </summary>
	void ExposeDirection();

	/// <summary>
	/// Shows the Light Position in ImGui.
	/// </summary>
	void ExposeAmbient();

	/// <summary>
	/// Shows the Light Diffuse colour, specular colour and specular power in ImGui.
	/// </summary>
	void ExposeColour();

	/// <summary>
	/// Shows the Light Attenuation settings in ImGui.
	/// </summary>
	void ExposeAttenuation();

private:
	Light* light_;
};

#endif

