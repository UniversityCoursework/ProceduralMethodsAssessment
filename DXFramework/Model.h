#ifndef _MODEL_H_
#define _MODEL_H_

#include "BaseMesh.h"
#include "TokenStream.h"
#include <vector>

using namespace DirectX;

/// <summary>
/// Loads a .obj and creates a mesh object from the data.
/// </summary>
class Model : public BaseMesh {
	struct ModelType {
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

public:
	/// <summary>
	/// Loads the model from the .obj and generates the vertex data, and loads the texture.
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	/// <param name="filename"> The model .obj filename. </param>
	Model(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, WCHAR* filename);
	~Model();

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// Filled with the ModelType data organised to be rendered.
	/// </summary>
	void InitBuffers(ID3D11Device* device);
	
	/// <summary>
	/// Loads the model from the filename.
	/// Adds all the .obj data to the ModelType.
	/// </summary>
	/// <param name="filename"></param>
	void LoadModel(WCHAR* filename);

	ModelType* model_;
};

#endif