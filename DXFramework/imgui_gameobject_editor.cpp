#include "imgui_gameobject_editor.h"

const int kMaxSelected = 16;

void GameObjectEditorWindow(GameObject& gameobject) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(280, io.DisplaySize.y - 20);
	ImVec2 window_pos = ImVec2(io.DisplaySize.x - window_size.x, 20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);

	ImGui::Begin("Inspector", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders);

	gameobject.RenderInspector();

	ImGui::End();
}

void ImGui::Editor(bool* is_open, std::vector<GameObject*> gameobjects) {
	ImGuiIO& io = ImGui::GetIO();
	ImVec2 window_size = ImVec2(110, io.DisplaySize.y - 20);
	ImVec2 window_pos = ImVec2(0, 20);

	ImGui::SetNextWindowSize(window_size);
	ImGui::SetNextWindowPos(window_pos);
	if (!ImGui::Begin("Outliner", is_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_ShowBorders)) {
		ImGui::End();
		return;
	}

	// Possible selectable items.
	static int selectedObject = 0;

	bool selected[kMaxSelected] = { false };
	selected[selectedObject] = true;
	// Render selectable boxes for all the possible game objects.
	for (int i = 0; i < (int)gameobjects.size(); i++) {
		ImGui::PushID(i);
		if (ImGui::Selectable(gameobjects[i]->Name().c_str(), &selected[i], 0, ImVec2(0, 12))) {
			// if it is selected set it to the selected object, only one item can be picked each frame.
			selectedObject = i;
		}
		ImGui::PopID();
	}

	// Display the Inspector for the currently selected game object.
	GameObjectEditorWindow(*gameobjects[selectedObject]);

	ImGui::End();
}
