#include "baseshader.h"

BaseShader::BaseShader(ID3D11Device* device, HWND hwnd) {
	device_ = device;
	hwnd_ = hwnd;
}

BaseShader::~BaseShader() {
	
	// Release the pixel shader.
	if (pixel_shader_) {
		pixel_shader_->Release();
		pixel_shader_ = 0;
	}

	// Release the vertex shader.
	if (vertex_shader_) {
		vertex_shader_->Release();
		vertex_shader_ = 0;
	}

	// Release the hull shader.
	if (hull_shader_) {
		hull_shader_->Release();
		hull_shader_ = 0;
	}

	// Release the domain shader.
	if (domain_shader_) {
		domain_shader_->Release();
		domain_shader_ = 0;
	}

	// Release the geometry shader.
	if (geometry_shader_) {
		geometry_shader_->Release();
		geometry_shader_ = 0;
	}

	// Release the layout.
	if (layout_) {
		layout_->Release();
		layout_ = 0;
	}

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}
}

void BaseShader::LoadVertexShader(WCHAR* filename) {

	HRESULT result;
	ID3DBlob* error_message;
	ID3DBlob* vertex_shader_buffer;	

	// Initialize the pointers this function will use to null.
	error_message = 0;
	vertex_shader_buffer = 0;

	// Compile the vertex shader code.
	result = D3DCompileFromFile(filename, NULL, NULL, "main", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &vertex_shader_buffer, &error_message);
	if (FAILED(result)) {
		// If the shader failed to compile it should have writen something to the error message.
		if (error_message) {
			OutputShaderErrorMessage(error_message, hwnd_, filename);
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else {
			MessageBox(hwnd_, filename, L"Missing Shader File", MB_OK);
		}
		exit(0);
	}

	CreateInputBufferLayout(vertex_shader_buffer);

	// Create the vertex shader from the buffer.
	result = device_->CreateVertexShader(vertex_shader_buffer->GetBufferPointer(), vertex_shader_buffer->GetBufferSize(), NULL, &vertex_shader_);
	if (FAILED(result)) {
		//return false;
	}

	// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
	vertex_shader_buffer->Release();
	vertex_shader_buffer = 0;
}

void BaseShader::CreateInputBufferLayout(ID3DBlob* vertex_shader_buffer) {
	HRESULT result;
	D3D11_INPUT_ELEMENT_DESC polygon_layout[4];
	unsigned int number_of_elements;
	// Create the vertex input layout description.
	// This setup needs to match the VertexInput stucture in the MeshClass and in the shader.
	polygon_layout[0].SemanticName = "POSITION";
	polygon_layout[0].SemanticIndex = 0;
	polygon_layout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[0].InputSlot = 0;
	polygon_layout[0].AlignedByteOffset = 0;
	polygon_layout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[0].InstanceDataStepRate = 0;

	polygon_layout[1].SemanticName = "TEXCOORD";
	polygon_layout[1].SemanticIndex = 0;
	polygon_layout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygon_layout[1].InputSlot = 0;
	polygon_layout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygon_layout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[1].InstanceDataStepRate = 0;

	polygon_layout[2].SemanticName = "NORMAL";
	polygon_layout[2].SemanticIndex = 0;
	polygon_layout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[2].InputSlot = 0;
	polygon_layout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygon_layout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygon_layout[2].InstanceDataStepRate = 0;

	// Extra layout for instance data for instanced rendering.
	polygon_layout[3].SemanticName = "INSTANCE";
	polygon_layout[3].SemanticIndex = 0;
	polygon_layout[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygon_layout[3].InputSlot = 1;
	polygon_layout[3].AlignedByteOffset = 0;
	polygon_layout[3].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	polygon_layout[3].InstanceDataStepRate = 1;

	// Get a count of the elements in the layout.
	number_of_elements = sizeof(polygon_layout) / sizeof(polygon_layout[0]);

	// Create the vertex input layout.
	result = device_->CreateInputLayout(polygon_layout, number_of_elements, vertex_shader_buffer->GetBufferPointer(), vertex_shader_buffer->GetBufferSize(),
		&layout_);
	if (FAILED(result)) {
		//return false;
	}
}

void BaseShader::LoadPixelShader(WCHAR* filename) {
	HRESULT result;
	ID3DBlob* error_message;
	ID3DBlob* pixel_shader_buffer;

	// Compile the pixel shader code.
	result = D3DCompileFromFile(filename, NULL, NULL, "main", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &pixel_shader_buffer, &error_message);
	if (FAILED(result)) {
		// If the shader failed to compile it should have writen something to the error message.
		if (error_message) {
			OutputShaderErrorMessage(error_message, hwnd_, filename);
		}
		// If there was  nothing in the error message then it simply could not find the file itself.
		else {
			MessageBox(hwnd_, filename, L"Missing Shader File", MB_OK);
		}

		exit(0);
	}

	// Create the pixel shader from the buffer.
	result = device_->CreatePixelShader(pixel_shader_buffer->GetBufferPointer(), pixel_shader_buffer->GetBufferSize(), NULL, &pixel_shader_);

	pixel_shader_buffer->Release();
	pixel_shader_buffer = 0;
}

void BaseShader::LoadHullShader(WCHAR* filename) {
	HRESULT result;
	ID3D10Blob* error_message;
	ID3D10Blob* hull_shader_buffer;

	// Compile the hull shader code.
	result = D3DCompileFromFile(filename, NULL, NULL, "main", "hs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &hull_shader_buffer, &error_message);
	if (FAILED(result)) {
		// If the shader failed to compile it should have writen something to the error message.
		if (error_message) {
			OutputShaderErrorMessage(error_message, hwnd_, filename);
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else {
			MessageBox(hwnd_, filename, L"Missing Shader File", MB_OK);
		}

		exit(0);
	}

	// Create the hull shader from the buffer.
	result = device_->CreateHullShader(hull_shader_buffer->GetBufferPointer(), hull_shader_buffer->GetBufferSize(), NULL, &hull_shader_);

	hull_shader_buffer->Release();
	hull_shader_buffer = 0;
}

void BaseShader::LoadDomainShader(WCHAR* filename) {
	HRESULT result;
	ID3D10Blob* error_message;
	ID3D10Blob* domain_shader_buffer;

	// Compile the domain shader code.
	result = D3DCompileFromFile(filename, NULL, NULL, "main", "ds_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &domain_shader_buffer, &error_message);
	if (FAILED(result)) {
		// If the shader failed to compile it should have writen something to the error message.
		if (error_message) {
			OutputShaderErrorMessage(error_message, hwnd_, filename);
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else {
			MessageBox(hwnd_, filename, L"Missing Shader File", MB_OK);
		}

		exit(0);
	}

	// Create the domain shader from the buffer.
	result = device_->CreateDomainShader(domain_shader_buffer->GetBufferPointer(), domain_shader_buffer->GetBufferSize(), NULL, &domain_shader_);

	domain_shader_buffer->Release();
	domain_shader_buffer = 0;
}

void BaseShader::LoadGeometryShader(WCHAR* filename) {
	HRESULT result;
	ID3D10Blob* error_message;
	ID3D10Blob* geometry_shader_buffer;

	// Compile the domain shader code.
	result = D3DCompileFromFile(filename, NULL, NULL, "main", "gs_5_0", D3DCOMPILE_ENABLE_STRICTNESS, 0, &geometry_shader_buffer, &error_message);
	if (FAILED(result)) {
		// If the shader failed to compile it should have writen something to the error message.
		if (error_message) {
			OutputShaderErrorMessage(error_message, hwnd_, filename);
		}
		// If there was nothing in the error message then it simply could not find the shader file itself.
		else {
			MessageBox(hwnd_, filename, L"Missing Shader File", MB_OK);
		}

		exit(0);
	}

	// Create the domain shader from the buffer.
	device_->CreateGeometryShader(geometry_shader_buffer->GetBufferPointer(), geometry_shader_buffer->GetBufferSize(), NULL, &geometry_shader_);

	geometry_shader_buffer->Release();
	geometry_shader_buffer = 0;
}

void BaseShader::OutputShaderErrorMessage(ID3D10Blob* error_message, HWND hwnd, WCHAR* shader_filename) {
	char* compile_errors;
	unsigned long buffer_size, i;
	ofstream file_stream_out;
	
	// Get a pointer to the error message text buffer.
	compile_errors = (char*)(error_message->GetBufferPointer());

	// Get the length of the message.
	buffer_size = error_message->GetBufferSize();

	// Open a file to write the error message to.
	file_stream_out.open("shader-error.txt");

	// Write out the error message.
	for (i = 0; i < buffer_size; i++) {
		file_stream_out << compile_errors[i];
	}

	// Close the file.
	file_stream_out.close();

	// Release the error message.
	error_message->Release();
	error_message = 0;

	// Pop a message up on the screen to notify the user to check the text file for compile errors.
	MessageBox(hwnd, L"Error compiling shader.  Check shader-error.txt for message.", shader_filename, MB_OK);
}

void BaseShader::SetShaders(ID3D11DeviceContext * device_context) {
	// Set the vertex input layout.
	device_context->IASetInputLayout(layout_);

	// Set the vertex and pixel shaders that will be used to render.
	device_context->VSSetShader(vertex_shader_, NULL, 0);
	device_context->PSSetShader(pixel_shader_, NULL, 0);

	// if Hull shader is not null then set HS and DS
	if (hull_shader_) {
		device_context->HSSetShader(hull_shader_, NULL, 0);
		device_context->DSSetShader(domain_shader_, NULL, 0);
	} else {
		device_context->HSSetShader(NULL, NULL, 0);
		device_context->DSSetShader(NULL, NULL, 0);
	}

	// if geometry shader is not null then set GS
	if (geometry_shader_) {
		device_context->GSSetShader(geometry_shader_, NULL, 0);
	} else {
		device_context->GSSetShader(NULL, NULL, 0);
	}

}

void BaseShader::Render(ID3D11DeviceContext* device_context, int index_count) {
	SetShaders(device_context);

	device_context->DrawIndexed(index_count, 0, 0);
}

void BaseShader::RenderInstance(ID3D11DeviceContext * device_context, int vertex_count, int instance_count) {
	SetShaders(device_context);

	device_context->DrawInstanced(vertex_count, instance_count, 0, 0);
}
