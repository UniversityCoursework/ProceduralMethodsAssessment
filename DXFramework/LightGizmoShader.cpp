#include "LightGizmoShader.h"

LightGizmoShader::LightGizmoShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"../DXFramework/shaders/light_gizmo_vs.hlsl", L"../DXFramework/shaders/light_gizmo_ps.hlsl");
}

LightGizmoShader::~LightGizmoShader() {

	// Release the matrix constant buffer.
	if (matrix_buffer_) {
		matrix_buffer_->Release();
		matrix_buffer_ = 0;
	}

	if (colour_buffer_) {
		colour_buffer_->Release();
		colour_buffer_ = 0;
	}

	//Release base shader components
	BaseShader::~BaseShader();
}

void LightGizmoShader::InitShader(WCHAR* vs_filename, WCHAR* ps_filename) {
	D3D11_BUFFER_DESC matrix_buffer_description;
	D3D11_BUFFER_DESC color_buffer_description;

	// Load (+ compile) shader files
	LoadVertexShader(vs_filename);
	LoadPixelShader(ps_filename);

	// Setup the Matrix Buffer.
	// Vertex Shader.
	matrix_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	matrix_buffer_description.ByteWidth = sizeof(MatrixBufferType);
	matrix_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrix_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrix_buffer_description.MiscFlags = 0;
	matrix_buffer_description.StructureByteStride = 0;

	// Create the constant buffer pointer.
	device_->CreateBuffer(&matrix_buffer_description, NULL, &matrix_buffer_);

	// Setup colour Buffer.
	// Pixel Shader.
	color_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	color_buffer_description.ByteWidth = sizeof(ColourBufferType);
	color_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	color_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	color_buffer_description.MiscFlags = 0;
	color_buffer_description.StructureByteStride = 0;

	// Create the constant buffer pointer.
	device_->CreateBuffer(&color_buffer_description, NULL, &colour_buffer_);

}

void LightGizmoShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, XMFLOAT4 light_colour) {

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mapped_resource;
	MatrixBufferType* data_ptr;
	ColourBufferType* colour_ptr;
	unsigned int buffer_number;
	XMMATRIX transposed_world_matrix, transposed_view_matrix, transposed_projection_matrix;

	// Setup the Matrix Buffer.
	// Vertex Shader.
	// Transpose the matrices to prepare them for the shader.
	transposed_world_matrix = XMMatrixTranspose(world_matrix);
	transposed_view_matrix = XMMatrixTranspose(view_matrix);
	transposed_projection_matrix = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);
	// Get a pointer to the data in the constant buffer.
	data_ptr = (MatrixBufferType*)mapped_resource.pData;
	// Copy the matrices into the constant buffer.
	data_ptr->world = transposed_world_matrix;
	data_ptr->view = transposed_view_matrix;
	data_ptr->projection = transposed_projection_matrix;
	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);
	// Set the position of the constant buffer in the vertex shader.
	buffer_number = 0;
	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(buffer_number, 1, &matrix_buffer_);


	// Setup colour buffer.
	// Pixel Shader.	
	// Lock the constant buffer so it can be written to.
	device_context->Map(colour_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// get a pointer to the data in the constant buffer
	colour_ptr = (ColourBufferType*)mapped_resource.pData;
	// Set Values
	colour_ptr->colour = light_colour;

	// unlock the buffer
	device_context->Unmap(colour_buffer_, 0);
	buffer_number = 0;
	// set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(buffer_number, 1, &colour_buffer_);

}
