#ifndef _GAME_SETTINGS_H
#define _GAME_SETTINGS_H

#include "ConfigIni.h"

/// <summary>
/// Simple Wrapper for saving Game Settings to .ini files.
/// </summary>
class GameSettings {
public:
	
	/// <summary>
	/// Gets the screen resolution stored in the .ini.
	/// </summary>
	/// <param name="width"> Will be updated with new width if succesfull. </param>
	/// <param name="height"> Will be updated with new height if succesfull. </param>
	/// <returns> True if successfully found. </returns>
	static bool GetScreenResolution(int& width, int& height);
	
	/// <summary>
	/// Returns the Wireframe mode stored in the .ini.
	/// </summary>
	/// <returns> Returns state of Wireframe Mode, Defaults to False if not Found.</returns>
	static bool GetWireFrameMode();

	/// <summary>
	/// Returns the Windowed mode stored in the .ini.
	/// </summary>
	/// <returns> Returns state of Windowed Mode, Defaults to False if not Found.</returns>
	static bool GetWindowedMode();

	/// <summary>
	/// Sets the screen resolution in the .ini to a the new values.
	/// Creates the entry if it is not found.
	/// </summary>
	static void SetScreenResolution(const int width, const int height);

	/// <summary>
	/// Sets the wireframe mode in the .ini to the new value.
	/// Creates the entry if it is not found.
	/// </summary>
	static void SetWireFrameMode(const bool is_on);

	/// <summary>
	/// Sets the windowed mode in the .ini to the new value.
	/// Creates the entry if it is not found.
	/// </summary>
	static void SetWindowedMode(const bool is_on);
};

#endif
