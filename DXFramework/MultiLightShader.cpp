#include "MultiLightShader.h"

MultiLightShader::MultiLightShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd) {
	InitShader(L"../DXFramework/shaders/multi_light_vs.hlsl", L"../DXFramework/shaders/multi_light_ps.hlsl");
}

MultiLightShader::~MultiLightShader() {
	//Release base shader components
	BaseShader::~BaseShader();

	// Release the sampler state.
	if (sample_state_) {
		sample_state_->Release();
		sample_state_ = 0;
	}

	// Release the light constant buffer.
	if (light_buffer_) {
		light_buffer_->Release();
		light_buffer_ = 0;
	}

	// Release the camera constant buffer.
	if (camera_buffer_) {
		camera_buffer_->Release();
		camera_buffer_ = 0;
	}
}

void MultiLightShader::InitShader(WCHAR* vs_filename, WCHAR* ps_filename) {
	D3D11_BUFFER_DESC matrix_buffer_description;
	D3D11_SAMPLER_DESC sampler_description;
	D3D11_BUFFER_DESC light_buffer_description;
	D3D11_BUFFER_DESC camera_buffer_description;

	// Load (+ compile) shader files
	LoadVertexShader(vs_filename);
	LoadPixelShader(ps_filename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrix_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	matrix_buffer_description.ByteWidth = sizeof(MatrixBufferType);
	matrix_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrix_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrix_buffer_description.MiscFlags = 0;
	matrix_buffer_description.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&matrix_buffer_description, NULL, &matrix_buffer_);

	// Create a texture sampler state description.
	sampler_description.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampler_description.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_description.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_description.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	sampler_description.MipLODBias = 0.0f;
	sampler_description.MaxAnisotropy = 1;
	sampler_description.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampler_description.BorderColor[0] = 0;
	sampler_description.BorderColor[1] = 0;
	sampler_description.BorderColor[2] = 0;
	sampler_description.BorderColor[3] = 0;
	sampler_description.MinLOD = 0;
	sampler_description.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	device_->CreateSamplerState(&sampler_description, &sample_state_);

	// Setup light buffer
	// Setup the description of the light dynamic constant buffer that is in the pixel shader.
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	light_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	light_buffer_description.ByteWidth = sizeof(LightBufferType);
	light_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	light_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	light_buffer_description.MiscFlags = 0;
	light_buffer_description.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&light_buffer_description, NULL, &light_buffer_);

	// Setup the camera buffer	
	// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
	camera_buffer_description.Usage = D3D11_USAGE_DYNAMIC;
	camera_buffer_description.ByteWidth = sizeof(CameraBufferType);
	camera_buffer_description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	camera_buffer_description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	camera_buffer_description.MiscFlags = 0;
	camera_buffer_description.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	device_->CreateBuffer(&camera_buffer_description, NULL, &camera_buffer_);

}

void MultiLightShader::SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix,
	ID3D11ShaderResourceView* texture, Light *light[MAXLIGHTS], XMFLOAT3 camera_position) {

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mapped_resource;
	MatrixBufferType* data_ptr;
	LightBufferType* light_ptr;
	CameraBufferType* camera_ptr;
	unsigned int buffer_number;
	XMMATRIX transposed_world_matrix, transposed_view_matrix, transposed_projection_matrix;
	
	// Transpose the matrices to prepare them for the shader.
	transposed_world_matrix = XMMatrixTranspose(world_matrix);
	transposed_view_matrix = XMMatrixTranspose(view_matrix);
	transposed_projection_matrix = XMMatrixTranspose(projection_matrix);

	// Lock the constant buffer so it can be written to.
	result = device_context->Map(matrix_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	data_ptr = (MatrixBufferType*)mapped_resource.pData;

	// Copy the matrices into the constant buffer.
	data_ptr->world = transposed_world_matrix;// world_matrix;
	data_ptr->view = transposed_view_matrix;
	data_ptr->projection = transposed_projection_matrix;

	// Unlock the constant buffer.
	device_context->Unmap(matrix_buffer_, 0);

	// Set the position of the constant buffer in the vertex shader.
	buffer_number = 0;

	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(buffer_number, 1, &matrix_buffer_);

	// Additional
	// Send camera data to vertex shader
	// Lock the constant buffer so it can be written to.
	result = device_context->Map(camera_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// Get a pointer to the data in the constant buffer.
	camera_ptr = (CameraBufferType*)mapped_resource.pData;
	camera_ptr->camera_position = camera_position;
	camera_ptr->padding = 0.0f;

	// Unlock the constant buffer.
	device_context->Unmap(camera_buffer_, 0);

	buffer_number = 1;
	// Now set the constant buffer in the vertex shader with the updated values.
	device_context->VSSetConstantBuffers(buffer_number, 1, &camera_buffer_);


	//Additional
	// Send light data to pixel shader

	// Lock the light to the constant buffer so it can be written to.
	device_context->Map(light_buffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_resource);

	// get a pointer to the data in the constant buffer
	light_ptr = (LightBufferType*)mapped_resource.pData;

	for (int i = 0; i < MAXLIGHTS; i++) {
		// Copy light data into the buffer		
		light_ptr->ambient[i] = light[i]->GetAmbientColour();
		light_ptr->diffuse[i] = light[i]->GetDiffuseColour();
		light_ptr->specular[i] = light[i]->GetSpecularColour();
		light_ptr->position[i] = XMFLOAT4(light[i]->GetPosition().x, light[i]->GetPosition().y, light[i]->GetPosition().z, light[i]->GetSpecularPower());
		light_ptr->attenuation[i] = XMFLOAT4(light[i]->GetConstantFactor(), light[i]->GetLinearFactor(), light[i]->GetQuadraticFactor(), light[i]->GetRange());
		light_ptr->direction[i] = XMFLOAT4(light[i]->GetDirection().x, light[i]->GetDirection().y, light[i]->GetDirection().z, 0);
		light_ptr->type[i] = XMINT4((int)light[i]->GetType(), 0, 0, 0);
	}

	// unlock the buffer
	device_context->Unmap(light_buffer_, 0);
	buffer_number = 0;
	// set the constant buffer in the pixel shader
	device_context->PSSetConstantBuffers(buffer_number, 1, &light_buffer_);

	// Set shader texture resource in the pixel shader.
	device_context->PSSetShaderResources(0, 1, &texture);
}

void MultiLightShader::Render(ID3D11DeviceContext* device_context, int index_count) {
	// Set the sampler state in the pixel shader.
	device_context->PSSetSamplers(0, 1, &sample_state_);

	// Base render function.
	BaseShader::Render(device_context, index_count);
}



