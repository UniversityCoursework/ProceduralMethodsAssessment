#include "spheremesh.h"

SphereMesh::SphereMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution) {
	resolution_ = resolution;
	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, device_context, texture_filename);
}

SphereMesh::~SphereMesh() {
	// Run parent deconstructor
	BaseMesh::~BaseMesh();
}

void SphereMesh::InitBuffers(ID3D11Device* device) {
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertex_buffer_desc, index_buffer_desc;
	D3D11_SUBRESOURCE_DATA vertex_data, index_data;

	// Calculate vertex count
	// 6 vertices per quad, res*res is face, times 6 for each face
	// Set the number of vertices in the vertex array.
	vertex_count_ = ((6 * resolution_)*resolution_) * 6;

	// Set the number of indices in the index array.
	index_count_ = vertex_count_;

	// Create the vertex array.
	vertices = new VertexType[vertex_count_];

	// Create the index array.
	indices = new unsigned long[index_count_];

	// Vertex variables
	float y_increment = 2.0f / resolution_;
	float x_increment = 2.0f / resolution_;
	float y_start = 1.0f;
	float x_start = -1.0f;
	//UV variables
	float tx_u = 0.0f;
	float tx_v = 0.0f;
	float tx_u_increment = 1.0f / resolution_;	// UV increment
	float tx_v_increment = 1.0f / resolution_;
	//Counters
	int v = 0;	// vertex counter
	int i = 0;	// index counter

	//front face

	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(x_start, y_start - y_increment, -1.0f);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(x_start + x_increment, y_start, -1.0f);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start, y_start, -1.0f);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(x_start, y_start - y_increment, -1.0f);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(x_start + x_increment, y_start - y_increment, -1.0f);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(x_start + x_increment, y_start, -1.0f);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start += x_increment;
			tx_u += tx_u_increment;
			//y_start -= y_increment;

		}

		y_start -= y_increment;
		x_start = -1;

		tx_u = 0;
		tx_v += tx_v_increment;

	}

	tx_v = 0;

	//back face
	y_start = 1;
	x_start = 1;

	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(x_start, y_start - y_increment, 1.0f);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start - x_increment, y_start, 1.0f);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(x_start, y_start, 1.0f);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(x_start, y_start - y_increment, 1.0f);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(x_start - x_increment, y_start - y_increment, 1.0f);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start - x_increment, y_start, 1.0f);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 0.0f, 1.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start -= x_increment;
			//y_start -= y_increment;
			tx_u += tx_u_increment;

		}

		y_start -= y_increment;
		x_start = 1;

		tx_u = 0;
		tx_v += tx_v_increment;

	}

	tx_v = 0;

	//right face
	y_start = 1;
	x_start = -1;
	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(1.0f, y_start - y_increment, x_start);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(1.0f, y_start, x_start + x_increment);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(1.0f, y_start, x_start);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(1.0f, y_start - y_increment, x_start);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(1.0f, y_start - y_increment, x_start + x_increment);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(1.0f, y_start, x_start + x_increment);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start += x_increment;
			//y_start -= y_increment;
			tx_u += tx_u_increment;

		}

		y_start -= y_increment;
		x_start = -1;
		tx_u = 0;
		tx_v += tx_v_increment;
	}

	tx_v = 0;

	//left face
	y_start = 1;
	x_start = 1;

	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(-1.0f, y_start - y_increment, x_start);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(-1.0f, y_start, x_start - x_increment);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(-1.0f, y_start, x_start);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(-1.0f, y_start - y_increment, x_start);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(-1.0f, y_start - y_increment, x_start - x_increment);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(-1.0f, y_start, x_start - x_increment);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(-1.0f, 0.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start -= x_increment;
			//y_start -= y_increment;
			tx_u += tx_u_increment;
		}

		y_start -= y_increment;
		x_start = 1;
		tx_u = 0;
		tx_v += tx_v_increment;
	}

	tx_v = 0;

	//top face
	y_start = 1;
	x_start = -1;

	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(x_start, 1.0f, y_start - y_increment);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start + x_increment, 1.0f, y_start);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(x_start, 1.0f, y_start);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(x_start, 1.0f, y_start - y_increment);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(x_start + x_increment, 1.0f, y_start - y_increment);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start + x_increment, 1.0f, y_start);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, 1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start += x_increment;
			//y_start -= y_increment;
			tx_u += tx_u_increment;
		}

		y_start -= y_increment;
		x_start = -1;
		tx_u = 0;
		tx_v += tx_v_increment;
	}

	tx_v = 0;

	//bottom face
	y_start = -1;
	x_start = -1;

	// for each quad in the y direction
	for (int y = 0; y < resolution_; y++) {
		// for each quad in the x direction
		for (int x = 0; x < resolution_; x++) {
			// Load the vertex array with data.
			//0
			vertices[v].position = XMFLOAT3(x_start, -1.0f, y_start + y_increment);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start + x_increment, -1.0f, y_start);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//1
			vertices[v].position = XMFLOAT3(x_start, -1.0f, y_start);  // Top left.	-1.0, 1.0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//0
			vertices[v].position = XMFLOAT3(x_start, -1.0f, y_start + y_increment);  // Bottom left. -1. -1. 0
			vertices[v].texture = XMFLOAT2(tx_u, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//3
			vertices[v].position = XMFLOAT3(x_start + x_increment, -1.0f, y_start + y_increment);  // Bottom right.	1.0, -1.0, 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v + tx_v_increment);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			//2
			vertices[v].position = XMFLOAT3(x_start + x_increment, -1.0f, y_start);  // Top right.	1.0, 1.0 0.0
			vertices[v].texture = XMFLOAT2(tx_u + tx_u_increment, tx_v);
			vertices[v].normal = XMFLOAT3(0.0f, -1.0f, 0.0f);

			indices[i] = i;
			v++;
			i++;

			// increment
			x_start += x_increment;
			//y_start -= y_increment;
			tx_u += tx_u_increment;
		}

		y_start += y_increment;
		x_start = -1;
		tx_u = 0;
		tx_v += tx_v_increment;
	}

	// now loop over every vertex and bend into a sphere (normalise the vertices)
	float x = 0;
	float y = 0;
	float z = 0;
	float dx = 0;
	float dy = 0;
	float dz = 0;

	for (int counter = 0; counter < v; counter++) {
		x = vertices[counter].position.x;
		y = vertices[counter].position.y;
		z = vertices[counter].position.z;

		dx = x * sqrtf(1.0f - (y*y / 2.0f) - (z*z / 2.0f) + (y*y*z*z / 3.0f));
		dy = y * sqrtf(1.0f - (z*z / 2.0f) - (x*x / 2.0f) + (z*z*x*x / 3.0f));
		dz = z * sqrtf(1.0f - (x*x / 2.0f) - (y*y / 2.0f) + (x*x*y*y / 3.0f));

		vertices[counter].position.x = dx;
		vertices[counter].position.y = dy;
		vertices[counter].position.z = dz;

		vertices[counter].normal.x = dx;
		vertices[counter].normal.y = dy;
		vertices[counter].normal.z = dz;
	}

	// Set up the description of the static vertex buffer.
	vertex_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	vertex_buffer_desc.ByteWidth = sizeof(VertexType)* vertex_count_;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.CPUAccessFlags = 0;
	vertex_buffer_desc.MiscFlags = 0;
	vertex_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertex_data.pSysMem = vertices;
	vertex_data.SysMemPitch = 0;
	vertex_data.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertex_buffer_desc, &vertex_data, &vertex_buffer_);

	// Set up the description of the static index buffer.
	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	index_buffer_desc.ByteWidth = sizeof(unsigned long)* index_count_;
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.CPUAccessFlags = 0;
	index_buffer_desc.MiscFlags = 0;
	index_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	index_data.pSysMem = indices;
	index_data.SysMemPitch = 0;
	index_data.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&index_buffer_desc, &index_data, &index_buffer_);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}
