#include "rendertexture.h"

RenderTexture::RenderTexture(ID3D11Device* device, int texture_width, int texture_height, float screen_near, float screen_far) {
	// Store the width and height of the render texture.
	texture_width_ = texture_width;
	texture_height_ = texture_height;

	screen_near_ = screen_near;
	screen_depth_ = screen_far;

	SetBuffers(device);
}

RenderTexture::~RenderTexture() {
	ReleaseBuffers();
}

void RenderTexture::SetRenderTarget(ID3D11DeviceContext* device_context) {
	// Bind the render target view and depth stencil buffer to the output render pipeline.
	device_context->OMSetRenderTargets(1, &render_target_view_, depth_stencil_view_);

	// Set the viewport.
	device_context->RSSetViewports(1, &viewport_);
}

void RenderTexture::ClearRenderTarget(ID3D11DeviceContext* device_context, float red, float green, float blue, float alpha) {
	float color[4];

	// Setup the color to clear the buffer to.
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	// Clear the back buffer.
	device_context->ClearRenderTargetView(render_target_view_, color);

	// Clear the depth buffer.
	device_context->ClearDepthStencilView(depth_stencil_view_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

ID3D11ShaderResourceView* RenderTexture::GetShaderResourceView() {
	return shader_resource_view_;
}

XMMATRIX RenderTexture::GetProjectionMatrix() {
	return projection_matrix_;
}

XMMATRIX RenderTexture::GetOrthoMatrix() {
	return ortho_matrix_;
}

int RenderTexture::GetTextureWidth() {
	return texture_width_;
}

int RenderTexture::GetTextureHeight() {
	return texture_height_;
}

void RenderTexture::ResizeTexture(ID3D11Device * device, int width, int height) {
	// Store the width and height of the render texture.
	texture_width_ = width;
	texture_height_ = height;
	SetBuffers(device);
}

void RenderTexture::SetBuffers(ID3D11Device * device) {
	D3D11_TEXTURE2D_DESC texture_description;
	HRESULT result;
	D3D11_RENDER_TARGET_VIEW_DESC render_target_view_description;
	D3D11_SHADER_RESOURCE_VIEW_DESC shader_resource_view_description;
	D3D11_TEXTURE2D_DESC depth_buffer_description;
	D3D11_DEPTH_STENCIL_VIEW_DESC depth_stencil_view_description;

	// Initialize the render target texture description.
	ZeroMemory(&texture_description, sizeof(texture_description));

	// Setup the render target texture description.
	texture_description.Width = texture_width_;
	texture_description.Height = texture_height_;
	texture_description.MipLevels = 1;
	texture_description.ArraySize = 1;
	texture_description.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	texture_description.SampleDesc.Count = 1;
	texture_description.Usage = D3D11_USAGE_DEFAULT;
	texture_description.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	texture_description.CPUAccessFlags = 0;
	texture_description.MiscFlags = 0;

	// Create the render target texture.
	result = device->CreateTexture2D(&texture_description, NULL, &render_target_texture_);

	// Setup the description of the render target view.
	render_target_view_description.Format = texture_description.Format;
	render_target_view_description.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	render_target_view_description.Texture2D.MipSlice = 0;

	// Create the render target view.
	result = device->CreateRenderTargetView(render_target_texture_, &render_target_view_description, &render_target_view_);

	// Setup the description of the shader resource view.
	shader_resource_view_description.Format = texture_description.Format;
	shader_resource_view_description.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shader_resource_view_description.Texture2D.MostDetailedMip = 0;
	shader_resource_view_description.Texture2D.MipLevels = 1;

	// Create the shader resource view.
	result = device->CreateShaderResourceView(render_target_texture_, &shader_resource_view_description, &shader_resource_view_);

	// Initialize the description of the depth buffer.
	ZeroMemory(&depth_buffer_description, sizeof(depth_buffer_description));

	// Set up the description of the depth buffer.
	depth_buffer_description.Width = texture_width_;
	depth_buffer_description.Height = texture_height_;
	depth_buffer_description.MipLevels = 1;
	depth_buffer_description.ArraySize = 1;
	depth_buffer_description.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_buffer_description.SampleDesc.Count = 1;
	depth_buffer_description.SampleDesc.Quality = 0;
	depth_buffer_description.Usage = D3D11_USAGE_DEFAULT;
	depth_buffer_description.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depth_buffer_description.CPUAccessFlags = 0;
	depth_buffer_description.MiscFlags = 0;

	// Create the texture for the depth buffer using the filled out description.
	result = device->CreateTexture2D(&depth_buffer_description, NULL, &depth_stencil_buffer_);

	// Initialize the depth stencil view.
	ZeroMemory(&depth_stencil_view_description, sizeof(depth_stencil_view_description));

	// Set up the depth stencil view description.
	depth_stencil_view_description.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depth_stencil_view_description.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depth_stencil_view_description.Texture2D.MipSlice = 0;

	// Create the depth stencil view.
	result = device->CreateDepthStencilView(depth_stencil_buffer_, &depth_stencil_view_description, &depth_stencil_view_);

	// Setup the viewport for rendering.
	viewport_.Width = (float)texture_width_;
	viewport_.Height = (float)texture_height_;
	viewport_.MinDepth = 0.0f;
	viewport_.MaxDepth = 1.0f;
	viewport_.TopLeftX = 0.0f;
	viewport_.TopLeftY = 0.0f;

	// Setup the projection matrix.
	projection_matrix_ = XMMatrixPerspectiveFovLH(((float)XM_PI / 4.0f), ((float)texture_width_ / (float)texture_height_), screen_near_, screen_depth_);

	// Create an orthographic projection matrix for 2D rendering.
	ortho_matrix_ = XMMatrixOrthographicLH((float)texture_width_, (float)texture_height_, screen_near_, screen_depth_);
}

void RenderTexture::ReleaseBuffers() {
	if (depth_stencil_view_) {
		depth_stencil_view_->Release();
		depth_stencil_view_ = 0;
	}

	if (depth_stencil_buffer_) {
		depth_stencil_buffer_->Release();
		depth_stencil_buffer_ = 0;
	}

	if (shader_resource_view_) {
		shader_resource_view_->Release();
		shader_resource_view_ = 0;
	}

	if (render_target_view_) {
		render_target_view_->Release();
		render_target_view_ = 0;
	}

	if (render_target_texture_) {
		render_target_texture_->Release();
		render_target_texture_ = 0;
	}
}
