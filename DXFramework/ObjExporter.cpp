#include "ObjExporter.h"

#include "DebugOutput.h"

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	const std::string kObjSeperator = " ";
	const std::string kObjFaceSeperator = "/";

	void ObjExporter::WriteToObj(std::string file_name) {

		file_stream_.open(file_name, std::ios_base::out | std::ios_base::trunc);
		file_stream_ << "# Exported @StewMcC" << std::endl;
		// Output all the vertices.
		for each (XMFLOAT3 v in v_) {
			file_stream_ << "v" << kObjSeperator
				<< v.x << kObjSeperator
				<< v.y << kObjSeperator
				<< v.z << std::endl;
		}

		// Output all the uv's.
		for each (XMFLOAT2 vt in vt_) {
			file_stream_ << "vt" << kObjSeperator
				<< vt.x << kObjSeperator
				<< vt.y << std::endl;

		}
		// Output all the normals.
		for each (XMFLOAT3 vn in vn_) {
			file_stream_ << "vn" << kObjSeperator
				<< vn.x << kObjSeperator
				<< vn.y << kObjSeperator
				<< vn.z << std::endl;

		}

		// Output all the faces.
		for each (Face f in f_) {
			file_stream_ << "f" << kObjSeperator;
			for (size_t i = 0; i < 3; i++) {
				file_stream_
					<< f.v[i] << kObjFaceSeperator
					<< f.vt[i] << kObjFaceSeperator
					<< f.vn[i] << kObjSeperator;
			}
			file_stream_ << std::endl;
		}

		// Save and close the file, then clear all the data.
		file_stream_.close();
		file_stream_.clear();
		DeleteAllDate();
	}

	void ObjExporter::DeleteAllDate() {
		f_.clear();
		v_.clear();
		vt_.clear();
		vn_.clear();
	}

	void ObjExporter::AddVertex(XMFLOAT3 position) {
		v_.push_back(position);
	}

	void ObjExporter::AddTextureCoordinate(XMFLOAT2 texture_coord) {
		vt_.push_back(texture_coord);
	}

	void ObjExporter::AddNormal(XMFLOAT3 normal) {
		vn_.push_back(normal);
	}

	void ObjExporter::AddFace(Face new_face) {
		f_.push_back(new_face);
	}
}
