#ifndef _GIZMO_SHADER_H
#define _GIZMO_SHADER_H

#include "BaseShader.h"

using namespace std;
using namespace DirectX;

/// <summary>
/// Light Gizmo Shader.
/// Used to render light positions with diffuse colour.
/// </summary>
class LightGizmoShader : public BaseShader {
private:
	/// <summary>
	/// Color Buffer used to pass the Gizmo's Color the shaders.
	/// </summary>
	struct ColourBufferType {
		XMFLOAT4 colour;
	};

public:
	LightGizmoShader(ID3D11Device* device, HWND hwnd);
	~LightGizmoShader();

	/// <summary>
	/// Loads the matrices and the colour data into the correct buffers for the shader.
	/// </summary>	
	/// <param name="light_colour"> The colour to render the mesh as. </param>
	void SetShaderParameters(ID3D11DeviceContext* device_context, const XMMATRIX &world_matrix, const XMMATRIX &view_matrix, const XMMATRIX &projection_matrix, XMFLOAT4 light_colour);
private:

	/// <summary>
	/// Loads the shaders, and initialises the shader buffers descriptions.	
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader to load. </param>
	/// <param name="ps_filename"> Pixel Shader to load. </param>
	void InitShader(WCHAR* vs_filename, WCHAR* ps_filename);

private:	
	ID3D11Buffer* colour_buffer_;
};

#endif