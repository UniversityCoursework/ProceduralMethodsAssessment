// Very Simple Shader for rendering a set colour.

cbuffer ColourBuffer : register(cb0) {
	float4 colour;
}

struct InputType {
	float4 position : SV_POSITION;	
};


float4 main(InputType input) : SV_TARGET {
	return colour;
}