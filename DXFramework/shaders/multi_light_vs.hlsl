// Calculates all the values for the correct lighting calculations.

cbuffer MatrixBuffer : register(cb0) {
	matrix world_matrix;
	matrix view_matrix;
	matrix projection_matrix;
};

cbuffer CameraBuffer : register(cb1) {
	float3 camera_position;
	float padding;
};

struct InputType {
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType {
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float3 view_direction : TEXCOORD1;
	float3 world_space_position : TEXCOORD2;
};

OutputType main(InputType input) {
	OutputType output;
	float4	world_position;

	// Change the position vector to be 4 units for proper matrix calculations.
	input.position.w = 1.0f;

	// calculate the position of the vertext in the world
	world_position = mul(input.position, world_matrix);

	// Calculate the position of the vertex against the world, view, and projection matrices.	
	output.position = mul(world_position, view_matrix);
	output.position = mul(output.position, projection_matrix);

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	// Calculate the normal vector against the world matrix only.
	output.normal = mul(input.normal, (float3x3)world_matrix);
	// Normalize the normal vector.
	output.normal = normalize(output.normal);

	// Determine the viewing direction based on the position of the camera.
	output.view_direction = normalize(camera_position.xyz - world_position.xyz);
	// world position of the vertex
	output.world_space_position = world_position.xyz;

	return output;
}