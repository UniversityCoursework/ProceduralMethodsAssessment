#include "LightObject.h"

#include <assert.h>

#include "imgui.h"

LightObject::LightObject()
	: GameObject() {
}

LightObject::LightObject(std::string name)
	: LightObject() {
	name_ = name;
}

LightObject::~LightObject() {
	mesh_ = nullptr;
	light_ = nullptr;
}

void LightObject::SetLight(Light * light) {
	light_ = light;
}

void LightObject::SetPosition(float x, float y, float z) {
	position_ = XMVectorSet(x, y, z, 1.0f);
	assert(light_);
	if (light_) {
		light_->SetPosition(x, y, z);
	}
}

Light * LightObject::GetLight() {
	assert(light_);
	if (light_) {
		return light_;
	}
	return nullptr;
}

void LightObject::RenderInspector() {
	ExposeChangeType();
	switch (light_->GetType()) {
		case kAmbient:
			ExposeAmbient();
			break;
		case kPoint:
			ExposePosition();
			ExposeColour();
			ExposeAttenuation();
			break;
		case kDirectional:
			ExposeDirection();
			ExposeColour();
			break;
		case kOff:
			break;
		default:
			break;
	}
}

void LightObject::ExposeChangeType() {
	int item = static_cast<int>(light_->GetType());
	ImGui::PushItemWidth(160);
	ImGui::Combo("LightType", &item, "Ambient\0Point\0Directional\0Off\0");
	light_->SetType(static_cast<LightType>(item));
}

void LightObject::ExposePosition() {
	XMFLOAT3 position = light_->GetPosition();
	ImGui::DragFloat3("Position", &position.x, 0.1f);
	light_->SetPosition(position.x, position.y, position.z);
}

void LightObject::ExposeDirection() {
	XMFLOAT3 direction = light_->GetDirection();
	ImGui::SliderFloat3("Direction", &direction.x, -1.0f, 1.0f);
	light_->SetDirection(direction.x, direction.y, direction.z);
}

void LightObject::ExposeAmbient() {
	XMFLOAT4 colour = light_->GetAmbientColour();
	ImGui::ColorEdit4("Ambient", &colour.x);
	light_->SetAmbientColour(colour.x, colour.y, colour.z, colour.w);
}

void LightObject::ExposeColour() {
	XMFLOAT4 colour = light_->GetDiffuseColour();
	ImGui::ColorEdit4("Diffuse", &colour.x);
	light_->SetDiffuseColour(colour.x, colour.y, colour.z, colour.w);

	colour = light_->GetSpecularColour();
	ImGui::ColorEdit4("Specular", &colour.x);
	light_->SetSpecularColour(colour.x, colour.y, colour.z, colour.w);
	float AccessValue = 0.0f;
	AccessValue = light_->GetSpecularPower();
	ImGui::SliderFloat("Specular", &AccessValue, 2.0f, 100.f, "Power : %.3f");
	light_->SetSpecularPower(AccessValue);

}

void LightObject::ExposeAttenuation() {
	ImGui::Text("Attenuation");
	float AccessValue = light_->GetConstantFactor();
	ImGui::SliderFloat("Constant", &AccessValue, 0.0f, 10.f, "Constant : %.3f");
	light_->SetConstantFactor(AccessValue);

	AccessValue = light_->GetLinearFactor();
	ImGui::SliderFloat("Linear", &AccessValue, 0.0f, 10.f, "Linear : %.3f");
	light_->SetLinearFactor(AccessValue);

	AccessValue = light_->GetQuadraticFactor();
	ImGui::SliderFloat("Quadratic", &AccessValue, 0.0f, 1.f, "Quadratic : %.3f");
	light_->SetQuadraticFactor(AccessValue);

	AccessValue = light_->GetRange();
	ImGui::SliderFloat("Range", &AccessValue, 0.0f, 100.f, "Range : %.3f");
	light_->SetRange(AccessValue);
}