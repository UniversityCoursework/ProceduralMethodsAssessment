#include "orthomesh.h"

OrthoMesh::OrthoMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, int width, int height, int x_position, int y_position) {
	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	width_ = width;
	height_ = height;
	x_position_ = x_position;
	y_position_ = y_position;

	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, device_context, NULL);
}

OrthoMesh::~OrthoMesh() {
	BaseMesh::~BaseMesh();
}

void OrthoMesh::ResizeMesh(ID3D11Device * device, int width, int height) {
	// Release the index buffer.
	if (index_buffer_) {
		index_buffer_->Release();
		index_buffer_ = 0;
	}

	// Release the vertex buffer.
	if (vertex_buffer_) {
		vertex_buffer_->Release();
		vertex_buffer_ = 0;
	}

	width_ = width;
	height_ = height;
	InitBuffers(device);
}

void OrthoMesh::InitBuffers(ID3D11Device* device) {
	float left, right, top, bottom;
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertex_buffer_desc, index_buffer_desc;
	D3D11_SUBRESOURCE_DATA vertex_data, index_data;

	// Calculate the screen coordinates of the left side of the window.
	left = (float)((width_ / 2) * -1) + x_position_;

	// Calculate the screen coordinates of the right side of the window.
	right = left + (float)width_;

	// Calculate the screen coordinates of the top of the window.
	top = (float)(height_ / 2) + y_position_;

	// Calculate the screen coordinates of the bottom of the window.
	bottom = top - (float)height_;

	// Set the number of vertices in the vertex array.
	vertex_count_ = 6;

	// Set the number of indices in the index array.
	index_count_ = vertex_count_;

	// Create the vertex array.
	vertices = new VertexType[vertex_count_];

	// Create the index array.
	indices = new unsigned long[index_count_];

	// Load the vertex array with data.
	vertices[0].position = XMFLOAT3(left, bottom, 0.0f);  // Bottom left.
	vertices[0].texture = XMFLOAT2(0.0f, 1.0f);
	vertices[0].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

	vertices[1].position = XMFLOAT3(left, top, 0.0f);  // Top left.
	vertices[1].texture = XMFLOAT2(0.0f, 0.0f);
	vertices[1].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

	vertices[2].position = XMFLOAT3(right, top, 0.0f);  // Top right.
	vertices[2].texture = XMFLOAT2(1.0f, 0.0f);
	vertices[2].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

	vertices[3].position = XMFLOAT3(right, bottom, 0.0f);  // Bottom right.
	vertices[3].texture = XMFLOAT2(1.0f, 1.0f);
	vertices[3].normal = XMFLOAT3(0.0f, 0.0f, -1.0f);

	// Load the index array with data.
	indices[0] = 0;  // Bottom left.
	indices[1] = 2;  // Top right.
	indices[2] = 1;  // Top left.

	indices[3] = 0;	// bottom left
	indices[4] = 3;	// bottom right
	indices[5] = 2;	// top right

	// Set up the description of the vertex buffer.
	vertex_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	vertex_buffer_desc.ByteWidth = sizeof(VertexType)* vertex_count_;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.CPUAccessFlags = 0;
	vertex_buffer_desc.MiscFlags = 0;
	vertex_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertex_data.pSysMem = vertices;
	vertex_data.SysMemPitch = 0;
	vertex_data.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
	device->CreateBuffer(&vertex_buffer_desc, &vertex_data, &vertex_buffer_);

	// Set up the description of the index buffer.
	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	index_buffer_desc.ByteWidth = sizeof(unsigned long)* index_count_;
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.CPUAccessFlags = 0;
	index_buffer_desc.MiscFlags = 0;
	index_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	index_data.pSysMem = indices;
	index_data.SysMemPitch = 0;
	index_data.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&index_buffer_desc, &index_data, &index_buffer_);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}
