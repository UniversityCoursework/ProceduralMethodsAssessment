#ifndef _OBJ_EXPORTER_H_
#define _OBJ_EXPORTER_H_

#include <string>
#include <vector>
#include <fstream>

#include "DirectXMath.h"

using namespace DirectX;


/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	/// <summary>
	/// Simple exporter for .obj files Slow and unoptimized.
	/// Will output provided info only, does not optimize or modify the info,
	/// in anyway.
	/// </summary>
	class ObjExporter {

	public:
		/// <summary>
		/// Stores indices of obj face.
		/// </summary>
		struct Face {
			int v[3] = { 0 };
			int vt[3] = { 0 };
			int vn[3] = { 0 };
		};
				
		/// <summary>
		/// Writes all currently stored information to a file.
		/// </summary>
		/// <param name="file_name"> The file which it will be written to. </param>
		void WriteToObj(std::string file_name);

		/// <summary>
		/// Deletes any currently stored data.
		/// </summary>
		void DeleteAllDate();

		/// <summary>
		/// Stores a Vertex that will be added to the obj file.
		/// </summary>
		/// <param name="position"> The position of the vertex. </param>
		void AddVertex(XMFLOAT3 position);

		/// <summary>
		/// Stores a Texture Coordinate that will be added to the obj file.
		/// </summary>
		/// <param name="texture_coord"> The U,V coordinates of the texture coord. </param>
		void AddTextureCoordinate(XMFLOAT2 texture_coord);

		/// <summary>
		/// Stores a Normal that will be added to the obj file.
		/// </summary>
		/// <param name="normal"> The x,y,z values of the normal, Note: will not be normalized. </param>
		void AddNormal(XMFLOAT3 normal);

		/// <summary>
		/// Stores a face refrence the indices of the 3 points on the triangle.
		/// </summary>
		/// <see cref="Face"/>
		/// <param name="new_face"> The face to be stored. </param>
		void AddFace(Face new_face);

	private:
		std::fstream file_stream_;
		std::string file_name_;

		std::vector<Face> f_;
		std::vector<XMFLOAT3> v_;
		std::vector<XMFLOAT2> vt_;
		std::vector<XMFLOAT3> vn_;

	};

}
#endif
