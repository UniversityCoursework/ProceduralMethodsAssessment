#include "GameObject.h"

#include "imgui.h"

GameObject::GameObject() {
	mesh_ = nullptr;
	position_ = XMVectorSet(0, 0, 0, 1.0f);
	euler_rotation_ = XMVectorSet(0, 0, 0, 1.0f);
	scale_ = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
	name_ = "GameObject";
}

GameObject::GameObject(std::string name)
	:GameObject() {
	name_ = name;
}

GameObject::~GameObject() {
	mesh_ = nullptr;
}

void GameObject::SetName(std::string name) {
	name_ = name;
}

void GameObject::SetMesh(BaseMesh * mesh) {
	mesh_ = mesh;
}

void GameObject::SetPosition(float x, float y, float z) {
	position_ = XMVectorSet(x, y, z, 1.0f);
}

void GameObject::SetEulerRotation(float pitch, float yaw, float roll) {
	// Convert the Input values of degrees to radians, for XMMatrix calculations latter on.
	euler_rotation_ = XMVectorSet(XMConvertToRadians(pitch), XMConvertToRadians(yaw), XMConvertToRadians(roll), 1.0f);
}

void GameObject::SetScale(float x, float y, float z) {
	scale_ = XMVectorSet(x, y, z, 1.0f);
}

void GameObject::SetUniformScale(float new_scale) {
	scale_ = XMVectorSet(new_scale, new_scale, new_scale, 1.0f);
}

std::string GameObject::Name() {
	return name_;
}

BaseMesh * GameObject::Mesh() {
	return mesh_;
}

XMFLOAT3 GameObject::Position() {
	XMFLOAT3 temp(XMVectorGetX(position_), XMVectorGetY(position_), XMVectorGetZ(position_));
	return temp;
}

XMFLOAT3 GameObject::EularRotation() {
	XMFLOAT3 temp(XMConvertToDegrees(XMVectorGetX(euler_rotation_)),
		XMConvertToDegrees(XMVectorGetY(euler_rotation_)),
		XMConvertToDegrees(XMVectorGetZ(euler_rotation_)));
	return temp;
}

XMFLOAT3 GameObject::Scale() {
	XMFLOAT3 temp(XMVectorGetX(scale_), XMVectorGetY(scale_), XMVectorGetZ(scale_));
	return temp;
}

XMMATRIX GameObject::WorldMatrix() {
	// Generates a world matrix, from the scale,rotate, and position values.
	// SRT - moves object to position, rotates it to the set amount, then scales it by scale.	
	return XMMatrixScalingFromVector(scale_)* XMMatrixRotationRollPitchYawFromVector(euler_rotation_) * XMMatrixTranslationFromVector(position_);
}

void GameObject::RenderInspector() {
	ExposePosition();
	ExposeScale();
	ExposeEulerRotation();
}

void GameObject::ExposePosition() {
	XMFLOAT3 position = Position();
	ImGui::DragFloat3("Position", &position.x, 0.1f);
	SetPosition(position.x, position.y, position.z);
}

void GameObject::ExposeScale() {
	XMFLOAT3 scale = Scale();
	ImGui::DragFloat3("Scale", &scale.x, 0.1f);
	SetScale(scale.x, scale.y, scale.z);

	if (ImGui::SmallButton("Set Scale to X")) {
		SetScale(scale.x, scale.x, scale.x);
	}
}

void GameObject::ExposeEulerRotation() {
	XMFLOAT3 rotation = EularRotation();
	ImGui::SliderFloat3("Rotation", &rotation.x, -180.0f, 180.0f, "%.0f");
	SetEulerRotation(rotation.x, rotation.y, rotation.z);
}

