#ifndef _DEBUG_OUTPUT_H_
#define _DEBUG_OUTPUT_H_

#include <string>
#include <sstream>
#include <windows.h>

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	void static DebugToOutput(std::string text) {
	#ifdef _DEBUG
		std::wstringstream string_stream;
		string_stream << text.c_str() << std::endl;
		OutputDebugString(string_stream.str().c_str());
	#endif
	}
}


#endif
