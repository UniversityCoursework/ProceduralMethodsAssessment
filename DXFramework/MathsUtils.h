#ifndef _MATH_UTILS_H
#define _MATH_UTILS_H

#include <algorithm>

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 06/12/16
/// </summary>
namespace LittleLot {

	/// <summary>
	/// Value of PI to Double accuracy. 
	/// </summary>
	const double kMathPie = 3.14159265358979323846;

	///<summary> Converts the value passed in from degrees into radians. </summary>
	inline float DegreesToRadians(float angle_in_degrees) {
		return angle_in_degrees* ((float)kMathPie / 180.0f);
	}

	///<summary> Converts the value passed in from radians into degrees. </summary>
	inline float RadiansToDegrees(float angle_in_radians) {
		return angle_in_radians*(180.0f / (float)kMathPie);
	}

	/// <summary> Lerps between 2 floats, i.e.Smooth movement. </summary>
	/// <param name = 'time'> between 0 and 1 </param>
	/// <returns> Value generated between the two values at time value. </returns>	
	inline float Lerp(float start, float end, float time) {
		// This method is mathematically correct, but due to floating point error the other is prefered. As rounding is done before the multiplication so accuracy is lost?
		// return start + (end - start) *time;	// common lerp
		// return a + t * (b - a);				// perlin noise lerp		

		return (1.0f - time)*start + time*end;
	}

	/// <summary> Lerps between 2 floats, i.e.Smooth movement. </summary>
	/// <param name = 'time'> between 0 and 1 </param>
	/// <returns> Value generated between the two values at time value. </returns>	
	inline double Lerp(double start, double end, double time) {
		return (1.0f - time)*start + time*end;
	}

	/// <summary>Clamp a value between an upper and lower range, works on any class that has comparision operators declared. </summary>
	/// <param name="n"> The value to be clamped. </param>
	/// <param name="upper"> The upper range value. </param>
	/// <param name="lower"> The lower range value. </param>
	/// <returns> std::max(lower, std::min(n, upper)) . </returns>	
	template <typename T>
	T Clamp(const T& n, const T& lower, const T& upper) {
		return (std::max)(lower, (std::min)(n, upper));
	}

	/// <summary> The Sign of the value provided, be aware of -0,0 floating error precision. </summary>
	/// <param name = 'val'> Value you wish to know the sign of. </param>
	/// <returns> Val's Sign -1, or 1. (-,+) </returns>	
	template <typename T>
	inline T Sign(T val) {
		if (val < 0) {
			return -1;
		} else {
			return 1;
		}
	}
}
#endif
