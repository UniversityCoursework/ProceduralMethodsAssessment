#include "camera.h"

Camera::Camera() {
	position_x_ = 0.0f;
	position_y_ = 0.0f;
	position_z_ = 0.0f;

	rotation_x_ = 0.0f;
	rotation_y_ = 0.0f;
	rotation_z_ = 0.0f;

	look_speed_ = 4.0f;
}

Camera::~Camera() {
}

void Camera::SetFrameTime(float t) {
	frame_time_ = t;
}

void Camera::SetPosition(float x, float y, float z) {
	position_x_ = x;
	position_y_ = y;
	position_z_ = z;
}

void Camera::SetRotation(float x, float y, float z) {
	rotation_x_ = x;
	rotation_y_ = y;
	rotation_z_ = z;
}

XMFLOAT3 Camera::GetPosition() {
	XMFLOAT3 pos(position_x_, position_y_, position_z_);
	return pos;
}

XMVECTOR Camera::GetRotation() {
	XMVECTOR rot;
	rot = XMVectorSet(rotation_x_, rotation_y_, rotation_z_, 1.0f);
	return rot;
}

void Camera::Update() {
	XMVECTOR up, position, look_at;
	float yaw, pitch, roll;
	XMMATRIX rotation_matrix;
	
	// Setup the vector that points upwards.
	up = XMVectorSet(0.0f, 1.0, 0.0, 1.0f);

	// Setup the position of the camera in the world.
	position = XMVectorSet(position_x_, position_y_, position_z_, 1.0f);

	// Setup where the camera is looking by default.
	look_at = XMVectorSet(0.0, 0.0, 1.0f, 1.0f);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = XMConvertToRadians(rotation_x_);
	yaw = XMConvertToRadians(rotation_y_);
	roll = XMConvertToRadians(rotation_z_);

	// Create the rotation matrix from the yaw, pitch, and roll values.
	rotation_matrix = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	// Transform the look_at and up vector by the rotation matrix so the view is correctly rotated at the origin.
	look_at = XMVector3TransformCoord(look_at, rotation_matrix);
	up = XMVector3TransformCoord(up, rotation_matrix);
	
	// Translate the rotated camera position to the location of the viewer.
	look_at = position + look_at;

	// Finally create the view matrix from the three updated vectors.
	view_matrix_ = XMMatrixLookAtLH(position, look_at, up);
}

void Camera::GetViewMatrix(XMMATRIX& view_matrix) {
	view_matrix = view_matrix_;
}

void Camera::GetBaseViewMatrix(XMMATRIX& baseMatrix) {
	XMVECTOR up, position, look_at;

	// Setup the vector that points upwards.
	up = XMVectorSet(0.0f, 1.0, 0.0, 1.0f);

	// Setup the position of the camera in the world.
	position = XMVectorSet(0.0f, 0.0, -10.0, 1.0f);

	// Setup where the camera is looking by default.
	look_at = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0);

	// Finally create the view matrix from the three updated vectors.
	baseMatrix = XMMatrixLookAtLH(position, look_at, up);
}

void Camera::MoveForward() {
	float radians;

	// Update the forward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Convert degrees to radians.
	radians = XMConvertToRadians(rotation_y_);

	// Update the position.
	position_x_ += sinf(radians) * speed_;
	position_z_ += cosf(radians) * speed_;
}

void Camera::MoveBackward() {
	float radians;

	// Update the backward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Convert degrees to radians.
	radians = XMConvertToRadians(rotation_y_);

	// Update the position.
	position_x_ -= sinf(radians) * speed_;
	position_z_ -= cosf(radians) * speed_;
}

void Camera::MoveUpward() {
	// Update the upward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Update the height position.
	position_y_ += speed_;
}

void Camera::MoveDownward() {
	// Update the downward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Update the height position.
	position_y_ -= speed_;
}

void Camera::TurnLeft() {
	// Update the left turn movement based on the frame time 
	speed_ = frame_time_ *kTurnMoveSpeedMultiplier;

	// Update the rotation.
	rotation_y_ -= speed_;

	// Keep the rotation in the 0 to 360 range.
	if (rotation_y_ < 0.0f) {
		rotation_y_ += 360.0f;
	}
}

void Camera::TurnRight() {
	// Update the right turn movement based on the frame time
	speed_ = frame_time_ * kTurnMoveSpeedMultiplier;

	// Update the rotation.
	rotation_y_ += speed_;

	// Keep the rotation in the 0 to 360 range.
	if (rotation_y_ > 360.0f) {
		rotation_y_ -= 360.0f;
	}
}

void Camera::TurnUp() {
	// Update the upward rotation movement based on the frame time
	speed_ = frame_time_ * kTurnMoveSpeedMultiplier;

	// Update the rotation.
	rotation_x_ -= speed_;

	// Keep the rotation maximum 90 degrees.
	if (rotation_x_ > 90.0f) {
		rotation_x_ = 90.0f;
	}
}

void Camera::TurnDown() {
	// Update the downward rotation movement based on the frame time
	speed_ = frame_time_ * kTurnMoveSpeedMultiplier;

	// Update the rotation.
	rotation_x_ += speed_;

	// Keep the rotation maximum 90 degrees.
	if (rotation_x_ < -90.0f) {
		rotation_x_ = -90.0f;
	}
}

void Camera::Turn(int x, int y) {
	rotation_y_ += (float)x / look_speed_;
	rotation_x_ += (float)y / look_speed_;
}

void Camera::StrafeRight() {
	float radians;

	// Update the forward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Convert degrees to radians.
	radians = XMConvertToRadians(rotation_y_);

	// Update the position.
	position_z_ -= sinf(radians) * speed_;
	position_x_ += cosf(radians) * speed_;

}

void Camera::StrafeLeft() {
	float radians;

	// Update the forward movement based on the frame time
	speed_ = frame_time_ * kMoveSpeedMultiplier;

	// Convert degrees to radians.
	radians = XMConvertToRadians(rotation_y_);

	// Update the position.
	position_z_ += sinf(radians) * speed_;
	position_x_ -= cosf(radians) * speed_;
}