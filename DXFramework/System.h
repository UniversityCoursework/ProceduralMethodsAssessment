#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include "BaseApplication.h"
#include "Input.h"

/// <summary>
/// Handles the running of the application, and windows messages.
/// Ensures the windows messages are also passed onto ImGui.
/// </summary>
class System {
public:

	/// <summary>
	/// Intiailises the WindowsApi and start the application.
	/// </summary>
	/// <param name="application"> The Application to start. </param>	
	System(BaseApplication* application);
	~System();

	/// <summary>
	/// Runs proccessing Windows events, updating and rendering the application.
	/// Until it recieves a quit message.
	/// </summary>
	void Run();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
	bool Frame();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();

private:
	LPCWSTR application_name_;
	HINSTANCE h_instance_;
	HWND hwnd_;
	BaseApplication* application_;
	Input input_;

	//Prototypes
	static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
};

//Globals
static System* kApplicationHandle = 0;

#endif