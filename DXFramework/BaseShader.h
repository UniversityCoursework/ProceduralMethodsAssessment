#ifndef _BASESHADER_H_
#define _BASESHADER_H_

#include <d3d11.h>
#include <D3Dcompiler.h>
#include <dxgi.h>
#include <DirectXMath.h>
#include <fstream>

using namespace std;
using namespace DirectX;

/// <summary>
/// Base Shader Class for inheriting functionality.
/// </summary>
class BaseShader {
protected:
	/// <summary>
	/// Default Matrix Buffer Type.
	/// </summary>
	struct MatrixBufferType {
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
	};

public:
	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}

	/// <summary>
	/// Stores the device and windows handle for later use.
	/// </summary>	
	BaseShader(ID3D11Device* device, HWND hwnd);
	~BaseShader();

	/// <summary>
	/// Renders the previous sent Vertex buffer of Index Count.
	/// Renders using DrawIndexed.
	/// </summary>	
	/// <param name="index_count"> The number of indices in the vertex buffer. </param>
	virtual void Render(ID3D11DeviceContext* device_context, int index_count);

	/// <summary>
	/// Renders the previous sent Vertex buffer with instanced data, only works with instanced data.
	/// Renders using DrawInstanced.
	/// </summary>
	/// <param name="vertex_count"> The number of vertices in the vertex buffer. </param>
	/// <param name="vertex_count"> The number of instance data in the vertex buffer. </param>	
	virtual void RenderInstance(ID3D11DeviceContext* device_context, int vertex_count, int instance_count);

protected:
	/// <summary>
	/// Loads the shaders, and initialises the shader buffers descriptions.	
	/// <para> Note: Shader requires at least a pixel and vertex shader to work.</para>
	/// </summary>
	/// <param name="vs_filename"> Vertex Shader to load. </param>
	/// <param name="ps_filename"> Pixel Shader to load. </param>
	virtual void InitShader(WCHAR* vs_filename, WCHAR* ps_filename) = 0;

	void LoadVertexShader(WCHAR* filename);

	/// <summary>
	/// Defines the polygon_layout for the input buffer.
	/// </summary>
	/// <param name="vertex_shader_buffer"></param>
	virtual void CreateInputBufferLayout(ID3DBlob* vertex_shader_buffer);

	void LoadHullShader(WCHAR* filename);
	void LoadDomainShader(WCHAR* filename);
	void LoadGeometryShader(WCHAR* filename);
	void LoadPixelShader(WCHAR* filename);

	/// <summary>
	/// Outputs the shader error to a text file, and shows a pop-up box.
	/// </summary>
	void OutputShaderErrorMessage(ID3D10Blob* error_message, HWND hwnd, WCHAR* shader_filename);

	/// <summary>
	/// Sets up the shaders and the vertex layout used by the shader .
	/// </summary>	
	void SetShaders(ID3D11DeviceContext* device_context);

protected:
	ID3D11Device* device_;
	HWND hwnd_;

	ID3D11VertexShader* vertex_shader_;
	ID3D11PixelShader* pixel_shader_;
	ID3D11HullShader* hull_shader_;
	ID3D11DomainShader* domain_shader_;
	ID3D11GeometryShader* geometry_shader_;
	ID3D11InputLayout* layout_;

	ID3D11Buffer* matrix_buffer_;
};

#endif