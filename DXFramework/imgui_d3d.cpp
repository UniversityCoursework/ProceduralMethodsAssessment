#include "imgui_d3d.h"
// Extra Librarys needed for the DirectX resolution detection.
#include <sstream>
#include <vector>
#include <utility>
#include <string>
#include "imgui_utils.h"

#include "GameSettings.h"

bool ImGui::ExposeWireFrameMenuItem(D3D * d3d) {

	bool wireframe_mode = d3d->IsWireFrameModeOn();
	if (ImGui::MenuItem("Wireframe Mode", NULL, &wireframe_mode)) {
		if (wireframe_mode) {
			GameSettings::SetWireFrameMode(true);
			d3d->TurnOnWireframe();
		} else {
			GameSettings::SetWireFrameMode(false);
			d3d->TurnOffWireframe();
		}
		// has been used so return true.
		return true;
	}
	return false;

}

bool ImGui::ExposeWireFrame(D3D * d3d) {
	bool wireframe_mode = d3d->IsWireFrameModeOn();
	if (ImGui::Checkbox("WireFrameMode", &wireframe_mode)) {
		if (wireframe_mode) {
			GameSettings::SetWireFrameMode(true);
			d3d->TurnOnWireframe();
		} else {
			GameSettings::SetWireFrameMode(false);
			d3d->TurnOffWireframe();
		}
		// has been used so return true.
		return true;
	}
	return false;
}

void ImGui::DisplayFPS() {
	ImGui::SameLine(0);
	ImGui::PushItemWidth(160);
	ImGui::Text("FPS: %.1f, %.3f ms", ImGui::GetIO().Framerate, 1000.0f / ImGui::GetIO().Framerate);
}

bool ImGui::ExposeResolutionOptions(D3D * d3d, HWND hwnd) {
	bool has_resolution_change = false;

	// Ensures it only does the expensive resolution fetching once.
	static bool has_possible_resolutions_ = false;

	// Used to store all the resolutions available on this device.
	static std::vector<std::pair<int, int>> screen_resolutions_;
	// Used to store all the resolutions in Readable format for ImGui drop down list.
	static std::vector<std::string> screen_string_enums_;

	static int selected_resolution = 1;

	// Generate Resolution list.
	if (!has_possible_resolutions_) {

		IDXGIFactory* factory;
		IDXGIAdapter* adapter;
		IDXGIOutput* adapter_output;
		DXGI_MODE_DESC* display_mode_list;
		unsigned int number_of_modes;

		// Create a DirectX graphics interface factory.
		CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

		// Use the factory to create an adapter for the primary graphics interface (video card).
		factory->EnumAdapters(0, &adapter);

		// Enumerate the primary adapter output (monitor).
		adapter->EnumOutputs(0, &adapter_output);

		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		adapter_output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &number_of_modes, NULL);

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		display_mode_list = new DXGI_MODE_DESC[number_of_modes];

		// Now fill the display mode list structures.
		adapter_output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &number_of_modes, display_mode_list);

		// Now go through all the display modes and find all the sposible screen widths/heights.
		for (unsigned int i = 0; i < number_of_modes; i++) {
			// Always add the first one.
			if (screen_resolutions_.empty()) {
				screen_resolutions_.push_back(std::make_pair(display_mode_list[i].Width, display_mode_list[i].Height));
			} else {
				// Check the rest to see if they are unique and not already in the vector.
				// Luckily displaymodelist stores then numerically so width/height will match previous if not unique.
				if ((display_mode_list[i - 1].Width != display_mode_list[i].Width) &&
					(display_mode_list[i - 1].Height != display_mode_list[i].Height)) {
					screen_resolutions_.push_back(std::make_pair(display_mode_list[i].Width, display_mode_list[i].Height));
				}
			}
			// set it up to run at 800/600 first, otherwise will run at whatever the first supported resolution is.
			if (display_mode_list[i].Width == 800 && display_mode_list[i].Height == 600) {
				selected_resolution = screen_resolutions_.size() - 1;
			}
		}

		// Store vector full of enum names for use by ImGui.
		for each (std::pair<int, int> resolution in screen_resolutions_) {
			screen_string_enums_.push_back(std::to_string(resolution.first) + "x" + std::to_string(resolution.second));
		}

		// clean up everything.

		// Release the display mode list.
		delete[] display_mode_list;
		display_mode_list = 0;

		// Release the adapter output.
		adapter_output->Release();
		adapter_output = 0;

		// Release the adapter.
		adapter->Release();
		adapter = 0;

		// Release the factory.
		factory->Release();
		factory = 0;
		has_possible_resolutions_ = true;

		// config setup
		{

			int start_width;
			int start_height;
			// See what resolution was saved.
			if (GameSettings::GetScreenResolution(start_width, start_height)) {
				// See if this resolution setting exists on current hardware.
				std::vector<std::pair<int, int>>::iterator possible_resolution;
				possible_resolution = std::find(screen_resolutions_.begin(), screen_resolutions_.end(), std::make_pair(start_width, start_height));
				// if it exists find its enum setting.
				if (possible_resolution != screen_resolutions_.end()) {
					selected_resolution = std::distance(screen_resolutions_.begin(), possible_resolution);
					d3d->ChangeResolution(screen_resolutions_[selected_resolution].first, screen_resolutions_[selected_resolution].second, hwnd);
				}
			}
			// check if it was set to fullscreen windowed mode.			
			if (GameSettings::GetWindowedMode()) {
				d3d->TurnFullScreenWindowedOn(hwnd);
			} else {
				d3d->TurnFullScreenWindowedOff(hwnd);
			}
			// check if it was set to wireframemode
			if (GameSettings::GetWireFrameMode()) {
				d3d->TurnOnWireframe();
			} else {
				d3d->TurnOffWireframe();
			}
		}
		has_resolution_change = true;
	}
		
	bool fake_fullscreen = d3d->IsFullScreenWindowed();
	bool fullscreen = d3d->IsFullscreen();

	// Visuals

	if (fullscreen) {
		ImGui::Checkbox("Exclusive Fullscreen", &fullscreen);
	} else {
		ImGui::Checkbox("Fullscreen Windowed", &fake_fullscreen);
	}

	if (fake_fullscreen && !fullscreen) {
		int not_available = 0;
		ImGui::SameLine(0);
		ImGui::PushItemWidth(160);
		ImGui::Combo("Resolution", &not_available, "Not Available");
	} else {
		ImGui::SameLine(0);
		ImGui::PushItemWidth(160);
		ImGui::Combo("Resolution", &selected_resolution, screen_string_enums_);
	}

	//Logic	

	if (fake_fullscreen) {
		if (d3d->TurnFullScreenWindowedOn(hwnd)) {
			GameSettings::SetWindowedMode(d3d->IsFullScreenWindowed());
			has_resolution_change = true;
		}
	} else {
		if (d3d->TurnFullScreenWindowedOff(hwnd)) {
			GameSettings::SetWindowedMode(d3d->IsFullScreenWindowed());
			has_resolution_change = true;
		}
		if (d3d->ChangeResolution(screen_resolutions_[selected_resolution].first, screen_resolutions_[selected_resolution].second, hwnd)) {
			GameSettings::SetScreenResolution(screen_resolutions_[selected_resolution].first, screen_resolutions_[selected_resolution].second);
			// resolution etc reset, so check if wireframe was on before hand and reset it
			if (d3d->IsWireFrameModeOn()) {
				d3d->TurnOnWireframe();
			} else {
				d3d->TurnOffWireframe();
			}
			has_resolution_change = true;
		}

	}
	if (!fullscreen) {
		d3d->TurnFullscreenOff();
	}
	return has_resolution_change;
}

