#ifndef _GAME_OBJECT_H
#define _GAME_OBJECT_H

#include "../DXFramework/basemesh.h"

#include <Windows.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <string>

using namespace DirectX;

/// <summary>
///	Holds position, rotation, scale and mesh values for an object, for use with app's Outliner, and inspector.
/// Base class for any interactable/modifiable object in the scene.
/// </summary>
class GameObject {
public:
	GameObject();
	GameObject(std::string name);
	virtual ~GameObject();

	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}
	/// <summary>
	/// Set the name to be shown in the Outliner window.
	/// Doesn't have to be Unique, but probably should.
	/// </summary>
	/// <param name="name"> New name for the gameobject.</param>
	void SetName(std::string name);

	/// <summary>
	/// Pointer to the Mesh to be used when rendering this game object.
	/// </summary>
	/// <param name="mesh"> New mesh for the gameobject.</param>
	virtual void SetMesh(BaseMesh* mesh);

	/// <summary>
	/// Set the position of the object to be used in the world matrix.
	/// </summary>
	virtual void SetPosition(float x, float y, float z);

	/// <summary>
	/// Rotate the object.
	/// Euler angles in degrees to represent each axis.
	/// </summary>
	void SetEulerRotation(float pitch, float yaw, float roll);

	/// <summary>
	/// Set the cale of the increasing the size of the mesh.
	/// </summary>	
	void SetScale(float x, float y, float z);

	/// <summary>
	/// Utility function of SetScale.
	/// Uniformily sets x,y and z of objects scale.
	/// </summary>
	/// <param name="new_scale"> Value to apply to the scales, x, y, z.</param>
	void SetUniformScale(float new_scale);

	std::string Name();

	virtual BaseMesh* Mesh();

	virtual XMFLOAT3 Position();

	XMFLOAT3 EularRotation();

	XMFLOAT3 Scale();

	/// <summary>
	/// The world matrix generated from the position, rotation and scale values.
	/// </summary>
	/// <returns> The game objects world matrix. </returns>
	XMMATRIX WorldMatrix();

	/// <summary>
	/// Renders the Imgui ui for the gameobject inspector.
	/// </summary>
	virtual void RenderInspector();

protected:

	/// <summary>
	/// Shows the Position in ImGui.
	/// </summary>
	virtual void ExposePosition();
	
	/// <summary>
	/// Shows the Scale in ImGui.
	/// </summary>
	virtual void ExposeScale();

	/// <summary>
	/// Shows the Euler Rotation (x,y,z) in ImGui.
	/// </summary>
	virtual void ExposeEulerRotation();

	BaseMesh* mesh_;

	XMVECTOR position_;
	XMVECTOR euler_rotation_;
	XMVECTOR scale_;

	// Name to be shown in the Outliner window.
	std::string name_;
};

#endif
