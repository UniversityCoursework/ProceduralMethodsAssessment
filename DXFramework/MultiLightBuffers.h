#ifndef _MULTILIGHT_BUFFER_H_
#define _MULTILIGHT_BUFFER_H_

#define MAXLIGHTS 3


/// <summary>
/// Light Buffer used to pass all lights in the scene to the shaders.
/// Used in lighting calculations.
/// </summary>
struct LightBufferType {
	XMFLOAT4 ambient[MAXLIGHTS];
	XMFLOAT4 diffuse[MAXLIGHTS];
	XMFLOAT4 specular[MAXLIGHTS];
	XMFLOAT4 position[MAXLIGHTS];
	XMFLOAT4 attenuation[MAXLIGHTS];
	XMFLOAT4 direction[MAXLIGHTS];
	XMINT4 type[MAXLIGHTS];
};

/// <summary>
/// Camera buffer use to pass the camera position for the lighting calculations.	
/// </summary>
struct CameraBufferType {
	XMFLOAT3 camera_position;
	float padding;
};

#endif