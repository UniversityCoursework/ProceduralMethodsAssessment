#ifndef _IMGUI_UTILITIES_H_
#define _IMGUI_UTILITIES_H_

#include "../DXFramework/imgui.h"

#include <vector>
#include <string>

namespace ImGui {	

	/// <summary>
	/// Function call back for ImGui's Combo button (enum drop down), takes the data from std::vector std::string 
	/// after it has been converted into a void* and casts it back to an std::string* array, then returns the char* style string to the 
	/// ImGui function. messy but no other way of doing it with ImGui as wanted a dynamic list for available resolutions.
	/// </summary>
	static auto vector_getter = [](void* vec, int idx, const char** out_text) {
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*out_text = vector.at(idx).c_str();
		return true;
	};

	/// <summary>
	/// Overloaded version of the default IMGUI Combo Box to accept std::vector of std::string's.
	/// </summary>
	/// <param name="label"> Display name. </param>
	/// <param name="current_index"> Current item selected. Will be Modified to currently selected index. </param>
	/// <param name="values"> Vector of possible selection names. </param>
	/// <returns> True if value has changed. </returns>
	static bool Combo(const char* label, int* current_index, std::vector<std::string>& values) {
		if (values.empty()) { return false; }
		return Combo(label, current_index, vector_getter,
			static_cast<void*>(&values), values.size());
	}

	/// <summary>
	/// Overloaded version of the default IMGUI List Box to accept std::vector of std::string's.
	/// </summary>
	/// <param name="label"> Display name. </param>
	/// <param name="current_index"> Current item selected. Will be Modified to currently selected index. </param>
	/// <param name="values"> Vector of possible selection names. </param>
	/// <returns> True if value has changed. </returns>
	static bool ListBox(const char* label, int* current_index, std::vector<std::string>& values) {
		if (values.empty()) { return false; }
		return ListBox(label, current_index, vector_getter,
			static_cast<void*>(&values), values.size());
	}

}
#endif
