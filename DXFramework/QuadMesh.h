#ifndef _QUADMESH_H_
#define _QUADMESH_H_

#include "BaseMesh.h"

using namespace DirectX;

/// <summary>
/// Simple Quad based mesh.
/// </summary>
class QuadMesh : public BaseMesh {

public:
	/// <summary>
	/// Initializes the Quad Mesh and generates the vertex data.	
	/// </summary>	
	/// <param name="texture_filename"> Texture to be loaded and applied to the mesh. </param>
	QuadMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename);
	~QuadMesh();

protected:
	/// <summary>
	/// Creates and sets the vertex and index buffer.
	/// </summary>
	void InitBuffers(ID3D11Device* device);

};

#endif