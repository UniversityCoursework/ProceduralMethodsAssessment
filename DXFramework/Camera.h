#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <directxmath.h>

using namespace DirectX;

/// <summary>
/// Allows viewing of the 3d world. provided correct view matrices.
/// Full freedom of movement as default.
/// </summary>
class Camera {
public:
	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}

	Camera();
	~Camera();

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);

	XMFLOAT3 GetPosition();
	XMVECTOR GetRotation();	

	void GetViewMatrix(XMMATRIX&);
	void GetBaseViewMatrix(XMMATRIX&);

	void SetFrameTime(float);

	/// <summary>
	/// Updates the view matrix to match current position and rotations.
	/// </summary>
	void Update();

	/// <summary>
	/// Moves the camera forward along its forward vector.
	/// </summary>
	void MoveForward();

	/// <summary>
	/// Moves the camera backward along its forward vector.
	/// </summary>
	void MoveBackward();

	/// <summary>
	/// Moves the camera up the y-axis.
	/// </summary>
	void MoveUpward();

	/// <summary>
	/// Moves the camera down the y-axis.
	/// </summary>
	void MoveDownward();

	/// <summary>
	/// Turns the camera left based on frame_time_ and kTurnMoveSpeedMultiplier.
	/// Ensures the rotation is kept within 0-360;
	/// </summary>
	void TurnLeft();

	/// <summary>
	/// Turns the camera right based on frame_time_ and kTurnMoveSpeedMultiplier.
	/// Ensures the rotation is kept within 0-360;
	/// </summary>
	void TurnRight();

	/// <summary>
	/// Turns the camera up based on frame_time_ and kTurnMoveSpeedMultiplier.
	/// Ensures the rotation is kept within 0-360;
	/// </summary>
	void TurnUp();

	/// <summary>
	/// Turns the camera down based on frame_time_ and kTurnMoveSpeedMultiplier.
	/// Ensures the rotation is kept within 0-360;
	/// </summary>
	void TurnDown();

	/// <summary>
	/// Moves the camera right along its right vector.
	/// </summary>
	void StrafeRight();

	/// <summary>
	/// Moves the camera left along its right vector.
	/// </summary>
	void StrafeLeft();

	/// <summary>
	/// Rotates the camera by x and y at look_speed_.
	/// </summary>
	/// <param name="x"> Change in x rotation axis.</param>
	/// <param name="y"> Change in y rotation axis. </param>
	void Turn(int x, int y);

private:
	float position_x_, position_y_, position_z_;
	float rotation_x_, rotation_y_, rotation_z_;
	XMMATRIX view_matrix_;
	float speed_, frame_time_;
	float look_speed_;

	const float kMoveSpeedMultiplier = 8.0f;
	const float kTurnMoveSpeedMultiplier = 25.0f;
};

#endif