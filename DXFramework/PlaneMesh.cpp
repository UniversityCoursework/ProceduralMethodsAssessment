#include "planemesh.h"

PlaneMesh::PlaneMesh(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* texture_filename, int resolution) {
	resolution_ = resolution;
	// Initialize the vertex and index buffer that hold the geometry for the triangle.
	InitBuffers(device);

	// Load the texture for this model.
	LoadTexture(device, device_context, texture_filename);
}

PlaneMesh::~PlaneMesh() {
	// Run parent deconstructor
	BaseMesh::~BaseMesh();
}

void PlaneMesh::InitBuffers(ID3D11Device* device) {
	VertexType* vertices;
	unsigned long* indices;
	int index, i, j;
	float position_x, position_z, u, v, increment;
	D3D11_BUFFER_DESC vertex_buffer_desc, index_buffer_desc;
	D3D11_SUBRESOURCE_DATA vertex_data, index_data;

	// Calculate the number of vertices in the terrain mesh.
	vertex_count_ = (resolution_ - 1) * (resolution_ - 1) * 8;

	// Set the index count to the same as the vertex count.
	index_count_ = vertex_count_;

	// Create the vertex array.
	vertices = new VertexType[vertex_count_];

	// Create the index array.
	indices = new unsigned long[index_count_];

	// Initialize the index to the vertex array.
	index = 0;

	// UV coords.
	u = 0;
	v = 0;
	increment = 1.0f / resolution_;


	// Load the vertex and index arrays with the terrain data.
	for (j = 0; j < (resolution_ - 1); j++) {
		for (i = 0; i < (resolution_ - 1); i++) {
			// Upper left.
			position_x = (float)i;
			position_z = (float)(j);

			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u, v);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;

			// Upper right.
			position_x = (float)(i + 1);
			position_z = (float)(j + 1);

			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u + increment, v + increment);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;


			// lower left
			position_x = (float)(i);
			position_z = (float)(j + 1);


			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u, v + increment);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;

			// Upper left
			position_x = (float)(i);
			position_z = (float)(j);

			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u, v);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;

			// Bottom right
			position_x = (float)(i + 1);
			position_z = (float)(j);

			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u + increment, v);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;

			// Upper right.
			position_x = (float)(i + 1);
			position_z = (float)(j + 1);

			vertices[index].position = XMFLOAT3(position_x, 0.0f, position_z);
			vertices[index].texture = XMFLOAT2(u + increment, v + increment);
			vertices[index].normal = XMFLOAT3(0.0, 1.0, 0.0);
			indices[index] = index;
			index++;

			u += increment;

		}

		u = 0;
		v += increment;
	}

	// Set up the description of the static vertex buffer.
	vertex_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	vertex_buffer_desc.ByteWidth = sizeof(VertexType)* vertex_count_;
	vertex_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertex_buffer_desc.CPUAccessFlags = 0;
	vertex_buffer_desc.MiscFlags = 0;
	vertex_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertex_data.pSysMem = vertices;
	vertex_data.SysMemPitch = 0;
	vertex_data.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	device->CreateBuffer(&vertex_buffer_desc, &vertex_data, &vertex_buffer_);

	// Set up the description of the static index buffer.
	index_buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	index_buffer_desc.ByteWidth = sizeof(unsigned long)* index_count_;
	index_buffer_desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	index_buffer_desc.CPUAccessFlags = 0;
	index_buffer_desc.MiscFlags = 0;
	index_buffer_desc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	index_data.pSysMem = indices;
	index_data.SysMemPitch = 0;
	index_data.SysMemSlicePitch = 0;

	// Create the index buffer.
	device->CreateBuffer(&index_buffer_desc, &index_data, &index_buffer_);

	// Release the arrays now that the buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;
}


