#include "GameSettings.h"

static const std::string ini_file = "game_settings.ini";
static const std::string ini_window_settings_section = "window";
static const std::string ini_screen_width = "width";
static const std::string ini_screen_height = "height";
static const std::string ini_wireframe_mode = "wireframe";
static const std::string ini_windowed_fullscreen_mode = "windowedfullscreen";

bool GameSettings::GetScreenResolution(int & width, int & height) {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	if (ini.GetIntValue(ini_window_settings_section, ini_screen_width, width) &&
		ini.GetIntValue(ini_window_settings_section, ini_screen_height, height)) {
		return true;
	}
	return false;
}

bool GameSettings::GetWireFrameMode() {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	bool result = false;
	if (ini.GetBoolValue(ini_window_settings_section, ini_wireframe_mode, result)) {
		if (result) {
			return true;
		}
	}
	return false;
}

bool GameSettings::GetWindowedMode() {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	bool result = false;
	if (ini.GetBoolValue(ini_window_settings_section, ini_windowed_fullscreen_mode, result)) {
		if (result) {
			return true;
		}
	}
	return false;
}

void GameSettings::SetScreenResolution(const int width, const int height) {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	ini.SetIntValue(ini_window_settings_section, ini_screen_width, width);
	ini.SetIntValue(ini_window_settings_section, ini_screen_height, height);
}

void GameSettings::SetWireFrameMode(const bool is_on) {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	ini.SetBoolValue(ini_window_settings_section, ini_wireframe_mode, is_on);
}

void GameSettings::SetWindowedMode(const bool is_on) {
	LittleLot::ConfigIni ini = LittleLot::ConfigIni(ini_file);
	ini.SetBoolValue(ini_window_settings_section, ini_windowed_fullscreen_mode, is_on);
}
