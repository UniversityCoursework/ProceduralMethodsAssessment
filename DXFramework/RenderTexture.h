#ifndef _RENDERTEXTURE_H_
#define _RENDERTEXTURE_H_

#include <d3d11.h>
#include <directxmath.h>

using namespace DirectX;

/// <summary>
/// An alternative render target to render too.
/// </summary>
class RenderTexture {
public:
	/// <summary>
	/// Replace so aligned 16 for direct x variables (XMFLOAT3 etc.).
	/// </summary>
	void* operator new(size_t i) {
		return _mm_malloc(i, 16);
	}

	/// <summary>
	/// Replace so deleted properly.
	/// </summary>
	void operator delete(void* p) {
		_mm_free(p);
	}
		
	/// <summary>
	/// Sets the Texture width, height and depths near and far values.
	/// Also generates all the required buffers for the Render Texture.
	/// </summary>	
	/// <param name="texture_width"></param>
	/// <param name="texture_height"></param>
	/// <param name="screen_near"></param>
	/// <param name="screen_depth"></param>
	RenderTexture(ID3D11Device* device, int texture_width, int texture_height, float screen_near, float screen_depth);
	~RenderTexture();
		
	/// <summary>
	/// Sets the D3D render Target to this Render Texture.
	/// </summary>	
	void SetRenderTarget(ID3D11DeviceContext* device_context);
	
	/// <summary>
	/// Clears the Render Texture with the provided colour.
	/// </summary>	
	/// <param name="red"> 0-1 Red Value. </param>
	/// <param name="green"> 0-1 Green Value. </param>
	/// <param name="blue"> 0-1 Blue Value. </param>
	/// <param name="alpha"> 0-1 Alpha Value. </param>
	void ClearRenderTarget(ID3D11DeviceContext* device_context, float red, float green, float blue, float alpha);
	ID3D11ShaderResourceView* GetShaderResourceView();

	XMMATRIX GetProjectionMatrix();
	XMMATRIX GetOrthoMatrix();

	int GetTextureWidth();
	int GetTextureHeight();

	/// <summary>
	/// Resizes the texture to fit a new width/height.
	/// Generally used for when the screen resolution has changed
	/// and you wish the render texture to cover the whole screen again.
	/// Also generates all the required buffers for the Render Texture.
	/// </summary>
	/// <param name="width"> New width of the mesh. </param>
	/// <param name="height"> New height of the mesh. </param>
	void ResizeTexture(ID3D11Device* device, int width, int height);

private:
	/// <summary>
	/// Sets up the various buffers and descriptions and the viewport for rendering.
	/// Texture, depth buffer and stencil buffer descriptions.
	/// Shader Resource view, Render Target view and Depth Stencil View descriptions.
	/// </summary>
	void SetBuffers(ID3D11Device * device);

	/// <summary>
	/// Releases all the buffers and views, and the texture.
	/// </summary>
	void ReleaseBuffers();

private:
	int texture_width_, texture_height_;
	float screen_near_, screen_depth_;
	ID3D11Texture2D* render_target_texture_;
	ID3D11RenderTargetView* render_target_view_;
	ID3D11ShaderResourceView* shader_resource_view_;
	ID3D11Texture2D* depth_stencil_buffer_;
	ID3D11DepthStencilView* depth_stencil_view_;
	D3D11_VIEWPORT viewport_;
	XMMATRIX projection_matrix_;
	XMMATRIX ortho_matrix_;

};

#endif