#ifndef _BASEMESH_H_
#define _BASEMESH_H_

#include <d3d11.h>
#include <directxmath.h>
#include "texture.h"

using namespace DirectX;

/// <summary>
/// Base Mesh class for inheriting functionality.
/// </summary>
class BaseMesh {
protected:
	struct VertexType {
		XMFLOAT3 position;
		XMFLOAT2 texture;
		XMFLOAT3 normal;
	};

public:	
	BaseMesh();
	~BaseMesh();

	/// <summary>
	/// Sends data to the vertex buffer and sets the primitive type.
	/// </summary>
	virtual void SendData(ID3D11DeviceContext* device_context);

	int GetIndexCount();
	int GetVertexCount();
	int GetInstanceCount();
	ID3D11ShaderResourceView* GetTexture();

protected:
	/// <summary>
	/// Creates and sets all the required buffers for this mesh.
	/// Vertex,instance, class specific etc.
	/// </summary>
	virtual void InitBuffers(ID3D11Device* device) = 0;

	/// <summary>
	/// Creates and sets the texture object.
	/// </summary>
	/// <param name="filename"> The filename of the Texture to be loaded. </param>
	void LoadTexture(ID3D11Device* device, ID3D11DeviceContext* device_context, WCHAR* filename);

	ID3D11Buffer *vertex_buffer_, *index_buffer_, *instance_buffer_;
	int vertex_count_, index_count_, instance_count_;
	Texture* texture_;
};

#endif