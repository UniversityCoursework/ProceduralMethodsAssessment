#include "SimpleTimer.h"

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 02/02/17
/// </summary>
namespace LittleLot {
	SimpleTimer::SimpleTimer() {
	}

	SimpleTimer::SimpleTimer(float time) {
		time_ = time;
	}

	void SimpleTimer::Update(float delta_time) {
		// Check if it has finished.
		if (timer_ > time_) {
			is_finished_ = true;
		} else {
			// Otherwise increment the amount of time passed.
			timer_ += delta_time;
		}
	}

	void SimpleTimer::Reset() {
		// Reset the SimpleTimer, and set it to unfinished.
		timer_ = 0;
		is_finished_ = false;
	}

	bool SimpleTimer::IsFinished() {
		return is_finished_;
	}

	void SimpleTimer::SetTimer(float time) {
		time_ = time;
	}
}
