#include "BaseApplication.h"

#include "imgui.h"
#include "imgui_impl_dx11.h"

BaseApplication::BaseApplication() {
}

BaseApplication::~BaseApplication() {
	// Release the timer object.
	if (timer_) {
		delete timer_;
		timer_ = 0;
	}
	// Release the camera object.
	if (camera_) {
		delete camera_;
		camera_ = 0;
	}
	// Release the Direct3D object.
	if (direct_3d_) {
		delete direct_3d_;
		direct_3d_ = 0;
	}
}

void BaseApplication::Init(HINSTANCE h_instance, HWND hwnd, int screen_width, int screen_height, Input *input) {
	input_ = input;
	wnd_ = hwnd;
	screen_width_ = screen_width;
	screen_height_ = screen_height;

	// Create the Direct3D object.
	direct_3d_ = new D3D(screen_width, screen_height, kVSyncEnabled, hwnd, kFullScreen, kScreenDepth, kScreenNear);
	// Initialize the Direct3D object.
	if (!direct_3d_) {
		MessageBox(hwnd, L"Could not initialize DirectX 11.", L"Error", MB_OK);
		exit(EXIT_FAILURE);
	}

	// Setup ImGui binding, after d3d built, Must be initialise with d3d context etc, so must be done in application
	ImGui_ImplDX11_Init(hwnd, direct_3d_->GetDevice(), direct_3d_->GetDeviceContext());

	// Create the camera object.
	camera_ = new Camera();
	// Initialize a base view matrix with the camera for 2D user interface rendering.
	camera_->SetPosition(0.0f, 0.0f, -10.0f);
	camera_->Update();

	// Create the timer object.
	timer_ = new Timer();


}

bool BaseApplication::Frame() {
	// Check if the user pressed escape and wants to exit the application.
	if (input_->IsKeyDown(VK_ESCAPE) == true) {
		return false;
	}

	// Update the system stats.
	timer_->Frame();

	// Do the frame input processing.
	HandleInput(timer_->GetTime());

	return true;
}

void BaseApplication::HandleInput(float frame_time) {
	// Set the frame time for calculating the updated position.
	camera_->SetFrameTime(frame_time);

	// Handle the input.
	if (input_->IsKeyDown('W')) {
		// forward
		camera_->MoveForward();
	}
	if (input_->IsKeyDown('S')) {
		// back
		camera_->MoveBackward();
	}
	if (input_->IsKeyDown('A')) {
		// Strafe Left
		camera_->StrafeLeft();
	}
	if (input_->IsKeyDown('D')) {
		// Strafe Right
		camera_->StrafeRight();
	}
	if (input_->IsKeyDown('Q')) {
		// Down
		camera_->MoveDownward();
	}
	if (input_->IsKeyDown('E')) {
		// Up
		camera_->MoveUpward();
	}
	if (input_->IsKeyDown(VK_UP)) {
		// rotate up
		camera_->TurnUp();
	}
	if (input_->IsKeyDown(VK_DOWN)) {
		// rotate down
		camera_->TurnDown();
	}
	if (input_->IsKeyDown(VK_LEFT)) {
		// rotate left
		camera_->TurnLeft();
	}
	if (input_->IsKeyDown(VK_RIGHT)) {
		// rotate right
		camera_->TurnRight();
	}

	if (input_->IsMouseActive()) {

		// mouse look is on		
		mouse_delta_x_ = input_->GetMouseX() - (screen_width_ / 2);
		mouse_delta_y_ = input_->GetMouseY() - (screen_height_ / 2);
		camera_->Turn(mouse_delta_x_, mouse_delta_y_);
		cursor_.x = screen_width_ / 2;
		cursor_.y = screen_height_ / 2;
		ClientToScreen(wnd_, &cursor_);
		SetCursorPos(cursor_.x, cursor_.y);
	}

	if (input_->IsKeyDown(VK_SPACE)) {
		RECT rect;
		GetClientRect(wnd_, &rect);
		screen_width_ = static_cast<int>(rect.right - rect.left);
		screen_height_ = static_cast<int>(rect.bottom - rect.top);
		// re-position cursor_
		cursor_.x = screen_width_ / 2;
		cursor_.y = screen_height_ / 2;
		ClientToScreen(wnd_, &cursor_);
		SetCursorPos(cursor_.x, cursor_.y);
		input_->SetMouseX(screen_width_ / 2);
		input_->SetMouseY(screen_height_ / 2);
		input_->SetKeyUp(VK_SPACE);
		// if space pressed toggle mouse
		input_->SetMouseActive(!input_->IsMouseActive());
		if (!input_->IsMouseActive()) {
			ShowCursor(true);
		} else {
			ShowCursor(false);
		}
	}
}
