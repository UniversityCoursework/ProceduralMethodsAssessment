#include "timer.h"

Timer::Timer() {
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&frequency_);
	if (frequency_ == 0) {
		MessageBox(NULL, L"No support for high performance timer", L"ERROR", MB_OK);
	}

	// Find out how many times the frequency counter ticks every second.
	ticks_per_second_ = (float)(frequency_);

	QueryPerformanceCounter((LARGE_INTEGER*)&start_time_);
}

Timer::~Timer() {
}

void Timer::Frame() {
	INT64 currentTime;
	float timeDifference;


	// Query the current time.
	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

	// Calculate the difference in time since the last time we queried for the current time.
	timeDifference = (float)(currentTime - start_time_);

	// Calculate the frame time by the time difference over the timer speed resolution.
	frame_time_ = timeDifference / ticks_per_second_;

	// Restart the timer.
	start_time_ = currentTime;
}

float Timer::GetTime() {
	return frame_time_;
}