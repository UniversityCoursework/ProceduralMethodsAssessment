#ifndef _LITTLELOT_SIMPLE_TIMER_H
#define _LITTLELOT_SIMPLE_TIMER_H

/// <summary>
/// Little Framework that provides alot of benefit.
/// @StewLMcC - 02/02/17
/// </summary>
namespace LittleLot {
	/// <summary>
	/// Simple, stupid and inprecise timer.
	/// 
	/// Useful for stuff were we just want a rough time, not incredably accurate.
	/// </summary>
	class SimpleTimer {
	public:
		SimpleTimer();
		/// <summary>
		/// Set up the timer with countdown time, before it is finished.
		/// </summary>
		/// <param name="time">The amount of time to pass in seconds. </param>
		SimpleTimer(float time);

		/// <summary>
		///	Update the timer with the amount of time passed since last update.
		/// </summary>
		/// <param name="delta_time"> Time in seconds since last update.</param>
		void Update(float delta_time);


		/// <summary>
		/// Reset timer back to zero, but maintain countdown time.
		/// </summary>
		void Reset();


		/// <summary>
		/// Check if the timer has reached the countdown time.
		/// </summary>
		/// <returns> Returns true if it has finished. </returns>
		bool IsFinished();

		/// <summary>
		/// Change time at which the timer will finish.
		/// </summary>
		/// <param name="time"> The amount of time to pass in seconds. </param>
		void SetTimer(float time);

	private:
		bool is_finished_;
		float timer_;
		float time_;
	};
}
#endif
