# Procedural Methods Assessment

## Doxygen

https://universitycoursework.gitlab.io/ProceduralMethodsAssessment/

Procedural Methods Assessment App
 - Based on graphics programming framework.
 - Expanded to showcase procedural methods.
 - fBm Noise Terrain, using perlin noise. 
 - Terrain color based on height and slope, and surronding voxels data ( multiple parameters).
 - Terrain depth by utilizing instance rendering.
 - Cellular automata cave generation. 
 - Post proccesses; Gaussian blur, chromatic aberation.
